<?php

// ini_set('display_errors', -1);
// if (version_compare(PHP_VERSION, '5.3', '>='))
// {
//     error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
// }
// else
// {
//     error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
// }
error_reporting(0);
define('APP_PATH', realpath('..'));
date_default_timezone_set('Asia/Manila');

//echo @$_SERVER['HTTP_HOST'];

try {

    /**
     * Read the configuration
     */
    $config = include APP_PATH . "/app/config/config.php";

    /**
     * Read auto-loader
     */
    include APP_PATH . "/app/config/loader.php";

    /**
     * Read services
     */
    include APP_PATH . "/app/config/services.php";

    /**
     * Read routes
     */
    // include APP_PATH . "/app/config/routes.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}
