// cls jscript
$(window).on('scrolldelta', function(e){
    var top = e.scrollTop;
    // var topDelta = e.scrollTopDelta;
    // var left = e.scrollLeft;
    // var leftDelta = e.scrollLeftDelta;
    var feedbackText = 'scrollTop: ' + top.toString() + "px";
    // + 'px (' + (topDelta >= 0 ? '+' : '') + topDelta.toString() + 'px), scrollLeft: ' + left.toString() + 'px (' + (leftDelta >= 0 ? '+' : '') + leftDelta.toString() + 'px)';
    // document.getElementById('feedback').innerHTML = feedbackText;
    $window = $(window);

    if($window.scrollTop() >= 50){
        $('.cls-menu-div').addClass('fixed');
        $('.cls-form-div').addClass('fixed');
        // $('.profile-menu').addClass('fixed');
    } else {
        $('.cls-menu-div').removeClass('fixed');
        $('.cls-form-div').removeClass('fixed');
        // $('.profile-menu').removeClass('fixed');
    }

    var bank_div = $('.bank-div').offset().top-220;
    if($window.scrollTop() >= bank_div){
    	$('.cd-ba').addClass('active');
		$('.cd-menu').removeClass('active');
    } else {
    	$('.cd-ba').removeClass('active');
		$('.cd-menu').addClass('active');
    }

    if($window.scrollTop() >= $('.csu-div').offset().top-220){
    	$('.cs-menu').addClass('active');
		$('.esu-menu').removeClass('active');
    } else {
    	$('.cs-menu').removeClass('active');
		$('.esu-menu').addClass('active');
    }
});

$(document).ready(function(){

	// for the wysiwyg text editor
    $('.editor').froalaEditor({
    	toolbarButtons: ['bold', 'italic', 'underline', '|', 'formatOL', 'formatUL', '|', 'align', 'outdent', 'indent', 'paragraphFormat']
    });

});

// for company profile edit - click action
$(document).on('click','.cd-menu',function(){
	$(this).addClass('active');
	$('.cd-ba').removeClass('active');
	$('html,body').animate({
        scrollTop: $('.company-div').offset().top-100},
        100);
});

$(document).on('click','.cd-ba',function(){
	$(this).addClass('active');
	$('.cd-menu').removeClass('active');
	$('html,body').animate({
        scrollTop: $('.bank-div').offset().top-100},
        100);
});

// for settings - click action
$(document).on('click','.esu-menu',function(){
	$(this).addClass('active');
	$('.cs-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bu-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.esu-div').offset().top-100},
        100);
});

$(document).on('click','.cs-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bu-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.csu-div').offset().top-100},
        100);
});

$(document).on('click','.tpv-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.cs-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bu-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.psev-div').offset().top-100},
        100);
});

$(document).on('click','.itbt-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.cs-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bu-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.it-div').offset().top-100},
        100);
});

$(document).on('click','.tac-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.cs-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.bu-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.tac-div').offset().top-100},
        100);
});

$(document).on('click','.bu-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.cs-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bj-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.bu-div').offset().top-100},
        100);
});

$(document).on('click','.bj-menu',function(){
	$(this).addClass('active');
	$('.esu-menu').removeClass('active');
	$('.cs-menu').removeClass('active');
	$('.tpv-menu').removeClass('active');
	$('.itbt-menu').removeClass('active');
	$('.tac-menu').removeClass('active');
	$('.bu-menu').removeClass('active');

	$('html,body').animate({
        scrollTop: $('.bk-div').offset().top-100},
        100);
});


$(document).on('click', '.add-phone-fir', function(){
	$(this).addClass('hidden');
	var clone_phone = $('.clone-phone').clone();

		clone_phone.addClass('mt-10');
		clone_phone.find('.add-phone-fir').addClass('hidden');
		clone_phone.find('.cls-form-btn-ad').removeClass('hidden');
		clone_phone.appendTo('.cls-clone-phone');

	// var div_height = $('.cls-form').height();
	// $('.cls-buttons').attr('style', 'height:'+div_height+'px');

	// $('.contact-p-div').find('.contactdiv:last').find('#btnDeleteClone').removeClass('hidden');
});

// add bank in the form
/*$(document).on('click', '.btn-ad-ba', function(){
	$('.bank-form').clone().appendTo('.bank-div');
});*/

$(document).on('click', '.btn-db', function(){
	var ctr = 0;

	$(".bank-div .bank-form").each(function(){
		ctr++;
	});

	if(ctr == 1){
		$('#modalLessBankError').modal('show');
	} else{
		$(this).closest('.bank-form').remove();
	}
});