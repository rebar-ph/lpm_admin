/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
  $('.js-example-basic-multiple').select2(); 
  $(".toggle-switch").onoff(); 
 
  $uploadCrop = $('.img-crop').croppie({ 
      	enableExif: true, 
      	viewport: { 
          	width: 350, 
          	height: 350, 
          	type: 'square' 
      	}, 
      	boundary: { 
          	width: 400, 
          	height: 400 
      	}, 
      	enforceBoundary: true, 
      	mouseWheelZoom: true, 
      	result: { 
        	type: 'canvas', 
 			format: 'png',
 			quality: 1
      	} 
  	}); 
}); 
 
$(document).on('click', '.btn-pi-add', function(){ 
	$("#modalUploadImg").modal("show"); 
        $('.cr-image').addClass('hidden');
  	var data_id = $(this).data("id"); 
  	$("#buttonCtr").val(data_id); 
}); 
//test
$(document).on('click', '.btn-choose-file', function (e){ 
  	e.preventDefault(); 
        $('.cr-image').removeClass('hidden');
  	$('input[name=imageFileUpload]').click(); 
}); 
 
$(document).on('change', '#modalProductPicture', function (e){ 
  	e.preventDefault(); 
  	var fr = new FileReader(); 
 
  	fr.onload = function(e) { 
    	$('.img-crop').croppie('bind', { 
      		url: e.target.result 
    	}); 
  	} 
  	fr.readAsDataURL(this.files[0]); 
}); 

// https://foliotek.github.io/Croppie/
$(document).on('click', '#submitUploadImg', function(){ 
  	var img_coordinates = $('.img-crop').croppie("result", {
  		type: "canvas", 
  		format: "png"
  	})
  	.then(function(img){ 
  	var data_id = $("#buttonCtr").val(); 
 
	  	if(data_id == 1){ 
		    $('#mainPic').attr('src',img);
            $('#image_link_1').val(img);
		    $('.main-btn').addClass('hidden'); 
		    $('.main-holder').removeClass('hidden');  
	  	} else if (data_id == 2){ 
		    $('#2ndPic').attr('src',img); 
            $('#image_link_2').val(img);
		    $('.secondary-btn').addClass('hidden'); 
		    $('.secondary-holder').removeClass('hidden'); 
	  	} else if (data_id == 3){ 
		    $('#3rdPic').attr('src',img);
            $('#image_link_3').val(img);
		    $('.third-btn').addClass('hidden'); 
		    $('.third-holder').removeClass('hidden'); 
	  	} else if (data_id == 4){ 
		    $('#4thPic').attr('src',img);
            $('#image_link_4').val(img);
		    $('.fourth-btn').addClass('hidden'); 
		    $('.fourth-holder').removeClass('hidden'); 
	  	} else { 
		    $('#5thPic').attr('src',img); 
            $('#image_link_5').val(img);
		    $('.fifth-btn').addClass('hidden'); 
		    $('.fifth-holder').removeClass('hidden'); 
	  	} 
	 
	  	$("#modalUploadImg").modal("hide"); 
  	}); 
}); 

$(document).on('click', '.btn-main-img', function(){
	var data_main = $(this).data("main");
	var main_src = $('#mainPic').attr('src');
  	$("#buttonCtr").val(data_main); 

  	if(data_main == 2){
  		var sec_src = $('#2ndPic').attr('src');

  		if(main_src == undefined){
  			main_src = $('#mainPic').attr('src', sec_src);
  			$('.main-btn').addClass('hidden'); 
			$('.main-holder').removeClass('hidden');
			
			$('#2ndPic').attr('src', undefined);
			$('.secondary-holder').addClass('hidden');
			$('.secondary-btn').removeClass('hidden');
  		} else {
  			$('#mainPic').attr('src', sec_src);
  			$('#2ndPic').attr('src', main_src);
		}
	} else if(data_main == 3){
  		var third_src = $('#3rdPic').attr('src');
  		
  		if(main_src == undefined){
  			main_src = $('#mainPic').attr('src', third_src);
  			$('.main-btn').addClass('hidden'); 
			$('.main-holder').removeClass('hidden');
			
			$('#3rdPic').attr('src', undefined);
			$('.third-holder').addClass('hidden');
			$('.third-btn').removeClass('hidden');
  		} else {
  			$('#mainPic').attr('src', third_src);
  			$('#3rdPic').attr('src', main_src);
		}
	} else if(data_main == 4){
  		var fourth_src = $('#4thPic').attr('src');
  		
  		if(main_src == undefined){
  			main_src = $('#mainPic').attr('src', fourth_src);
  			$('.main-btn').addClass('hidden'); 
			$('.main-holder').removeClass('hidden');
			
			$('#4thPic').attr('src', undefined);
			$('.fourth-holder').addClass('hidden');
			$('.fourth-btn').removeClass('hidden');
  		} else {
  			$('#mainPic').attr('src', fourth_src);
  			$('#4thPic').attr('src', main_src);
		}
	} else {
  		var fifth_src = $('#5thPic').attr('src');
  		
  		if(main_src == undefined){
  			main_src = $('#mainPic').attr('src', fifth_src);
  			$('.main-btn').addClass('hidden'); 
			$('.main-holder').removeClass('hidden');
			
			$('#5thPic').attr('src', undefined);
			$('.fifth-holder').addClass('hidden');
			$('.fifth-btn').removeClass('hidden');
  		} else {
  			$('#mainPic').attr('src', fifth_src);
  			$('#5thPic').attr('src', main_src);
		}
	}
});
 
$(document).on('click', '.btn-delete-img', function(){ 
  	var data_delete = $(this).data("delete");
  	var empty = "";
  	$("#buttonCtr").val(data_delete); 

  	if (data_delete == 1){
  		var main_sec = $("#mainPic").attr("src", empty);
  		$('.main-btn').removeClass('hidden'); 
		$('.main-holder').addClass('hidden');
                $('#image_link_1').val("DEL");
  	} else if(data_delete == 2){
  		var sec_src = $("#2ndPic").attr("src", empty);
  		$('.secondary-holder').addClass('hidden');
		$('.secondary-btn').removeClass('hidden');
                $('#image_link_2').val("DEL");
	} else if(data_delete == 3){
  		var third_src = $('#3rdPic').attr('src', empty);
  		$('.third-holder').addClass('hidden');
		$('.third-btn').removeClass('hidden');
                $('#image_link_3').val("DEL");
	} else if(data_delete == 4){
  		var fourth_src = $('#4thPic').attr('src', empty);
  		$('.fourth-holder').addClass('hidden');
		$('.fourth-btn').removeClass('hidden');
                $('#image_link_4').val("DEL");
	} else {
  		var fifth_src = $('#5thPic').attr('src', empty);
  		$('.fifth-holder').addClass('hidden');
		$('.fifth-btn').removeClass('hidden');
                $('#image_link_5').val("DEL");
	}
});

$(document).on('click', '#btnDeleteProd', function(){
	$('#modalDeleteProd').modal('show');
});

$(document).on('click', '#applyChangesBtn', function(){
	console.log($('#selectGroups').val());
	
	$(this).prop('disabled','disabled');
	
	if ($('#selectBrand').val() == "0")
	{
		alert("Brand is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	if ($('#selectCategory').val() == "0")
	{
		alert("Category is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	if ($('#productName').val() == "")
	{
		alert("Product Name is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	var groups_array = $('#selectGroups').val();
//	if ((!groups_array) || (groups_array.length == 0))
//	{
//		alert('No Groups found');
//		return;
//	}
	
	var radio_states = document.getElementsByName('radioState');
	var radio_states_value = 0;
	for (var i=0, length = radio_states.length; i < length; i++)
	{
		if (radio_states[i].checked)
		{
			radio_states_value = radio_states[i].value;
			break;
		}
	}
	
	var kinds_array = [];
	var measurements_array = [];
	var colors_array = [];
	var types_array = [];
	var displays_array = [];
	var uoms_array = [];
	var lead_times_array = [];
	
	$('input.input-kind').each(function(){
		if ($(this).val() != "") kinds_array.push($(this).val());
		else kinds_array.push("-");
	});
	
	$('input.input-measurement').each(function(){
		if ($(this).val() != "") measurements_array.push($(this).val());
		else measurements_array.push("-");
	});
	
	$('input.input-color').each(function(){
		if ($(this).val() != "") colors_array.push($(this).val());
		else colors_array.push("-");
	});
	
	$('input.input-type').each(function(){
		if ($(this).is(':checked')) types_array.push(0);
		else types_array.push(1);
	});
	
	$('input.input-display').each(function(){
		
		if ($(this).is(':checked')) displays_array.push(1);
		else displays_array.push(0);
		
		//displays_array.push($(this).val());
	});
	
	$('input.input-uom').each(function(){
		if ($(this).val() != "") uoms_array.push($(this).val());
		else uoms_array.push("-");
	});
	
	$('input.input-lead-time').each(function(){
		lead_times_array.push($(this).val());
	});
	
	var params = {
		brand_id : $('#selectBrand').val(),
		category_id : $('#selectCategory').val(),
		product_name : $('#productName').val(),
		product_description : $('#productDescription').val(),
		state_id : radio_states_value,
		kinds_array : kinds_array,
		measurements_array : measurements_array,
		colors_array : colors_array,
		types_array : types_array,
		displays_array : displays_array,
		uoms_array : uoms_array,
		lead_times_array : lead_times_array,
		groups_array : groups_array,
        image_link_1 : $('#image_link_1').val(),
        image_link_2 : $('#image_link_2').val(),
        image_link_3 : $('#image_link_3').val(),
        image_link_4 : $('#image_link_4').val(),
        image_link_5 : $('#image_link_5').val()                
		
	};
	
	console.log(params);
	
	$.post(BASE_URL+'products/addProduct',params,function(data){
		if (data == "Success")
		{
			window.location.href = BASE_URL+"products";
		}
		else
		{
			alert("Something Went Wrong");
			$(this).removeAttr('disabled');
		}
	});
});

$(document).on('click', '#applyChangesEditBtn', function(){
	
	$(this).prop('disabled','disabled');
	
	if ($('#selectBrand').val() == "0")
	{
		alert("Brand is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	if ($('#selectCategory').val() == "0")
	{
		alert("Category is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	if ($('#productName').val() == "")
	{
		alert("Product Name is Required");
		$(this).removeAttr('disabled');
		return;
	}
	
	var groups_array = $('#selectGroups').val();
//	if ((!groups_array) || (groups_array.length == 0))
//	{
//		alert('No Groups found');
//		return;
//	}
	
	
	var radio_states = document.getElementsByName('radioState');
	var radio_states_value = 0;
	for (var i=0, length = radio_states.length; i < length; i++)
	{
		if (radio_states[i].checked)
		{
			radio_states_value = radio_states[i].value;
			break;
		}
	}
	
	var kind_ids_array = [];
	var kinds_array = [];
	var measurements_array = [];
	var colors_array = [];
	var types_array = [];
	var displays_array = [];
	var uoms_array = [];
	var lead_times_array = [];
	
	$('.tr-add-class').each(function(){
		kind_ids_array.push($(this).attr('data-id'));
	});
	
	$('input.input-kind').each(function(){
		if ($(this).val() != "") kinds_array.push($(this).val());
		else kinds_array.push("-");
	});
	
	$('input.input-measurement').each(function(){
		if ($(this).val() != "") measurements_array.push($(this).val());
		else measurements_array.push("-");
	});
	
	$('input.input-color').each(function(){
		if ($(this).val() != "") colors_array.push($(this).val());
		else colors_array.push("-");
	});
	
	$('input.input-type').each(function(){
		if ($(this).is(':checked')) types_array.push(0);
		else types_array.push(1);
	});
	
	$('input.input-display').each(function(){
		
		if ($(this).is(':checked')) displays_array.push(1);
		else displays_array.push(0);
	});
	
	$('input.input-uom').each(function(){
		if ($(this).val() != "") uoms_array.push($(this).val());
		else uoms_array.push("-");
	});
	
	$('input.input-lead-time').each(function(){
		lead_times_array.push($(this).val());
	});
	
	var params = {
		product_id : $('#productID').val(),
		brand_id : $('#selectBrand').val(),
		category_id : $('#selectCategory').val(),
		product_name : $('#productName').val(),
		product_description : $('#productDescription').val(),
		state_id : radio_states_value,
		kind_ids_array : kind_ids_array,
		kinds_array : kinds_array,
		measurements_array : measurements_array,
		colors_array : colors_array,
		types_array : types_array,
		displays_array : displays_array,
		uoms_array : uoms_array,
		lead_times_array : lead_times_array,
		groups_array : groups_array,
        image_link_1 : $('#image_link_1').val(),
        image_link_2 : $('#image_link_2').val(),
        image_link_3 : $('#image_link_3').val(),
        image_link_4 : $('#image_link_4').val(),
        image_link_5 : $('#image_link_5').val()                   
		
	};
	
	console.log(params);
	
	$.post(BASE_URL+'products/updateProduct',params,function(data){
		if (data == "Success")
		{
			window.location.href = BASE_URL+"products";
		}
		else
		{
			alert("Something Went Wrong");
			$(this).removeAttr('disabled');
		}
	});
});


$(document).on('click', '#addKind', function(){
	
	$("input.toggle-switch").onoff('destroy');
	
	var add_class_counter = 0;
	$('.tr-add-class').each(function(){
		add_class_counter++;
	});
	
	var html_content = '<tr class="tr-add-class" data-id="0" data-counter="'+add_class_counter+'"><td><input type="text" style="text-align : left;" class="form-control input-kind tbl-field" value="-"></td><td><input type="text" style="text-align : left;" class="form-control input-measurement tbl-field" value="-"></td>' +
			'<td><input type="text" style="text-align : left;" class="form-control input-color tbl-field" value="-"></td><td><input type="checkbox" class="toggle-switch toggle-type input-type tbl-field" checked/></td>' +
			'<td><input type="checkbox" class="toggle-switch toggle-display input-display tbl-field"/></td><td><input type="text" class="form-control input-uom tbl-field" value="Piece/s"></td>' +
			'<td><input type="text" class="form-control input-lead-time tbl-field" min="0" value="0" disabled></td><td class="text-center"><a class="btn-save-kind"><i class="fa fa-save"></i></a>' +
			'<a class="btn-edit-kind hidden"><i class="fa fa-pencil"></i></a><a class="btn-delete-kind"><i class="fa fa-trash-o"></i></a></td></tr>';
	
	//$('#toCloneRow').clone().appendTo('#prodKindsTbl tbody');
	$('#prodKindsTbl tbody').append(html_content);
	
	
	$("input.toggle-switch").onoff();
        $(".input-lead-time").inputmask("numeric", {prefix: "", rightAlign: false, allowMinus: !1}); 
});

$(document).on('click', '.btn-save-kind', function(){

	$(this).closest('tr').find('.tbl-field').attr('disabled','disabled');
	$(this).addClass('hidden');
	$(this).closest('tr').find('.btn-edit-kind').removeClass('hidden');

});

$(document).on('click', '.btn-edit-kind', function(){

	
	$(this).closest('tr').find('.tbl-field').removeAttr('disabled');
	$(this).addClass('hidden');
	$(this).closest('tr').find('.btn-save-kind').removeClass('hidden');
	
	if ($(this).closest('tr').find('.input-type').is(':checked')) $(this).closest('tr').find('.input-lead-time').attr('disabled','disabled');
});

$(document).on('click', '.btn-delete-kind', function(){
	//$(this).closest('tr').remove();
	
	var data_counter = $(this).closest('tr').attr('data-counter');
	var data_name = $(this).closest('tr').find('.input-kind').val();
	
	$('#modalDeleteProdKind').modal('show');
    $('#modalDeleteProdKind .product-kind-name').html(data_name);
    $('#modalDeleteProdKind #confirmDeleteProdKind').attr('data-counter', data_counter);
	
});

$(document).on('click', '#modalDeleteProdKind #confirmDeleteProdKind', function(){
	var kind_row = "";
	var kind_id = $(this).attr('data-counter');
	
	$('.tr-add-class').each(function(){
		if ($(this).attr('data-counter') == kind_id) $(this).remove();
	});
	
	$('#modalDeleteProdKind').modal('hide');
	
});

$(document).on('keyup', '.tbl-field', function(){
	if ($(this).val() != "") $(this).removeClass('failed');
	else $(this).addClass('failed');
});

$(document).on('click', 'input.toggle-type', function(){
	if ($(this).is(':checked'))
	{
		$(this).closest('tr').find('.input-lead-time').attr('disabled','disabled');
		$(this).closest('tr').find('.input-lead-time').val(0);
	}
	else
	{
		$(this).closest('tr').find('.input-lead-time').removeAttr('disabled');
	}
});

$(document).on('click', '#confirmDeleteProd', function(){
	var params = {
		product_id : $(this).attr('data-id'),
	};
	
	$.post(BASE_URL+'products/deleteProduct', params, function(data){
		if (data == "Success")
		{
			alert("Delete Success");
			window.location.href = BASE_URL+"products";
		}
		else
		{
			alert("Something Went Wrong");
		}
		
		
	});
});

$(document).on('click', '#newProduct', function(){
	window.location.href = BASE_URL+"products/add_new";
});

$(document).on('click', '#cancelBtn', function(){
	window.location.href = BASE_URL+"products";
});

$(document).on('click', '.btn-published-prod,.btn-publish-prod', function(){
	$(this).closest('td').find('.btn-published-prod').toggleClass('hidden');
	$(this).closest('td').find('.btn-publish-prod').toggleClass('hidden');
	var params = {
		product_kind_id : $(this).attr('data-id'),
	};
	
	$.post(BASE_URL+'products/toggleDisplayStatus', params, function(data){
		console.log(data);
	});
});

$(document).on('click', '.publish-all', function(){
	
	var table = $('#productsTable').DataTable();
	
	table.$('.btn-published-prod').removeClass('hidden');
	table.$('.btn-publish-prod').addClass('hidden');
	
	table.draw();
	
	var params = {
		product_kind_id : "ALL",
	};
	
	$.post(BASE_URL+'products/toggleDisplayStatus', params, function(data){
		console.log(data);
	});
});

