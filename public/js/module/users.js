// users jscript file
$(document).ready(function(){
	$('.toggle-switch').onoff();
});

$(window).on('scrolldelta', function(e){
    var top = e.scrollTop;
    // var topDelta = e.scrollTopDelta;
    // var left = e.scrollLeft;
    // var leftDelta = e.scrollLeftDelta;
    var feedbackText = 'scrollTop: ' + top.toString() + "px";
    // + 'px (' + (topDelta >= 0 ? '+' : '') + topDelta.toString() + 'px), scrollLeft: ' + left.toString() + 'px (' + (leftDelta >= 0 ? '+' : '') + leftDelta.toString() + 'px)';
    // document.getElementById('feedback').innerHTML = feedbackText;
    $window = $(window);

    if(top >= 50){
        $('.cls-menu-div').addClass('fixed');
        $('.cls-user-div').addClass('fixed');
    } else {
        $('.cls-menu-div').removeClass('fixed');
        $('.cls-user-div').removeClass('fixed');
    }

    if(top >= 120){
        $('.clsm-company').removeClass('active');
        $('.clsm-pi').addClass('active');
    }

    if(top < 150){
        $('.clsm-pi').removeClass('active');
        $('.clsm-company').addClass('active');
    } else if(top >= 200){
        $('.clsm-ld').addClass('active');
        $('.clsm-pi').removeClass('active');
    }

    if(top < 200){
        $('.clsm-ld').removeClass('active');
    }

});

$(document).on('click','.clsm-company',function(){
	$(this).addClass('active');
	$('.clsm-pi').removeClass('active');
	$('.clsm-ld').removeClass('active');
	$('html,body').animate({
        scrollTop: $('.clsu-company-section').offset().top-100},
        100);
});

$(document).on('click','.clsm-pi',function(){
	$(this).addClass('active');
	$('.clsm-company').removeClass('active');
	$('.clsm-ld').removeClass('active');
	$('html,body').animate({
        scrollTop: $('.clsu-pi-section').offset().top-100},
        100);
});

$(document).on('click','.clsm-ld',function(){
	$(this).addClass('active');
	$('.clsm-company').removeClass('active');
	$('.clsm-pi').removeClass('active');
	$('html,body').animate({
        scrollTop: $('.clsu-ld-section').offset().top-100},
        100);
});


// toggle active class to customer type buttons
$(document).on("click", "#btnSeller", function(){
    $(this).toggleClass("active");
    $("#btnContractor").removeClass("active");
    $("#btnPersonal").removeClass("active");

    $('.imgSeller').attr('src',BASE_URL+'/img/customer-type/seller-active.png');
    $('.imgContractor').attr('src',BASE_URL+'/img/customer-type/contractor-inactive.png');
    $('.imgPersonal').attr('src',BASE_URL+'/img/customer-type/personal-inactive.png');
});

$(document).on("click", "#btnContractor", function(){ 
    $(this).toggleClass("active");
    $("#btnSeller").removeClass("active");
    $("#btnPersonal").removeClass("active");

    $('.imgSeller').attr('src',BASE_URL+'/img/customer-type/seller-inactive.png');
    $('.imgContractor').attr('src',BASE_URL+'/img/customer-type/contractor-active.png');
    $('.imgPersonal').attr('src',BASE_URL+'/img/customer-type/personal-inactive.png');
});

$(document).on("click", "#btnPersonal", function(){
    $(this).toggleClass("active");
    $("#btnSeller").removeClass("active");
    $("#btnContractor").removeClass("active");

    $('.imgSeller').attr('src',BASE_URL+'/img/customer-type/seller-inactive.png');
    $('.imgContractor').attr('src',BASE_URL+'/img/customer-type/contractor-inactive.png');
    $('.imgPersonal').attr('src',BASE_URL+'/img/customer-type/personal-active.png');
});
