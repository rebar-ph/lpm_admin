/*! cropit - v0.5.1 <https://github.com/scottcheng/cropit> */ 
(function webpackUniversalModuleDefinition(root, factory) { 
  if(typeof exports === 'object' && typeof module === 'object') 
    module.exports = factory(require("jquery")); 
  else if(typeof define === 'function' && define.amd) 
    define(["jquery"], factory); 
  else if(typeof exports === 'object') 
    exports["cropit"] = factory(require("jquery")); 
  else 
    root["cropit"] = factory(root["jQuery"]); 
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__) { 
return /******/ (function(modules) { // webpackBootstrap 
/******/   // The module cache 
/******/   var installedModules = {}; 
 
/******/   // The require function 
/******/   function __webpack_require__(moduleId) { 
 
/******/     // Check if module is in cache 
/******/     if(installedModules[moduleId]) 
/******/       return installedModules[moduleId].exports; 
 
/******/     // Create a new module (and put it into the cache) 
/******/     var module = installedModules[moduleId] = { 
/******/       exports: {}, 
/******/       id: moduleId, 
/******/       loaded: false 
/******/     }; 
 
/******/     // Execute the module function 
/******/     modules[moduleId].call(module.exports, module, module.exports, __webpack_require__); 
 
/******/     // Flag the module as loaded 
/******/     module.loaded = true; 
 
/******/     // Return the exports of the module 
/******/     return module.exports; 
/******/   } 
 
 
/******/   // expose the modules object (__webpack_modules__) 
/******/   __webpack_require__.m = modules; 
 
/******/   // expose the module cache 
/******/   __webpack_require__.c = installedModules; 
 
/******/   // __webpack_public_path__ 
/******/   __webpack_require__.p = ""; 
 
/******/   // Load entry module and return exports 
/******/   return __webpack_require__(0); 
/******/ }) 
/************************************************************************/ 
/******/ ([ 
/* 0 */ 
/***/ function(module, exports, __webpack_require__) { 
 
  var _slice = Array.prototype.slice; 
 
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; } 
 
  var _jquery = __webpack_require__(1); 
 
  var _jquery2 = _interopRequireDefault(_jquery); 
 
  var _cropit = __webpack_require__(2); 
 
  var _cropit2 = _interopRequireDefault(_cropit); 
 
  var _constants = __webpack_require__(4); 
 
  var _utils = __webpack_require__(6); 
 
  var applyOnEach = function applyOnEach($el, callback) { 
    return $el.each(function () { 
      var cropit = _jquery2['default'].data(this, _constants.PLUGIN_KEY); 
 
      if (!cropit) { 
        return; 
      } 
      callback(cropit); 
    }); 
  }; 
 
  var callOnFirst = function callOnFirst($el, method, options) { 
    var cropit = $el.first().data(_constants.PLUGIN_KEY); 
 
    if (!cropit || !_jquery2['default'].isFunction(cropit[method])) { 
      return null; 
    } 
    return cropit[method](options); 
  }; 
 
  var methods = { 
    init: function init(options) { 
      return this.each(function () { 
        // Only instantiate once per element 
        if (_jquery2['default'].data(this, _constants.PLUGIN_KEY)) { 
          return; 
        } 
 
        var cropit = new _cropit2['default'](_jquery2['default'], this, options); 
        _jquery2['default'].data(this, _constants.PLUGIN_KEY, cropit); 
      }); 
    }, 
 
    destroy: function destroy() { 
      return this.each(function () { 
        _jquery2['default'].removeData(this, _constants.PLUGIN_KEY); 
      }); 
    }, 
 
    isZoomable: function isZoomable() { 
      return callOnFirst(this, 'isZoomable'); 
    }, 
 
    'export': function _export(options) { 
      return callOnFirst(this, 'getCroppedImageData', options); 
    } 
  }; 
 
  var delegate = function delegate($el, fnName) { 
    return applyOnEach($el, function (cropit) { 
      cropit[fnName](); 
    }); 
  }; 
 
  var prop = function prop($el, name, value) { 
    if ((0, _utils.exists)(value)) { 
      return applyOnEach($el, function (cropit) { 
        cropit[name] = value; 
      }); 
    } else { 
      var cropit = $el.first().data(_constants.PLUGIN_KEY); 
      return cropit[name]; 
    } 
  }; 
 
  _jquery2['default'].fn.cropit = function (method) { 
    if (methods[method]) { 
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1)); 
    } else if (['imageState', 'imageSrc', 'offset', 'previewSize', 'imageSize', 'zoom', 'initialZoom', 'exportZoom', 'minZoom', 'maxZoom'].indexOf(method) >= 0) { 
      return prop.apply(undefined, [this].concat(_slice.call(arguments))); 
    } else if (['rotateCW', 'rotateCCW', 'disable', 'reenable'].indexOf(method) >= 0) { 
      return delegate.apply(undefined, [this].concat(_slice.call(arguments))); 
    } else { 
      return methods.init.apply(this, arguments); 
    } 
  }; 
 
/***/ }, 
/* 1 */ 
/***/ function(module, exports) { 
 
  module.exports = __WEBPACK_EXTERNAL_MODULE_1__; 
 
/***/ }, 
/* 2 */ 
/***/ function(module, exports, __webpack_require__) { 
 
  Object.defineProperty(exports, '__esModule', { 
    value: true 
  }); 
 
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })(); 
 
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; } 
 
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } } 
 
  var _jquery = __webpack_require__(1); 
 
  var _jquery2 = _interopRequireDefault(_jquery); 
 
  var _Zoomer = __webpack_require__(3); 
 
  var _Zoomer2 = _interopRequireDefault(_Zoomer); 
 
  var _constants = __webpack_require__(4); 
 
  var _options = __webpack_require__(5); 
 
  var _utils = __webpack_require__(6); 
 
  var Cropit = (function () { 
    function Cropit(jQuery, element, options) { 
      _classCallCheck(this, Cropit); 
 
      this.$el = (0, _jquery2['default'])(element); 
 
      var defaults = (0, _options.loadDefaults)(this.$el); 
      this.options = _jquery2['default'].extend({}, defaults, options); 
 
      this.init(); 
    } 
 
    _createClass(Cropit, [{ 
      key: 'init', 
      value: function init() { 
        var _this = this; 
 
        this.image = new Image(); 
        this.preImage = new Image(); 
        this.image.onload = this.onImageLoaded.bind(this); 
        this.preImage.onload = this.onPreImageLoaded.bind(this); 
        this.image.onerror = this.preImage.onerror = function () { 
          _this.onImageError.call(_this, _constants.ERRORS.IMAGE_FAILED_TO_LOAD); 
        }; 
 
        this.$preview = this.options.$preview.css('position', 'relative'); 
        this.$fileInput = this.options.$fileInput.attr({ accept: 'image/*' }); 
        this.$zoomSlider = this.options.$zoomSlider.attr({ min: 0, max: 1, step: 0.01 }); 
 
        this.previewSize = { 
          width: this.options.width || this.$preview.innerWidth(), 
          height: this.options.height || this.$preview.innerHeight() 
        }; 
 
        this.$image = (0, _jquery2['default'])('<img />').addClass(_constants.CLASS_NAMES.PREVIEW_IMAGE).attr('alt', '').css({ 
          transformOrigin: 'top left', 
          webkitTransformOrigin: 'top left', 
          willChange: 'transform' 
        }); 
        this.$imageContainer = (0, _jquery2['default'])('<div />').addClass(_constants.CLASS_NAMES.PREVIEW_IMAGE_CONTAINER).css({ 
          position: 'absolute', 
          overflow: 'hidden', 
          left: 0, 
          top: 0, 
          width: '100%', 
          height: '100%' 
        }).append(this.$image); 
        this.$preview.append(this.$imageContainer); 
 
        if (this.options.imageBackground) { 
          if (_jquery2['default'].isArray(this.options.imageBackgroundBorderWidth)) { 
            this.bgBorderWidthArray = this.options.imageBackgroundBorderWidth; 
          } else { 
            this.bgBorderWidthArray = [0, 1, 2, 3].map(function () { 
              return _this.options.imageBackgroundBorderWidth; 
            }); 
          } 
 
          this.$bg = (0, _jquery2['default'])('<img />').addClass(_constants.CLASS_NAMES.PREVIEW_BACKGROUND).attr('alt', '').css({ 
            position: 'relative', 
            left: this.bgBorderWidthArray[3], 
            top: this.bgBorderWidthArray[0], 
            transformOrigin: 'top left', 
            webkitTransformOrigin: 'top left', 
            willChange: 'transform' 
          }); 
          this.$bgContainer = (0, _jquery2['default'])('<div />').addClass(_constants.CLASS_NAMES.PREVIEW_BACKGROUND_CONTAINER).css({ 
            position: 'absolute', 
            zIndex: 0, 
            top: -this.bgBorderWidthArray[0], 
            right: -this.bgBorderWidthArray[1], 
            bottom: -this.bgBorderWidthArray[2], 
            left: -this.bgBorderWidthArray[3] 
          }).append(this.$bg); 
          if (this.bgBorderWidthArray[0] > 0) { 
            this.$bgContainer.css('overflow', 'hidden'); 
          } 
          this.$preview.prepend(this.$bgContainer); 
        } 
 
        this.initialZoom = this.options.initialZoom; 
 
        this.imageLoaded = false; 
 
        this.moveContinue = false; 
 
        this.zoomer = new _Zoomer2['default'](); 
 
        if (this.options.allowDragNDrop) { 
          _jquery2['default'].event.props.push('dataTransfer'); 
        } 
 
        this.bindListeners(); 
 
        if (this.options.imageState && this.options.imageState.src) { 
          this.loadImage(this.options.imageState.src); 
        } 
      } 
    }, { 
      key: 'bindListeners', 
      value: function bindListeners() { 
        this.$fileInput.on('change.cropit', this.onFileChange.bind(this)); 
        this.$imageContainer.on(_constants.EVENTS.PREVIEW, this.onPreviewEvent.bind(this)); 
        this.$zoomSlider.on(_constants.EVENTS.ZOOM_INPUT, this.onZoomSliderChange.bind(this)); 
 
        if (this.options.allowDragNDrop) { 
          this.$imageContainer.on('dragover.cropit dragleave.cropit', this.onDragOver.bind(this)); 
          this.$imageContainer.on('drop.cropit', this.onDrop.bind(this)); 
        } 
      } 
    }, { 
      key: 'unbindListeners', 
      value: function unbindListeners() { 
        this.$fileInput.off('change.cropit'); 
        this.$imageContainer.off(_constants.EVENTS.PREVIEW); 
          this.offset = { x: newX, y: newY }; // Triggers renderImage() 
        } else { 
          this._zoom = newZoom; 
        } 
 
        this.zoomSliderPos = this.zoomer.getSliderPos(this.zoom); 
        this.$zoomSlider.val(this.zoomSliderPos); 
 
        this.options.onZoomChange(newZoom); 
      }, 
      get: function () { 
        return this._zoom; 
      } 
    }, { 
      key: 'rotatedOffset', 
      get: function () { 
        return { 
          x: this.offset.x + (this.rotation === 90 ? this.image.height * this.zoom : 0) + (this.rotation === 180 ? this.image.width * this.zoom : 0), 
          y: this.offset.y + (this.rotation === 180 ? this.image.height * this.zoom : 0) + (this.rotation === 270 ? this.image.width * this.zoom : 0) 
        }; 
      } 
    }, { 
      key: 'rotation', 
      set: function (newRotation) { 
        this._rotation = newRotation; 
 
        if (this.imageLoaded) { 
          // Change in image size may lead to change in zoom range 
          this.setupZoomer(); 
        } 
      }, 
      get: function () { 
        return this._rotation; 
      } 
    }, { 
      key: 'imageState', 
      get: function () { 
        return { 
          src: this.image.src, 
          offset: this.offset, 
          zoom: this.zoom 
        }; 
      } 
    }, { 
      key: 'imageSrc', 
      get: function () { 
        return this.image.src; 
      }, 
      set: function (imageSrc) { 
        this.loadImage(imageSrc); 
      } 
    }, { 
      key: 'imageWidth', 
      get: function () { 
        return this.rotation % 180 === 0 ? this.image.width : this.image.height; 
      } 
    }, { 
      key: 'imageHeight', 
      get: function () { 
        return this.rotation % 180 === 0 ? this.image.height : this.image.width; 
      } 
    }, { 
      key: 'imageSize', 
      get: function () { 
        return { 
          width: this.imageWidth, 
          height: this.imageHeight 
        }; 
      } 
    }, { 
      key: 'initialZoom', 
      get: function () { 
        return this.options.initialZoom; 
      }, 
      set: function (initialZoomOption) { 
        this.options.initialZoom = initialZoomOption; 
        if (initialZoomOption === 'min') { 
          this._initialZoom = 0; // Will be fixed when image loads 
        } else if (initialZoomOption === 'image') { 
          this._initialZoom = 1; 
        } else { 
          this._initialZoom = 0; 
        } 
      } 
    }, { 
      key: 'exportZoom', 
      get: function () { 
        return this.options.exportZoom; 
      }, 
      set: function (exportZoom) { 
        this.options.exportZoom = exportZoom; 
        this.setupZoomer(); 
      } 
    }, { 
      key: 'minZoom', 
      get: function () { 
        return this.options.minZoom; 
      }, 
      set: function (minZoom) { 
        this.options.minZoom = minZoom; 
        this.setupZoomer(); 
      } 
    }, { 
      key: 'maxZoom', 
      get: function () { 
        return this.options.maxZoom; 
      }, 
      set: function (maxZoom) { 
        this.options.maxZoom = maxZoom; 
        this.setupZoomer(); 
      } 
    }, { 
      key: 'previewSize', 
      get: function () { 
        return this._previewSize; 
      }, 
      set: function (size) { 
        if (!size || size.width <= 0 || size.height <= 0) { 
          return; 
        } 
 
        this._previewSize = { 
          width: size.width, 
          height: size.height 
        }; 
        this.$preview.innerWidth(this.previewSize.width).innerHeight(this.previewSize.height); 
 
        if (this.imageLoaded) { 
          this.setupZoomer(); 
        } 
      } 
    }]); 
 
    return Cropit; 
  })(); 
 
  exports['default'] = Cropit; 
  module.exports = exports['default']; 
 
/***/ }, 
/* 3 */ 
/***/ function(module, exports) { 
 
  Object.defineProperty(exports, '__esModule', { 
    value: true 
  }); 
 
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })(); 
 
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } } 
      name: 'onImageError', 
      description: 'Called when image cannot be loaded.', 
      params: [{ 
        name: 'error', 
        type: 'object', 
        description: 'Error object.' 
      }, { 
        name: 'error.code', 
        type: 'number', 
        description: 'Error code. `0` means generic image loading failure. `1` means image is too small.' 
      }, { 
        name: 'error.message', 
        type: 'string', 
        description: 'A message explaining the error.' 
      }] 
    }, { 
      name: 'onZoomEnabled', 
      description: 'Called when image the zoom slider is enabled.' 
    }, { 
      name: 'onZoomDisabled', 
      description: 'Called when image the zoom slider is disabled.' 
    }, { 
      name: 'onZoomChange', 
      description: 'Called when zoom changes.', 
      params: [{ 
        name: 'zoom', 
        type: 'number', 
        description: 'New zoom.' 
      }] 
    }, { 
      name: 'onOffsetChange', 
      description: 'Called when image offset changes.', 
      params: [{ 
        name: 'offset', 
        type: 'object', 
        description: 'New offset, with `x` and `y` values.' 
      }] 
    }].map(function (o) { 
      o.type = 'function';return o; 
    }) 
  }; 
 
  var loadDefaults = function loadDefaults($el) { 
    var defaults = {}; 
    if ($el) { 
      options.elements.forEach(function (o) { 
        defaults[o.name] = $el.find(o.defaultSelector); 
      }); 
    } 
    options.values.forEach(function (o) { 
      defaults[o.name] = o['default']; 
    }); 
    options.callbacks.forEach(function (o) { 
      defaults[o.name] = function () {}; 
    }); 
 
    return defaults; 
  }; 
 
  exports.loadDefaults = loadDefaults; 
  exports['default'] = options; 
 
/***/ }, 
/* 6 */ 
/***/ function(module, exports) { 
 
  Object.defineProperty(exports, '__esModule', { 
    value: true 
  }); 
  var exists = function exists(v) { 
    return typeof v !== 'undefined'; 
  }; 
 
  exports.exists = exists; 
  var round = function round(x) { 
    return +(Math.round(x * 100) + 'e-2'); 
  }; 
  exports.round = round; 
 
/***/ } 
/******/ ]) 
}); 
;