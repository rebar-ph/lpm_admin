<?php

use Phalcon\Mvc\Router as PhRouter;

$di->set('router', function() {
    $router = new PhRouter(false);

    $router->add('/', array(
        'controller' => 'index',
        'action' => 'index'
    ));
    $router->add('login', array(
        'controller' => 'login',
        'action' => 'index'
    ));
    $router->add('login/forgot_password', array(
        'controller' => 'login',
        'action' => 'forgotPassword'
    ));
//    $router->add('login/reset_password', array(
//        'controller' => 'login',
//        'action' => 'reset_password'
//    ));
    $router->add('login/logout', array(
        'controller' => 'login',
        'action' => 'logout'
    ));
    $router->add('login/adminLogin', array(
        'controller' => 'login',
        'action' => 'adminLogin'
    )); 
//    $router->add('saveSessionVar', array(
//        'controller' => 'index',
//        'action' => 'saveSessionVar'
//    )); 
    
    $router->add('login/resetPassword', array(
        'controller' => 'login',
        'action' => 'resetPassword'
    )); 
    $router->add('login/reset_password/:params', array(
        'controller' => 'login',
        'action' => 'reset_password',
        'params' => 1
    ));

    $router->add('login/setnewpw/', array(
        'controller' => 'login',
        'action' => 'setnewpw',
        'params' => 1
    ));
    
    //Common
    $router->add('common/saveSessionVar', array(
        'controller' => 'common',
        'action' => 'saveSessionVar'
    )); 
    $router->add('common/addDaysCalendarBased', array(
        'controller' => 'common',
        'action' => 'addDaysCalendarBased'
    )); 
    $router->add('common/page_error', array(
        'controller' => 'common',
        'action' => 'page_error'
    ));     
    
    

    //Main Views
    $router->add('inquiry', array(
        'controller' => 'inquiry',
        'action' => 'index'
    ));
    $router->add('inquiry/inq_form', array(
        'controller' => 'inquiry',
        'action' => 'inq_form'
    ));
	$router->add('inquiry/getInquiryList', array(
        'controller' => 'inquiry',
        'action' => 'getInquiryList'
    ));	


    $router->add('draft_order', array(
        'controller' => 'draftorder',
        'action' => 'index'
    ));
    $router->add('draft_order/draft_form', array(
        'controller' => 'draftorder',
        'action' => 'draft_form'
    ));
    $router->add('draft_order/soa_draft', array(
        'controller' => 'draftorder',
        'action' => 'soa_draft'
    ));
	$router->add('draft_order/getDraftOrderList', array(
        'controller' => 'draftorder',
        'action' => 'getDraftOrderList'
    ));	

    $router->add('po_sent', array(
        'controller' => 'posent',
        'action' => 'index'
    ));
    $router->add('po_sent/po_form', array(
        'controller' => 'posent',
        'action' => 'po_form'
    ));
    $router->add('po_sent/create_soa', array(
        'controller' => 'posent',
        'action' => 'create_soa'
    ));
	$router->add('po_sent/getPOSentList', array(
        'controller' => 'posent',
        'action' => 'getPOSentList'
    ));
	$router->add('po_sent/sendSOA', array(
        'controller' => 'posent',
        'action' => 'sendSOA'
    ));

    $router->add('soa_for_approval', array(
        'controller' => 'soaforapproval',
        'action' => 'index'
    ));
    $router->add('soa_for_approval/form', array(
        'controller' => 'soaforapproval',
        'action' => 'form'
    ));
	$router->add('soa_for_approval/getSOAApprovalList', array(
        'controller' => 'soaforapproval',
        'action' => 'getSOAApprovalList'
    ));

    $router->add('for_payment', array(
        'controller' => 'forpayment',
        'action' => 'index'
    )); 
    $router->add('for_payment/form', array(
        'controller' => 'forpayment',
        'action' => 'form'
    ));
	$router->add('for_payment/getForPaymentList', array(
        'controller' => 'forpayment',
        'action' => 'getForPaymentList'
    ));

    $router->add('verify_payment', array(
        'controller' => 'verifypayment',
        'action' => 'index'
    )); 
    $router->add('verify_payment/form', array(
        'controller' => 'verifypayment',
        'action' => 'form'
    ));
	$router->add('verify_payment/getVerifyPaymentList', array(
        'controller' => 'verifypayment',
        'action' => 'getVerifyPaymentList'
    ));
	$router->add('verify_payment/updateStatus', array(
        'controller' => 'verifypayment',
        'action' => 'updateStatus'
    ));
	$router->add('verify_payment/finalizeOrder', array(
        'controller' => 'verifypayment',
        'action' => 'finalizeOrder'
    ));

    $router->add('partially_verified', array(
        'controller' => 'partiallyverified',
        'action' => 'index'
    ));
    $router->add('partially_verified/form', array(
        'controller' => 'partiallyverified',
        'action' => 'form'
    ));
	$router->add('partially_verified/getPartialPaymentList', array(
        'controller' => 'partiallyverified',
        'action' => 'getPartialPaymentList'
    ));

	$router->add('partially_verified/addCreditMemo', array(
        'controller' => 'partiallyverified',
        'action' => 'addCreditMemo'
    ));

    $router->add('partially_verified/getCreditMemo', array(
        'controller' => 'partiallyverified',
        'action' => 'getCreditMemo'
    ));


    $router->add('for_delivery_pickup', array(
        'controller' => 'fordeliverypickup',
        'action' => 'index'
    )); 
    $router->add('for_delivery_pickup/form', array(
        'controller' => 'fordeliverypickup',
        'action' => 'form'
    ));
	$router->add('for_delivery_pickup/getForDeliveryList', array(
        'controller' => 'fordeliverypickup',
        'action' => 'getForDeliveryList'
    ));
	$router->add('for_delivery_pickup/markServed', array(
        'controller' => 'fordeliverypickup',
        'action' => 'markServed'
    ));


    $router->add('served', array(
        'controller' => 'served',
        'action' => 'index'
    ));
	$router->add('served/form', array(
        'controller' => 'served',
        'action' => 'form'
    ));
	$router->add('served/getServedList', array(
        'controller' => 'served',
        'action' => 'getServedList'
    ));
	
    $router->add('expired', array(
        'controller' => 'expired',
        'action' => 'index'
    )); 
    $router->add('archived', array(
        'controller' => 'archived',
        'action' => 'index'
    )); 

    //Products Section
    $router->add('products', array(
        'controller' => 'products',
        'action' => 'index'
    )); 
    $router->add('products/add_new', array(
        'controller' => 'products',
        'action' => 'add_new'
    )); 
	$router->add('products/addProduct', array(
        'controller' => 'products',
        'action' => 'addProduct'
    ));
	
    $router->add('products/edit', array(
        'controller' => 'products',
        'action' => 'edit_prod'
    ));

	$router->add('products/updateProduct', array(
        'controller' => 'products',
        'action' => 'updateProduct'
    ));
	
	$router->add('products/getProductsList', array(
        'controller' => 'products',
        'action' => 'getProductsList'
    ));
	$router->add('products/deleteProduct', array(
        'controller' => 'products',
        'action' => 'deleteProduct'
    ));
	$router->add('products/toggleDisplayStatus', array(
        'controller' => 'products',
        'action' => 'toggleDisplayStatus'
    ));	
    
    $router->add('categories', array(
        'controller' => 'categories',
        'action' => 'index'
    )); 
    $router->add('groups', array(
        'controller' => 'groups',
        'action' => 'index'
    )); 
    $router->add('brands', array(
        'controller' => 'brands',
        'action' => 'index'
    )); 
    $router->add('pricing', array(
        'controller' => 'pricing',
        'action' => 'index'
    ));
	$router->add('pricing/getPricingList', array(
        'controller' => 'pricing',
        'action' => 'getPricingList'
    ));
	$router->add('pricing/deletePricing', array(
        'controller' => 'pricing',
        'action' => 'deletePricing'
    ));
	$router->add('pricing/savePricing', array(
        'controller' => 'pricing',
        'action' => 'savePricing'
    ));
	
    $router->add('city_charges', array(
        'controller' => 'citycharges',
        'action' => 'index'
    ));
	$router->add('city_charges/getCityChargesList', array(
        'controller' => 'citycharges',
        'action' => 'getCityChargesList'
    ));
	$router->add('city_charges/deleteCityCharge', array(
        'controller' => 'citycharges',
        'action' => 'deleteCityCharge'
    ));
	$router->add('city_charges/saveCityCharge', array(
        'controller' => 'citycharges',
        'action' => 'saveCityCharge'
    ));
	
	
    $router->add('trucks', array(
        'controller' => 'trucks',
        'action' => 'index'
    )); 
    $router->add('users', array(
        'controller' => 'users',
        'action' => 'index'
    )); 
    $router->add('roles', array(
        'controller' => 'roles',
        'action' => 'index'
    )); 
    $router->add('company', array(
        'controller' => 'company',
        'action' => 'index'
    )); 
    $router->add('logs', array(
        'controller' => 'logs',
        'action' => 'index'
    )); 
    $router->add('settings', array(
        'controller' => 'settings',
        'action' => 'index'
    )); 

    $router->add('special_inquiries', array(
        'controller' => 'specialinquiries',
        'action' => 'index'
    ));
    $router->add('special_inquiries/si_form', array(
        'controller' => 'specialinquiries',
        'action' => 'si_form'
    )); 
    $router->add('special_inquiries/create_quotation', array(
        'controller' => 'specialinquiries',
        'action' => 'create_quotation'
    )); 
	$router->add('special_inquiries/getSpecialInquiriesList', array(
        'controller' => 'specialinquiries',
        'action' => 'getSpecialInquiriesList'
    ));
	$router->add('special_inquiries/search', array(
        'controller' => 'specialinquiries',
        'action' => 'search'
    ));
	$router->add('special_inquiries/createInquiry', array(
        'controller' => 'specialinquiries',
        'action' => 'createInquiry'
    ));
	$router->add('special_inquiries/decline', array(
        'controller' => 'specialinquiries',
        'action' => 'decline'
    ));
	$router->add('special_inquiries/toggleFor', array(
        'controller' => 'specialinquiries',
        'action' => 'toggleFor'
    ));
	

    //All
    $router->add('all/getAllList', array(
        'controller' => 'all',
        'action' => 'getAllList'
    ));	 

      $router->add('all/checkforpayment', array(
        'controller' => 'all',
        'action' => 'checkforpayment'
    ));  

    // search for provinces
    // $router->add('all/getprovinces', array(
    //     'controller' => 'all',
    //     'action' => 'getProvinces'
    // ));

    // $router->add('all/getcities', array(
    //     'controller' => 'all',
    //     'action' => 'getCities'
    // ));  
    
    //Users
    $router->add('users/getUsersAdminList', array(
        'controller' => 'users',
        'action' => 'getUsersAdminList'
    )); 
    $router->add('users/customers', array(
        'controller' => 'users',
        'action' => 'customers'
    ));
    $router->add('users/getCustomerList', array(
        'controller' => 'users',
        'action' => 'getCustomerList'
    ));
    $router->add('users/new_admin', array(
        'controller' => 'users',
        'action' => 'new_admin'
    ));
    $router->add('users/edit_admin', array(
        'controller' => 'users',
        'action' => 'edit_admin'
    ));

    $router->add('users/new_customer', array(
        'controller' => 'users',
        'action' => 'new_customer'
    ));

    $router->add('users/edit_customer', array(
        'controller' => 'users',
        'action' => 'edit_customer'
    ));
    $router->add('users/newAdminUser', array(
        'controller' => 'users',
        'action' => 'newAdminUser'
    ));
    $router->add('users/editAdminUser', array(
        'controller' => 'users',
        'action' => 'editAdminUser'
    ));
    $router->add('users/deleteAdminUser', array(
        'controller' => 'users',
        'action' => 'deleteAdminUser'
    )); 
    $router->add('users/newCustomerIndiv', array(
        'controller' => 'users',
        'action' => 'newCustomerIndiv'
    ));
    $router->add('users/editCustomerIndiv', array(
        'controller' => 'users',
        'action' => 'editCustomerIndiv'
    ));
    $router->add('users/deleteCustomerIndiv', array(
        'controller' => 'users',
        'action' => 'deleteCustomerIndiv'
    ));
    $router->add('users/newCustomerContractor', array(
        'controller' => 'users',
        'action' => 'newCustomerContractor'
    ));
    $router->add('users/editCustomerContractor', array(
        'controller' => 'users',
        'action' => 'editCustomerContractor'
    ));
    $router->add('users/deleteCustomerContractor', array(
        'controller' => 'users',
        'action' => 'deleteCustomerContractor'
    ));
    $router->add('users/newCustomerSeller', array(
        'controller' => 'users',
        'action' => 'newCustomerSeller'
    ));
    $router->add('users/editCustomerSeller', array(
        'controller' => 'users',
        'action' => 'editCustomerSeller'
    ));
    $router->add('users/deleteCustomerSeller', array(
        'controller' => 'users',
        'action' => 'deleteCustomerSeller'
    ));
    
	$router->add('users/getProvinces', array(
        'controller' => 'users',
        'action' => 'getProvinces'
    ));
	$router->add('users/getCities', array(
        'controller' => 'users',
        'action' => 'getCities'
    ));

    //Upload Proof of business
    $router->add('upload/uploadPermit', array(
        'controller' => 'uploadpermit',
        'action' => 'uploadPermit'
    ));
    
//Roles
    $router->add('roles/getRoleList', array(
        'controller' => 'roles',
        'action' => 'getRoleList'
    )); 
    $router->add('roles/saveRole', array(
        'controller' => 'roles',
        'action' => 'saveRole'
    ));
    $router->add('roles/editRole', array(
        'controller' => 'roles',
        'action' => 'editRole'
    )); 
    $router->add('roles/deleteRole', array(
        'controller' => 'roles',
        'action' => 'deleteRole'
    ));    
    

//Categories
    $router->add('categories/getCategoryList', array(
        'controller' => 'categories',
        'action' => 'getCategoryList'
    )); 
    $router->add('categories/saveNewCategory', array(
        'controller' => 'categories',
        'action' => 'saveNewCategory'
    ));
    $router->add('categories/editCategory', array(
        'controller' => 'categories',
        'action' => 'editCategory'
    )); 
    $router->add('categories/deleteCategory', array(
        'controller' => 'categories',
        'action' => 'deleteCategory'
    ));
    
//Groups
    $router->add('groups/getGroupList', array(
        'controller' => 'groups',
        'action' => 'getGroupList'
    ));
    $router->add('groups/saveNewGroup', array(
        'controller' => 'groups',
        'action' => 'saveNewGroup'
    ));
    $router->add('groups/editGroup', array(
        'controller' => 'groups',
        'action' => 'editGroup'
    )); 
    $router->add('groups/deleteGroup', array(
        'controller' => 'groups',
        'action' => 'deleteGroup'
    ));    
    
//Brands
    $router->add('brands/getBrandList', array(
        'controller' => 'brands',
        'action' => 'getBrandList'
    ));
    $router->add('brands/saveNewBrand', array(
        'controller' => 'brands',
        'action' => 'saveNewBrand'
    ));
    $router->add('brands/editBrand', array(
        'controller' => 'brands',
        'action' => 'editBrand'
    )); 
    $router->add('brands/deleteBrand', array(
        'controller' => 'brands',
        'action' => 'deleteBrand'
    ));  
    
//Trucks
    $router->add('trucks/getTruckList', array(
        'controller' => 'trucks',
        'action' => 'getTruckList'
    ));     
    $router->add('trucks/saveNewTruck', array(
        'controller' => 'trucks',
        'action' => 'saveNewTruck'
    ));
    $router->add('trucks/editTruck', array(
        'controller' => 'trucks',
        'action' => 'editTruck'
    )); 
    $router->add('trucks/deleteTruck', array(
        'controller' => 'trucks',
        'action' => 'deleteTruck'
    ));
    
//Logs
    $router->add('logs/storefront', array(
        'controller' => 'logs',
        'action' => 'storefront'
    ));
    $router->add('logs/getLogsList', array(
        'controller' => 'logs',
        'action' => 'getLogsList'
    ));    
    
//Settings
    $router->add('settings/saveNumSettings', array(
        'controller' => 'settings',
        'action' => 'saveNumSettings'
    ));

    $router->add('settings/getBlockedKeywords', array(
        'controller' => 'settings',
        'action' => 'getBlockedKeywords'
    )); 
    
    $router->add('settings/saveTextSettings', array(
        'controller' => 'settings',
        'action' => 'saveTextSettings'
    ));   
    
    $router->add('settings/saveCalendarSettings', array(
        'controller' => 'settings',
        'action' => 'saveCalendarSettings'
    )); 
    
    $router->add('settings/saveBlockedUsers', array(
        'controller' => 'settings',
        'action' => 'saveBlockedUsers'
    ));    
    
//Expired
    $router->add('expired/getExpiredList', array(
        'controller' => 'expired',
        'action' => 'getExpiredList'
    ));
    
//Archived
    $router->add('archived/getArchivedList', array(
        'controller' => 'archived',
        'action' => 'getArchivedList'
    ));
    
    $router->add('archived/special_inquiries', array(
        'controller' => 'archived',
        'action' => 'special_inquiries'
    )); 
    
    $router->add('archived/getArchivedSpecialList', array(
        'controller' => 'archived',
        'action' => 'getArchivedSpecialList'
    ));    

	$router->add('company/applyCompany', array(
        'controller' => 'company',
        'action' => 'applyCompany'
    ));
	
	$router->add('company/applyBanks', array(
        'controller' => 'company',
        'action' => 'applyBanks'
    ));
    
    return $router;
});
