<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$env = 'default';

if(@$_SERVER['HTTP_HOST'] == 'localhost')
{
    $env = 'localhost';
} else if(@$_SERVER['HTTP_HOST'] == '107.6.113.222:8884') {
      $env = 'newstaging';
} else if(@$_SERVER['HTTP_HOST'] == '107.6.113.222:8885') {
      $env = 'devinstance';
} else {

      $env = 'staging';
}

//add base uri environment here
$baseuri = array();
$storefronturi = array();

$baseuri['localhost'] = 'http://localhost/lpm_admin/';
$baseuri['devops'] = 'http://107.6.113.222:8807/lpm_admin/';
//$baseuri['staging'] = 'http://staging.lpmconstruction.ml/lpm_admin/';
$storefronturi['localhost'] = 'http://localhost/lpm_website/';
$storefronturi['devops'] = 'http://107.6.113.222:8807/lpm_website/';
$storefronturi['staging'] = 'http://staging.lpmconstruction.ml/lpm_website/';
$baseuri['robot'] = 'http://mr-robot.local:8888/lpm_admin/';
$storefonturi['robot'] = 'http://mr-robot.local:8888/lpm_website/';

$baseuri['newstaging'] = 'http://107.6.113.222:8884/lpm_admin/';
$baseuri['devinstance'] = 'http://107.6.113.222:8885/lpm_admin/';

$storefronturi['newstaging'] = 'http://107.6.113.222:8884/lpm_website/';
$storefronturi['devinstance'] = 'http://107.6.113.222:8885/lpm_website/';

$database['localhost'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
        'port' => '3306'
    );

$database['devops'] = array(
        'adapter'     => 'Mysql',
        'host'        => '107.6.113.222',
        'username'    => 'aws',
        'password'    => 'aws1234',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
        'port' => '6033'
);

$database['staging'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'lpm',
        'password'    => 'lpm12lpm34',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
        'port' => '3306'
);


$database['robot'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => 'root',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
);

$database['devinstance'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'lpm',
        'password'    => 'lpm12lpm34',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
);

$database['newstaging'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'lpm',
        'password'    => 'lpm12lpm34',
        'dbname'      => 'lpm',
        'charset'     => 'utf8',
);

define("BASE_URI",$baseuri[$env]);
define("STOREFRONT_URI",$storefronturi[$env]);

return new \Phalcon\Config(array(
    'database' => $database[$env],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => $baseuri[$env],
    )
));
