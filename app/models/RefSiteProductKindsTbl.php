<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RefSiteProductKindsTbl extends ModelBase {

    public function initialize() {
        $this->setSource('ref_site_product_kinds_tbl');
    }

	public function publishAll()
	{
		$sql = "UPDATE RefSiteProductKindsTbl
				SET published_status = 1
				WHERE display_status = 1 AND published_status = 0";
        
        $data = $this->modelsManager->executeQuery($sql);
        return $data;
	}

}
