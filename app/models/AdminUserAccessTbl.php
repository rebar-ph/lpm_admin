<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class AdminUserAccessTbl extends ModelBase {

    public function initialize() {
        $this->setSource('admin_user_access_tbl');
    }

    public function getUsersAdminList(){
        $data = new AdminUserAccessTbl();
        $sql = "SELECT *
                FROM admin_user_access_tbl auat
                LEFT JOIN ref_admin_access_type_tbl raatt ON auat.access_id = raatt.access_id
                WHERE auat.archive_flag = 0
                ORDER BY auat.user_id ASC";
        
        return new Resultset(null, $data, $data->getReadConnection()->query($sql));               
    }

}
