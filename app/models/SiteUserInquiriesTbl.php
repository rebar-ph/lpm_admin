<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class SiteUserInquiriesTbl extends ModelBase {

    public function initialize() {
        $this->setSource('site_user_inquiries_tbl');
    }
	
	public function beforeUpdate()
    {
        // Set the modification date
        $this->datetime_modified = date('Y-m-d H:i:s');
    }

	public function getVerified()
	{
		$sql = "SELECT suit.*
                FROM SiteUserInquiriesTbl suit
                LEFT JOIN RefSiteInquiryPaymentTbl rsipt ON suit.inquiry_id = rsipt.inquiry_id
                WHERE suit.archive_flag = 0 AND suit.status = 6 AND (rsipt.payment_status = 0 OR rsipt.payment_status is null)
				ORDER BY suit.reference_number DESC";
        
        $data = $this->modelsManager->executeQuery($sql);
        return $data;
	}
	
	public function getPartialVerified()
	{
		$sql = "SELECT suit.*
                FROM SiteUserInquiriesTbl suit
                LEFT JOIN RefSiteInquiryPaymentTbl rsipt ON suit.inquiry_id = rsipt.inquiry_id
                WHERE suit.archive_flag = 0 AND suit.status = 6 AND rsipt.payment_status > 0
				ORDER BY suit.reference_number DESC";
        
        $data = $this->modelsManager->executeQuery($sql);
        return $data;
	}

    public function getAll() {
        $sql = "SELECT suit.*,rsipt.payment_status
                FROM SiteUserInquiriesTbl suit
                LEFT JOIN RefSiteInquiryPaymentTbl rsipt ON suit.inquiry_id = rsipt.inquiry_id
                WHERE suit.archive_flag = 0
                ORDER BY suit.status ASC";
        
        $data = $this->modelsManager->executeQuery($sql);
        return $data;
    }

}
