<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RefSiteCategoryTbl extends ModelBase {

    public function initialize() {
        $this->setSource('ref_site_category_tbl');
    } 
    
//    public function getCategoryList(){
//        $data = new RefSiteCategoryTbl();
//        $sql = "SELECT rsct.category_id, rsct.category_name, rsct.group_id, rsgt.group_name, rsct.modified_on, rsct.archive_flag
//                FROM ref_site_category_tbl rsct
//                LEFT JOIN ref_site_group_tbl rsgt ON rsct.group_id = rsgt.group_id
//                WHERE rsct.archive_flag = '0'";
//        
//        return new Resultset(null, $data, $data->getReadConnection()->query($sql));               
//    }  
    
//    public function getCategoryList(){
//        $data = new RefSiteCategoryTbl();
//        $sql = "SELECT rsct.category_id, rsct.category_name, rsct.modified_on, rsct.archive_flag
//                FROM ref_site_category_tbl rsct
//                WHERE rsct.archive_flag = '0'";
//        
//        return new Resultset(null, $data, $data->getReadConnection()->query($sql));               
//    }       

}
