<?php

class RefRegionsTb extends ModelBase
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $country_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=false)
     */
    public $pos_lat;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=false)
     */
    public $pos_lng;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $status_flag;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date_created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date_updated;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_regions_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefRegionsTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefRegionsTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getCities($id) {
        $sql = "SELECT ref_regions_tb.id , ref_provinces_tb.id AS province_id , ref_provinces_tb.`name` AS province_name , ref_cities_tb.id AS city_id , ref_cities_tb.`name` AS city_name FROM ref_regions_tb JOIN ref_provinces_tb ON ref_regions_tb.id = ref_provinces_tb.region_id JOIN ref_cities_tb ON ref_provinces_tb.id = ref_cities_tb.province_id WHERE ref_regions_tb.country_id = 170 AND ref_provinces_tb.region_id = $id";

      $db = self::getConnection(); 
      $data = $db->query($sql); 
      $data->setFetchMode(\Phalcon\Db::FETCH_OBJ); 
      return $results = $data->fetchAll(); 

    }

}
