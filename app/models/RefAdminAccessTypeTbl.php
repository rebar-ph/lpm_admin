<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RefAdminAccessTypeTbl extends ModelBase {

    public function initialize() {
        $this->setSource('ref_admin_access_type_tbl');
    }
    
//    public function getRoleList(){
//        $data = new RefAdminAccessTypeTbl();
//        $sql = "SELECT raatt.access_id, raatt.access_name, raatt.modified_on, raatt.archive_flag
//                FROM ref_admin_access_type_tbl raatt
//                WHERE raatt.archive_flag = '0'";
//        
//        return new Resultset(null, $data, $data->getReadConnection()->query($sql));               
//    }     

}
