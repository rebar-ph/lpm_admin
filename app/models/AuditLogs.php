<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class AuditLogs extends ModelBase {

    public function initialize() {
        $this->setSource('audit_logs');
    }

    public function beforeValidation() {
        if (!$this->timestamp)
            $this->timestamp = new PhRawValue('default');
    }
    
    public function getLogsList(){
        $sql = "SELECT al.id, al.user_id, al.timestamp, al.action, auat.email
                FROM audit_logs al
                LEFT JOIN admin_user_access_tbl auat ON al.user_id = auat.user_id
                WHERE 1=1
                ORDER BY al.timestamp DESC";
        
        $db = self::getConnection();
        $data = $db->query($sql);
        return $results = $data->fetchAll(PDO::FETCH_OBJ);        
        
    }     

}
