<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class SiteUserAccessTbl extends ModelBase {

    public function initialize() {
        $this->setSource('site_user_access_tbl');
    }    

    public function getCustomerList(){
        $data = new SiteUserAccessTbl();
        $sql = "SELECT *
                FROM site_user_access_tbl suat
                LEFT JOIN ref_site_customer_type_tbl rsctt ON suat.customer_type_id = rsctt.customer_type_id
                WHERE suat.archive_flag = 0
                ORDER BY suat.user_id ASC";
        
        return new Resultset(null, $data, $data->getReadConnection()->query($sql));               
    }     
    
}
