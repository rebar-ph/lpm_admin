<?php

class RefCitiesTb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=9, nullable=false)
     */
    public $province_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=false)
     */
    public $pos_lat;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=false)
     */
    public $pos_lng;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $status_flag;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date_created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date_updated;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_cities_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefCitiesTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefCitiesTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
