<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RefSiteProductTbl extends ModelBase {

    public function initialize() {
        $this->setSource('ref_site_product_tbl');
    }

	public function getProductsList()
	{
		$phql = "SELECT rspt.*, rspkt.*
				FROM RefSiteProductTbl rspt
				LEFT JOIN RefSiteProductKindsTbl rspkt ON rspkt.product_id = rspt.product_id
				WHERE rspt.archive_flag = 0 and rspkt.archive_flag = 0
				ORDER BY rspt.product_name ASC";
				
		$data = $this->modelsManager->executeQuery($phql);
        return $data;
	}

}
