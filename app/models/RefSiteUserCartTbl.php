<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RefSiteUserCartTbl extends ModelBase {

    public function initialize() {
        $this->setSource('ref_site_user_cart_tbl');
    }    

}
