<?php

class RefSiteInquiryPaymentTransactionsTbl extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $paymain_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $bank_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date_paid;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $transaction_no;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $amount;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $file;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $status;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_site_inquiry_payment_transactions_tbl';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefSiteInquiryPaymentTransactionsTbl[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefSiteInquiryPaymentTransactionsTbl
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
