<?php

class CreditMemoTb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $credit_memo_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=9, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=9, nullable=false)
     */
    public $admin_user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $reference_number;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $type;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $amount;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $reason;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $datetime_created;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'credit_memo_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CreditMemoTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CreditMemoTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
