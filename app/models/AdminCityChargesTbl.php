<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
    Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
    Phalcon\Db\RawValue as PhRawValue,
    Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class AdminCityChargesTbl extends ModelBase {

    public function initialize() {
        $this->setSource('admin_city_charges_tbl');
    }

	public function beforeUpdate()
    {
        // Set the modification date
        $this->date_modified_on = date('Y-m-d H:i:s');
    }
	
	public function insertRows($ref_cities_array, $truck_type_id)
	{
		echo "Insert Row";
		$servername = $this->config->database->host;
		$username = $this->config->database->username;
		$password = $this->config->database->password;
		$db = $this->config->database->dbname;
		$port = $this->config->database->port;
		$mysqli = new mysqli($servername, $username, $password, $db, $port);
		
		$region_id=0;
		
		
		echo "Before Connected";
		/* check connection */
		if ($mysqli->connect_errno) {
			die("Connection Error " . $mysqli->connect_error);
			//printf("Connect failed: %s\n", $mysqli->connect_error);
			//exit();
		}
		echo "Connected";
		
		$query = "INSERT INTO admin_city_charges_tbl (region_id, province_id, city_id, truck_type_id, wheel_charge, wheel_restriction) VALUES (1, 1, 1, 99, 0.0, 0)";
		$stmt = $mysqli->prepare($query);
		echo "After Prepare";
		
		//$stmt->bind_param("i", $region_id);
		echo "After Bind";
		
		$mysqli->query("START TRANSACTION");
		foreach ($ref_cities_array as $ref_city) {
			//$region_id = $ref_city['region_id'];
			//$province_id = $ref_city['province_id'];
			//$city_id = $ref_city['city_id'];
			
			$stmt->execute();
		}
		$stmt->close();
		$mysqli->query("COMMIT");
	}

}
