<?php

class AdminAllView extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $inquiry_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $ud_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $reference_number;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $valid_until;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $for;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $remarks;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $truckid;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $delivery_address_id;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $delivery_charge;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $discount;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $cwt;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $payment;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $bank;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $reason;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $soa_prepared_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $soa_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $approve_soa_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_served;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_expired;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_archived;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $datetime_created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $datetime_modified;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $prev_status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $recompute_status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $archive_flag;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $payment_status;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'admin_all_view';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminAllView[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminAllView
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
