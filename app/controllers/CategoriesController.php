<?php

class CategoriesController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'categories/index');
        $this->view->setVar('page_active', 'categories');
        $this->view->setVar('page_title', 'Categories');
//        $this->view->setVar('css', 'categories.css');
        
        $groups = RefSiteGroupTbl::find("archive_flag = '0'");
        $this->view->setVar('groups', $groups);        
    }
    
    public function getCategoryListAction(){
        $this->view->disable();
        
        $data = RefSiteCategoryTbl::find("archive_flag = '0'");
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    }
    
    public function saveNewCategoryAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        
        $data_row = new RefSiteCategoryTbl();
        $data_row->category_name = $post_data['category_name'];
        $data_row->group_id = $post_data['group_id'];
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD CATEGORY");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
            echo "success";
        }   
        
    } 
    
    public function editCategoryAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $category_id = $post_data['category_id'];
        
        $data_row = RefSiteCategoryTbl::findFirst("category_id = '$category_id'");
        $data_row->category_name = $post_data['category_name'];
        $data_row->group_id = $post_data['group_id'];
        $data_row->modified_on = date('Y-m-d H:i:s');
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT CATEGORY");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
            echo "success";
        }   
        
    }
    
    public function deleteCategoryAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $category_id = $post_data['category_id'];

        $data_row = RefSiteCategoryTbl::findFirst("category_id = '$category_id'");
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE CATEGORY");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
            echo "success";
        }   
        
    }      

}
