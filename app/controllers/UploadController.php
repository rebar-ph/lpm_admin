<?php

class UploadController extends \Phalcon\Mvc\Controller
{
    
    public function get_random_filename()
    {
        $date   = Date('Y-m-d H:i:s');
        $length = 20;
        $key    = '';
        $keys   = array_merge(range(0, 9), range('a', 'z'));
        
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key.md5($date);
    }
    
    public function uploadPermitAction()
    {
        $this->view->disable();
            if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
//                $baseLocation = 'uploads/junk/';
                $baseLocation = '../../lpm_website/public/uploads/junk/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                echo $unique_filename . "." . $ext;
            }
        }
    }

    public function uploadPaymentSlipAction()
    {
        $this->view->disable();
            if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
                $baseLocation = 'uploads/slips/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                echo $unique_filename . "." . $ext;
                //return $unique_filename . "." . $ext;
            }
        }
    }
	
	public function uploadBatchFileAction()
    {
        $this->view->disable();
            if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
                $baseLocation = 'uploads/temp/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                echo $unique_filename . "." . $ext;
                //return $unique_filename . "." . $ext;
            }
        }
    }

    public function testAction() {
         $products = SiteProductsView::findBygroup_name($query);
         echo json_encode($products);
    }
    
}