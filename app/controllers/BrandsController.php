<?php

class BrandsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'brands/index');
        $this->view->setVar('page_active', 'brands');
        $this->view->setVar('page_title', 'Brands');

//        $this->view->setVar('css', 'brands.css');
    }
    
    public function getBrandListAction(){
        $this->view->disable();
        
        $data = RefSiteBrandTbl::find("archive_flag = '0'");
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    } 
    
    public function saveNewBrandAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/brands/';
        
        if ($this->request->hasFiles(true) == true) {
            foreach ($this->request->getUploadedFiles() as $file) { 
                $filename = $file->getName();               
                $file->moveTo($baseLocation . $filename);              
            }
        }  
        
        $data_row = new RefSiteBrandTbl();
        $data_row->brand_name = $post_data['brand_name'];
        $data_row->brand_image_url = $filename;
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD BRAND");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/  
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'brands', true);  
        }   
        
    } 
    
    public function editBrandAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/brands/';
        $id = $post_data['brand_id']; 
        
        $data_row = RefSiteBrandTbl::findFirst("brand_id = '$id'");
        $data_row->brand_name = $post_data['brand_name'];
        if ($this->request->hasFiles(true) == true) { 
            unlink($baseLocation.$data_row->brand_image_url);           
            foreach ($this->request->getUploadedFiles() as $file) { 
                $filename = $file->getName();                
                $file->moveTo($baseLocation . $filename);              
            }           
            $data_row->brand_image_url = $filename;    
        }            
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{   
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT BRAND");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'brands', true);  
        }
    }
    
    public function deleteBrandAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/brands/';
        $id = $post_data['brand_id'];
        
        
        $data_row = RefSiteBrandTbl::findFirst("brand_id = '$id'");
            unlink($baseLocation.$data_row->brand_image_url);
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE BRAND");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'brands', true); 
        }   
        
    }     

}
