<?php

class TrucksController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'trucks/index');
        $this->view->setVar('page_active', 'trucks');
        $this->view->setVar('page_title', 'Trucks');

//        $this->view->setVar('css', 'trucks.css');
    }
    
    public function getTruckListAction(){
        $this->view->disable();
        
        $data = AdminTruckTypesTbl::find("archive_flag = '0' ORDER BY min_points ASC");
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    }    

    public function saveNewTruckAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        
        $data_row = new AdminTruckTypesTbl();
        $data_row->truck_type = $post_data['truck_type'];
        $data_row->min_points = $post_data['min_points'];
        $data_row->max_points = $post_data['max_points'];
        $data_row->datetime_created = date('Y-m-d H:i:s');
        $data_row->datetime_modified = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD TRUCK");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/ 			
			
			
            echo "success";
        }
    }     
    
    public function editTruckAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $id = $post_data['id'];
        
        $data_row = AdminTruckTypesTbl::findFirst("id = '$id'");
        $data_row->truck_type = $post_data['truck_type'];
        $data_row->min_points = $post_data['min_points'];
        $data_row->max_points = $post_data['max_points'];
        $data_row->datetime_modified = date('Y-m-d H:i:s');
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT TRUCK");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
            echo "success";
        }           
    }
    
    public function deleteTruckAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $id = $post_data['id'];

        $data_row = AdminTruckTypesTbl::findFirst("id = '$id'");
        $data_row->datetime_modified = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE TRUCK");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
            echo "success";
        }          
    }       
    
}
