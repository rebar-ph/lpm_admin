<?php
use MailerController as Mailer;
class VerifyPaymentController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'verify_payment/index');
        $this->view->setVar('page_active', 'verify_payment');
        $this->view->setVar('page_title', 'For Verification');

       	// $this->view->setVar('css', 'verify_payment.css');
    }

    public function formAction() {
		
		$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		$prepared_by = $inquiry->soa_prepared_by;
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$users_info = SiteUserAccessTbl::find();
		$users_info_array = array();
		foreach ($users_info as $user_info)
		{
			$users_info_array[$user_info->user_id] = $user_info->first_name . " " . $user_info->last_name;
		}
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		$cwt_status = RefSiteUserProofDocumentsTbl::findFirst("user_id = $user_id and document_type = 2");
		if($prepared_by){
			$admin = AdminUserAccessTbl::findFirst("user_id = $prepared_by");
		}
		
		$payment = RefSiteInquiryPaymentTbl::findFirstByinquiry_id($inquiry->inquiry_id);
		
		$payment_details = RefSiteInquiryPaymentTxnsTbl::findBypaymain_id($payment->id);
		
		$banks = RefBanks::find();
		$banks_array = array();
		foreach ($banks as $bank)
		{
			$banks_array[$bank->id] = $bank;
		}
		$logged_in_user_id = $this->session->LPMADMINSESSION['user_id'];
		
		$admin_users = AdminUserAccessTbl::find();
		$admin_users_array = array();
		foreach ($admin_users as $admin_user)
		{
			$admin_users_array[$admin_user->user_id] = $admin_user->first_name . " " . $admin_user->last_name;
		}
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('payment', $payment);
		$this->view->setVar('payment_details', $payment_details);
		$this->view->setVar('banks_array', $banks_array);
		$this->view->setVar('users_info_array', $users_info_array);
		$this->view->setVar('logged_in_user_id', $logged_in_user_id);
		$this->view->setVar('admin_users_array', $admin_users_array);
		$this->view->setVar('banks', $banks);
		$this->view->setVar('cwt_status', $cwt_status);
		$this->view->setVar('admin', $admin);
		
        $this->view->setVar('page_content', 'verify_payment/form');
        $this->view->setVar('page_active', 'verify_payment');
        $this->view->setVar('page_title', 'For Verification');

       	$this->view->setVar('css', 'forms.css');
    }
	
	public function getVerifyPaymentListAction() {
	
		$this->view->disable();

		$inquiries = SiteUserInquiriesTbl::getVerified();

		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
	
	}
	
	public function updateStatusAction() {
		$this->view->disable();
		
		$payment_detail_id = $_POST['payment_detail_id'];
		$new_status = $_POST['new_status'];
		$decline_reason = $_POST['decline_reason'];
		$date_verified = $_POST['date_verified'];
		$checked_via = $_POST['checked_via'];
		
		$amount_due = 0;
		$amount_paid = 0;
		if ($_POST['amount_due']) $amount_due = $_POST['amount_due'];
		if ($_POST['amount_paid']) $amount_paid = $_POST['amount_paid'];
		
		$payment_status = 0;
		if ($amount_due == $amount_paid) $payment_status = 2;
		else if (($amount_due > $amount_paid) && ($amount_paid > 0)) $payment_status = 1;
		
		
		$payment_detail = RefSiteInquiryPaymentTxnsTbl::findFirst("id=$payment_detail_id");
		$payment_detail->checked_by = $this->session->LPMADMINSESSION['user_id'];
		$payment_detail->decline_reason = $decline_reason;
		$payment_detail->date_verified = $date_verified;
		$payment_detail->checked_via = $checked_via;
		$payment_detail->status = $new_status;
		if ($payment_detail->save() == false)
		{
			foreach ($payment_detail->getMessages() as $msg)
			{
				echo $msg;
			}
		} 

		if ($payment_status > 0)
		{
			$payment_main_id = $payment_detail->paymain_id;
			$payment_main = RefSiteInquiryPaymentTbl::findFirst("id = $payment_main_id");
			$payment_main->payment_status = $payment_status;
			$payment_main->save();

		}

		$bank_id = $payment_detail->bank_id;
		$banks = RefBanks::findFirst("id = $bank_id");

		//Set details
		$bank_name = $banks->bank_name;
		$date_paid = $payment_detail->date_paid;
		$trans_no = $payment_detail->transaction_no;
		$amount = $payment_detail->amount;

		$paymain_id = $payment_detail->paymain_id;
		$payments = RefSiteInquiryPaymentTbl::findFirst("id = $paymain_id");
		$inquiry_id = $payments->inquiry_id;

		$inquiry_info = SiteUserInquiriesTbl::findFirst("inquiry_id = $inquiry_id");
		$order_no = $inquiry_info->reference_number;
		$ud_id = $inquiry_info->ud_id;
		$soa_date = $inquiry_info->soa_date;

		$user_id = $inquiry_info->user_id;
		$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");
		$email = $user_info->email;

		$customer_type_id = $user_info->customer_type_id;

		if($user_info->customer_type_id != 1){

			$fullname = $user_info->company_name;

		}else{
			$fullname = $user_info->first_name." ".$user_info->last_name;
		}
		
		if ($payment_detail->status == 1){

			$mailer = new Mailer();
			$mailer->sendVerifyPaymentAction($email, $fullname, $bank_name, $date_paid, $trans_no, $amount, $order_no, $ud_id, $soa_date, $customer_type_id);

			echo "Verified";

		} else if ($payment_detail->status == 2) {

			$mailer = new Mailer();
			$mailer->sendDeclinedPaymentAction($email, $fullname, $bank_name, $date_paid, $trans_no, $amount, $order_no, $ud_id, $soa_date, $customer_type_id, $decline_reason);
			
			echo "Declined";
		}
		else {
			echo "Something Went Wrong";
		}
                
                $audit_action = "";
                switch ($payment_detail->status) {
                    case "1":
                        $audit_action = "VERIFY PAYMENT";
                        break;
                    case "2":
                        $audit_action = "DECLINE PAYMENT";
                        break;
                    default:
                        break;
                }
                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "$audit_action");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                

	}
	
	public function finalizeOrderAction() {
		$this->view->disable();
		
		$inquiry_id = $_POST['inquiry_id'];
		$inquiry = SiteUserInquiriesTbl::findFirst("inquiry_id = $inquiry_id");
                $inquiry->datetime_created = date('Y-m-d H:i:s');
		$inquiry->status = 7;
		$inquiry->save();

		$order_no = $inquiry->reference_number;
		$ud_id = $inquiry->ud_id;
		$soa_date = $inquiry->soa_date;

		$user_id = $inquiry->user_id;
		$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");
		$email = $user_info->email;
		$customer_type = $user_info->customer_type_id;

		if($user_info->customer_type_id != 1){
			$fullname = $user_info->company_name;
		}else{
			$fullname = $user_info->first_name." ".$user_info->last_name;
		}

		if($inquiry->for == 1){

			$mailer = new Mailer();
			$mailer->sendForDeliveryAction($email, $fullname, $order_no, $ud_id, $soa_date, $customer_type);
		}
		else if($inquiry->for == 2){

			$mailer = new Mailer();
			$mailer->sendForPickupAction($email, $fullname, $order_no, $ud_id, $soa_date, $customer_type);
		}

                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "FINALIZE ORDER");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                  
                
		echo "Success";
	}

}
