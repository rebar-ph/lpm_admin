<?php

class RolesController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'roles/index');
        $this->view->setVar('page_active', 'roles');
        $this->view->setVar('page_title', 'Roles');

        $this->view->setVar('css', 'roles.css');
    }
    
    public function getRoleListAction(){
        $this->view->disable();
        
        $data = RefAdminAccessTypeTbl::find("archive_flag = '0'");
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    }
    
    public function saveRoleAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();

        $exist = RefAdminAccessTypeTbl::findFirstByaccess_name($post_data['access_name']);
        if($exist->archive_flag == 1) {
                  $exist->delete();
        }

        $data_row = new RefAdminAccessTypeTbl();
        $data_row->access_name = $post_data['access_name'];
        $data_row->orders = $post_data['orders'];
        $data_row->soa = $post_data['soa'];
        $data_row->payment = $post_data['payment'];
        $data_row->products = $post_data['products'];
        $data_row->pricing = $post_data['pricing'];
        $data_row->users = $post_data['users'];
        $data_row->settings = $post_data['settings'];
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD ROLE");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/            
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'roles', true);             
        }       
    }
    
    public function editRoleAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $id = $post_data['access_id'];
        
        $data_row = RefAdminAccessTypeTbl::findFirst("access_id = '$id'");
        $data_row->access_name = $post_data['access_name'];
        $data_row->orders = $post_data['orders'];
        $data_row->soa = $post_data['soa'];
        $data_row->payment = $post_data['payment'];
        $data_row->products = $post_data['products'];
        $data_row->pricing = $post_data['pricing'];
        $data_row->users = $post_data['users'];
        $data_row->settings = $post_data['settings'];
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT ROLE");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'roles', true);             
        }         
    }
    
    
    public function deleteRoleAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $id = $post_data['access_id'];

        $data_row = RefAdminAccessTypeTbl::findFirst("access_id = '$id'");
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE ROLE");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'roles', true); 
        }   
        
    } 
    

}
