<?php

class LogsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'logs/index');
        $this->view->setVar('page_active', 'logs');
        $this->view->setVar('page_title', 'Logs');

//        $this->view->setVar('css', 'logs.css');
    }
    
    public function storefrontAction() {
        $this->view->setVar('page_content', 'logs/admin');
        $this->view->setVar('page_active', 'logs');
        $this->view->setVar('page_title', 'Logs');

//        $this->view->setVar('css', 'logs.css');
    }
    
    public function getLogsListAction(){
        $this->view->disable();
        $logs = AuditLogs::getLogsList();
        $logs_array = array();

        foreach ($logs as $log)
        {
                $logs_array[] = $log;
        }

        echo json_encode($logs_array);        
    }

}
