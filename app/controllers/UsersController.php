<?php
use Phalcon\Security\Random;
use MailerController as Mailer;
class UsersController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'users/index');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
    }

    public function new_adminAction() {
        $this->view->setVar('page_content', 'users/new-admin');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
        $this->view->setVar('js', 'users.js');
        
        $roles = RefAdminAccessTypeTbl::find("archive_flag = '0'");
        $this->view->setVar('roles', $roles);
        
        $admin_users = AdminUserAccessTbl::find("archive_flag = 0");
        $email_arr = array();
        foreach ($admin_users as $row){
            $email_arr[] = $row->email;
        }
        $this->view->setVar("email_arr", $email_arr);
        
    }

    public function edit_adminAction() {
        $this->view->setVar('page_content', 'users/edit-admin');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
        $this->view->setVar('js', 'users.js');
 
        $id = $this->request->getQuery("id");
        $user_info = AdminUserAccessTbl::findFirst("user_id = '$id'");
        $this->view->setVar('user_info', $user_info); 
        
        $roles = RefAdminAccessTypeTbl::find("archive_flag = '0'");
        $this->view->setVar('roles', $roles);
        
        $updater = AdminUserAccessTbl::findFirst("user_id = '$user_info->modified_by'");
        $this->view->setVar('updater', $updater->first_name.' '.$updater->last_name);
        
    }
    
    public function customersAction() {
        $this->view->setVar('page_content', 'users/customers');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
    }

    public function new_customerAction() {
        $this->view->setVar('page_content', 'users/new_customer');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
        $this->view->setVar('js', 'users.js');
        
        $regions = RefRegionsTb::find("country_id = '170'");
        $this->view->setVar('regions', $regions);
        
        $cities = RefCitiesTb::find("province_id = '47'");
        $this->view->setVar('cities', $cities);
        
        $provinces = RefProvincesTb::find("region_id = '117'");
        $this->view->setVar('provinces', $provinces);

        $this->assets->addJs('plugins/upload/filestyle.js');
        $this->assets->addJs('plugins/upload/upload.js');
    }

    public function edit_customerAction() {
        
        $user_id = $this->request->getQuery("id");
        $user_info = SiteUserAccessTbl::findFirst("user_id=$user_id");

        $user_address = RefUserAddressTbl::findFirst("user_id=$user_id");
        $region_id = $user_address->region;
        $province_id = $user_address->province;

        $regions = RefRegionsTb::find("country_id = '170'");
        $provinces = RefProvincesTb::find("region_id=$region_id");
        $cities = RefCitiesTb::find("province_id=$province_id");
        
        $bfile_info = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '0'");
        $this->view->setVar('bfile_info',$bfile_info);
        $sfile_info = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '1'");
        $this->view->setVar('sfile_info',$sfile_info);
        $tfile_info = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '2'");
        $this->view->setVar('tfile_info',$tfile_info);

        //echo substr($user_info->birthdate,5,2); die;

        $this->view->setVar('user_info',$user_info);
        $this->view->setVar('user_address',$user_address);
        $this->view->setVar('regions', $regions);
        $this->view->setVar('provinces', $provinces);
        $this->view->setVar('cities', $cities);

        $this->view->setVar('page_content', 'users/edit_customer');
        $this->view->setVar('page_active', 'users');
        $this->view->setVar('page_title', 'Users');

        $this->view->setVar('css', 'users.css');
        $this->view->setVar('js', 'users.js');
    }
    
    public function getUsersAdminListAction(){
        $this->view->disable();
        
        $data = AdminUserAccessTbl::getUsersAdminList();
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    }
    
    public function getCustomerListAction(){
        $this->view->disable();
        
        $data = SiteUserAccessTbl::getCustomerList();
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    }  
    
    public function newAdminUserAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        
        $data_row = new AdminUserAccessTbl();
        $data_row->first_name = $post_data['first_name'];
        $data_row->last_name = $post_data['last_name'];
        $data_row->email = $post_data['email'];
        $data_row->password = $this->security->hash($post_data['password']);
        $data_row->company_name = $post_data['company_name'];
        $data_row->mobile_no = $post_data['mobile_no'];
        if ($post_data['office_no']){
            $data_row->office_no = $post_data['office_no'];
        }
        if ($post_data['fax_no']){
            $data_row->fax_no = $post_data['fax_no'];
        }
        $data_row->access_id = $post_data['access_id'];
        $data_row->access_status = "0";
        $data_row->datetime_created = date('Y-m-d H:i:s');
        $data_row->modified_by = $modifier_id;
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD ADMIN USER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
            echo "success";
        }   
        
    }  
    
    public function editAdminUserAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        $user_id = $post_data['user_id'];
        
        $data_row = AdminUserAccessTbl::findFirst("user_id = '$user_id'");
        $data_row->first_name = $post_data['first_name'];
        $data_row->last_name = $post_data['last_name'];
        $data_row->email = $post_data['email'];
        if ($post_data['password']){
            $data_row->password = $this->security->hash($post_data['password']);
        }
        $data_row->company_name = $post_data['company_name'];
        $data_row->mobile_no = $post_data['mobile_no'];
        if ($post_data['office_no']){
            $data_row->office_no = $post_data['office_no'];
        }
        if ($post_data['fax_no']){
            $data_row->fax_no = $post_data['fax_no'];
        }
        $data_row->access_id = $post_data['access_id'];
        if ($post_data['access_status']){
            $data_row->access_status = "0";
        } else {
            $data_row->access_status = "1";
        }
        $data_row->modified_by = $modifier_id;
        $data_row->modified_on = date('Y-m-d H:i:s');
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{ 
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT ADMIN USER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
            echo "success";
        }   
        
    }
    
    public function deleteAdminUserAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        $user_id = $post_data['user_id'];

        $data_row = AdminUserAccessTbl::findFirst("user_id = '$user_id'");
        $data_row->modified_by = $modifier_id;
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{  
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE ADMIN USER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
            echo "success";
        }   
        
    }
    
    public function newCustomerIndivAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];

        $mailer = new Mailer();
        $random = new Random();
        $token = $random->uuid();

        $fullname = $post_data['first_name']." ".$post_data['last_name'];
        
        $data_row = new SiteUserAccessTbl();
		$data_row->customer_type_id = $post_data['customer_type_id'];
        $data_row->first_name = $post_data['first_name'];
        $data_row->last_name = $post_data['last_name'];
        $data_row->email = $post_data['email'];
        $data_row->password = $this->security->hash($post_data['password']);        
        $data_row->company_name = $post_data['company_name'];
        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
        $data_row->mobile_number = $post_data['mobile_number'];
        if ($post_data['office_number']){
            $data_row->office_number = $post_data['office_number'];
        }
        if ($post_data['fax_number']){
            $data_row->fax_number = $post_data['fax_number'];
        }
        $data_row->access_status = "1";
        $data_row->verification_token = $token;
        $data_row->datetime_created = date('Y-m-d H:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
            $data_row_address = new RefUserAddressTbl();
            $data_row_address->user_id = $data_row->user_id;
            $data_row_address->region = $post_data['region'];
            $data_row_address->province = $post_data['province'];
            $data_row_address->city = $post_data['city'];
            $data_row_address->address1 = $post_data['address1'];
            $data_row_address->address2 = $post_data['address2'];

            if ($data_row_address->save() == false ) {
                foreach ($data_row_address->getMessages() as $message) {
                    echo $message;
                    echo "\n";
                }
            } else{ 
                
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                
//                echo "success";
                $mailer->sendVerificationEmailAction($post_data['email'],$fullname,$token);
                $this->response->redirect(BASE_URI . 'users/customers', true); 
            } 
        }
    }

	public function editCustomerIndivAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];

	$user_id = $post_data['user_id'];
        $data_row = SiteUserAccessTbl::findFirst("user_id=$user_id");
		//$data_row->customer_type_id = $post_data['customer_type_id'];
//        $data_row->first_name = $post_data['first_name'];
//        $data_row->last_name = $post_data['last_name'];
//        $data_row->email = $post_data['email'];
//        $data_row->password = $this->security->hash($post_data['password']);        
//        $data_row->company_name = $post_data['company_name'];
//        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
//        $data_row->mobile_number = $post_data['mobile_number'];
//        if ($post_data['office_number']){
//            $data_row->office_number = $post_data['office_number'];
//        }
//        if ($post_data['fax_number']){
//            $data_row->fax_number = $post_data['fax_number'];
//        }
        $data_row->access_status = $post_data['access_status'];
        //$data_row->datetime_created = date('Y-m-d h:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
	$data_row->modified_by = $modifier_id;
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
//            $data_row_address = RefUserAddressTbl::findFirst("user_id=$user_id");
//			if (!$data_row_address)
//			{
//				$data_row_address = new RefUserAddressTbl();
//				$data_row_address->user_id = $user_id;
//			}
//            
//            $data_row_address->region = $post_data['region'];
//            $data_row_address->province = $post_data['province'];
//            $data_row_address->city = $post_data['city'];
//            $data_row_address->address1 = $post_data['address1'];
//            $data_row_address->address2 = $post_data['address2'];
//
//            if ($data_row_address->save() == false ) {
//                foreach ($data_row_address->getMessages() as $message) {
//                    echo $message;
//                    echo "\n";
//                }
//            } else{       
//                echo "success";
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/             
            
                $this->response->redirect(BASE_URI . 'users/customers', true); 
//            } 
        }
    }
	
	public function deleteCustomerIndivAction(){
		$this->view->disable();
		$post_data = $this->request->getPost();
		$user_id = $post_data['user_id'];
		$modifier_id = $this->session->LPMADMINSESSION['user_id'];
		
		$user_info = SiteUserAccessTbl::findFirst("user_id=$user_id");
		$user_info->modified_by = $modifier_id;
		$user_info->modified_on = date('Y-m-d H:i:s');
		$user_info->archive_flag = 1;
		$user_info->save();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                
                
		echo "Success";
		
	}
	
	public function newCustomerContractorAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];

        $mailer = new Mailer();
        $random = new Random();
        $token = $random->uuid();

        $fullname = $post_data['first_name']." ".$post_data['last_name'];
        
        $data_row = new SiteUserAccessTbl();
	    $data_row->customer_type_id = $post_data['customer_type_id'];
        $data_row->first_name = $post_data['first_name'];
        $data_row->last_name = $post_data['last_name'];
        $data_row->email = $post_data['email'];
        $data_row->password = $this->security->hash($post_data['password']);        
        $data_row->company_name = $post_data['company_name'];
        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
        $data_row->mobile_number = $post_data['mobile_number'];
        if ($post_data['office_number']){
            $data_row->office_number = $post_data['office_number'];
        }

        
        if ($post_data['fax_number']){
            $data_row->fax_number = $post_data['fax_number'];
        }
        $data_row->access_status = "1";
        $data_row->verification_token = $token;
        $data_row->datetime_created = date('Y-m-d H:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
            $data_row_address = new RefUserAddressTbl();
            $data_row_address->user_id = $data_row->user_id;
            $data_row_address->region = $post_data['region'];
            $data_row_address->province = $post_data['province'];
            $data_row_address->city = $post_data['city'];
            $data_row_address->address1 = $post_data['address1'];
            $data_row_address->address2 = $post_data['address2'];

            if ($data_row_address->save() == false ) {
                foreach ($data_row_address->getMessages() as $message) {
                    echo $message;
                    echo "\n";
                }
            } else{       
                //save filenames
                $bpermit = $this->request->getPost("bfile");
                $spermit = $this->request->getPost("sfile");
                $tpermit = $this->request->getPost("tfile");
                $BMonthExp = $this->request->getPost("BMonthExp"); 
                $BDayExp = $this->request->getPost("BDayExp"); 
                $BYearExp = $this->request->getPost("BYearExp");
                $SMonthExp = $this->request->getPost("SMonthExp"); 
                $SDayExp = $this->request->getPost("SDayExp"); 
                $SYearExp = $this->request->getPost("SYearExp");

                $files = array($bpermit,$spermit,$tpermit);
                $i = 0;
                foreach($files as $file) {
                    if($i == 0) {
                        $exp = $BYearExp."-".$BMonthExp."-".$BDayExp;
                    } else if($i == 1){
                        $exp = $SYearExp."-".$SMonthExp."-".$SDayExp;
                    } else {
                        $exp = "";
                    }

                    if ($file){
                        $p1 = new RefSiteUserProofDocumentsTbl();
                        $p1->user_id = $data_row->user_id;
                        $p1->expiration = $exp;
                        $p1->file = $file;
                        $p1->document_type = $i;
                        $p1->save();
                    }
                    $i += 1;
                    
                } 
                
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                $mailer->sendVerificationEmailAction($post_data['email'],$fullname,$token);
                $this->response->redirect(BASE_URI . 'users/customers', true); 
            } 
        }
    }
	
	public function editCustomerContractorAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        
	$user_id = $post_data['user_id'];
        $data_row = SiteUserAccessTbl::findFirst("user_id=$user_id");
		//$data_row->customer_type_id = $post_data['customer_type_id'];
//        $data_row->first_name = $post_data['first_name'];
//        $data_row->last_name = $post_data['last_name'];
//        $data_row->email = $post_data['email'];
//        $data_row->password = $this->security->hash($post_data['password']);        
//        $data_row->company_name = $post_data['company_name'];
//        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
//        $data_row->mobile_number = $post_data['mobile_number'];
//        if ($post_data['office_number']){
//            $data_row->office_number = $post_data['office_number'];
//        }
//        if ($post_data['fax_number']){
//            $data_row->fax_number = $post_data['fax_number'];
//        }
        $data_row->access_status = $post_data['access_status'];
        //$data_row->datetime_created = date('Y-m-d h:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
		$data_row->modified_by = $modifier_id;
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
//            $data_row_address = RefUserAddressTbl::findFirst("user_id=$user_id");
//			if (!$data_row_address)
//			{
//				$data_row_address = new RefUserAddressTbl();
//				$data_row_address->user_id = $user_id;
//			}
//            
//            $data_row_address->region = $post_data['region'];
//            $data_row_address->province = $post_data['province'];
//            $data_row_address->city = $post_data['city'];
//            $data_row_address->address1 = $post_data['address1'];
//            $data_row_address->address2 = $post_data['address2'];
//
//            if ($data_row_address->save() == false ) {
//                foreach ($data_row_address->getMessages() as $message) {
//                    echo $message;
//                    echo "\n";
//                }
//            } else{       
//                echo "success";
            
                if ($post_data['b_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '0'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }
                if ($post_data['s_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '1'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }
                if ($post_data['t_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '2'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }                

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                
                $mailer->sendVerificationEmailAction($post_data['email'],$fullname,$token);
                $this->response->redirect(BASE_URI . 'users/customers', true); 
//            } 
        }
    }
	
	public function deleteCustomerContractorAction(){
		$this->view->disable();
		$post_data = $this->request->getPost();
		$user_id = $post_data['user_id'];
		$modifier_id = $this->session->LPMADMINSESSION['user_id'];
		
		$user_info = SiteUserAccessTbl::findFirst("user_id=$user_id");
		$user_info->modified_by = $modifier_id;
		$user_info->modified_on = date('Y-m-d H:i:s');
		$user_info->archive_flag = 1;
		$user_info->save();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                
                
		echo "Success";
		
	}
	
	public function newCustomerSellerAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        
        $mailer = new Mailer();
        $random = new Random();
        $token = $random->uuid();

        $fullname = $post_data['first_name']." ".$post_data['last_name'];


        $data_row = new SiteUserAccessTbl();
	    $data_row->customer_type_id = $post_data['customer_type_id'];
        $data_row->first_name = $post_data['first_name'];
        $data_row->last_name = $post_data['last_name'];
        $data_row->email = $post_data['email'];
        $data_row->password = $this->security->hash($post_data['password']);        
        $data_row->company_name = $post_data['company_name'];
        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
        $data_row->mobile_number = $post_data['mobile_number'];
        if ($post_data['office_number']){
            $data_row->office_number = $post_data['office_number'];
        }
        if ($post_data['fax_number']){
            $data_row->fax_number = $post_data['fax_number'];
        }
        $data_row->access_status = "1";
        $data_row->verification_token = $token;
        $data_row->datetime_created = date('Y-m-d H:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
            $data_row_address = new RefUserAddressTbl();
            $data_row_address->user_id = $data_row->user_id;
            $data_row_address->region = $post_data['region'];
            $data_row_address->province = $post_data['province'];
            $data_row_address->city = $post_data['city'];
            $data_row_address->address1 = $post_data['address1'];
            $data_row_address->address2 = $post_data['address2'];

            if ($data_row_address->save() == false ) {
                foreach ($data_row_address->getMessages() as $message) {
                    echo $message;
                    echo "\n";
                }
            } else{
                //save filenames
                $bpermit = $this->request->getPost("bfile");
                $spermit = $this->request->getPost("sfile");
                $tpermit = $this->request->getPost("tfile");
                $BMonthExp = $this->request->getPost("BMonthExp"); 
                $BDayExp = $this->request->getPost("BDayExp"); 
                $BYearExp = $this->request->getPost("BYearExp");
                $SMonthExp = $this->request->getPost("SMonthExp"); 
                $SDayExp = $this->request->getPost("SDayExp"); 
                $SYearExp = $this->request->getPost("SYearExp");

                $files = array($bpermit,$spermit,$tpermit);
                $i = 0;
                foreach($files as $file) {
                    if($i == 0) {
                        $exp = $BYearExp."-".$BMonthExp."-".$BDayExp;
                    } else if($i == 1){
                        $exp = $SYearExp."-".$SMonthExp."-".$SDayExp;
                    } else {
                        $exp = "";
                    }
                    
                    if ($file){
                        $p1 = new RefSiteUserProofDocumentsTbl();
                        $p1->user_id = $data_row->user_id;
                        $p1->expiration = $exp;
                        $p1->file = $file;
                        $p1->document_type = $i;
                        $p1->save();
                    }
                    $i += 1;

                } 
                
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                $mailer->sendVerificationEmailAction($post_data['email'],$fullname,$token);
                $this->response->redirect(BASE_URI . 'users/customers', true); 
            } 
        }
    }
	
	public function editCustomerSellerAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $modifier_id = $this->session->LPMADMINSESSION['user_id'];
        
	$user_id = $post_data['user_id'];
        $data_row = SiteUserAccessTbl::findFirst("user_id=$user_id");
		//$data_row->customer_type_id = $post_data['customer_type_id'];
//        $data_row->first_name = $post_data['first_name'];
//        $data_row->last_name = $post_data['last_name'];
//        $data_row->email = $post_data['email'];
//        $data_row->password = $this->security->hash($post_data['password']);        
//        $data_row->company_name = $post_data['company_name'];
//        $data_row->birthdate = date('Y-m-d',strtotime($post_data['year']."-".$post_data['month']."-".$post_data['day']));        
//        $data_row->mobile_number = $post_data['mobile_number'];
//        if ($post_data['office_number']){
//            $data_row->office_number = $post_data['office_number'];
//        }
//        if ($post_data['fax_number']){
//            $data_row->fax_number = $post_data['fax_number'];
//        }
        $data_row->access_status = $post_data['access_status'];
        //$data_row->datetime_created = date('Y-m-d h:i:s');
        $data_row->modified_on = date('Y-m-d H:i:s');
		$data_row->modified_by = $modifier_id;
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{       
//            $data_row_address = RefUserAddressTbl::findFirst("user_id=$user_id");
//			if (!$data_row_address)
//			{
//				$data_row_address = new RefUserAddressTbl();
//				$data_row_address->user_id = $user_id;
//			}
//            
//            $data_row_address->region = $post_data['region'];
//            $data_row_address->province = $post_data['province'];
//            $data_row_address->city = $post_data['city'];
//            $data_row_address->address1 = $post_data['address1'];
//            $data_row_address->address2 = $post_data['address2'];
//
//            if ($data_row_address->save() == false ) {
//                foreach ($data_row_address->getMessages() as $message) {
//                    echo $message;
//                    echo "\n";
//                }
//            } else{       
//                echo "success";
                if ($post_data['b_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '0'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }
                if ($post_data['s_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '1'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }
                if ($post_data['t_verification']){
                    $update_ver = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user_id' AND document_type = '2'");
                    $update_ver->verified_status = "1";
                    $update_ver->save(); 
                }
                
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                
                $this->response->redirect(BASE_URI . 'users/customers', true); 
//            } 
        }
    }
	
	public function deleteCustomerSellerAction(){
		$this->view->disable();
		$post_data = $this->request->getPost();
		$user_id = $post_data['user_id'];
		$modifier_id = $this->session->LPMADMINSESSION['user_id'];
		
		$user_info = SiteUserAccessTbl::findFirst("user_id=$user_id");
		$user_info->modified_by = $modifier_id;
		$user_info->modified_on = date('Y-m-d H:i:s');
		$user_info->archive_flag = 1;
		$user_info->save();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE CUSTOMER");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                
		echo "Success";
		
	}
	
    
	public function getCitiesAction() {
        $this->view->disable();
        $provinceId = $_POST['province_id'];
        $cities = RefCitiesTb::find("province_id =' $provinceId'");
        
        $data_array = array();
        foreach ($cities as $entry) {
            $data_array[] = $entry;
        }        
        
        echo json_encode($data_array);
    }
	
	public function getProvincesAction() {
        $this->view->disable();
        $regionId = $_POST['region_id'];
        $provinces = RefProvincesTb::find(" region_id = '$regionId'");
        
        $data_array = array();
        foreach ($provinces as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);         
    }

}
