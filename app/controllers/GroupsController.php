<?php

class GroupsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'groups/index');
        $this->view->setVar('page_active', 'groups');
        $this->view->setVar('page_title', 'Groups');

//        $this->view->setVar('css', 'groups.css');
    }
    
    public function getGroupListAction(){
        $this->view->disable();
        
        $data = RefSiteGroupTbl::find("archive_flag = '0' ORDER BY group_sequence ASC");
        $data_array = array();
        foreach ($data as $entry) {
            $data_array[] = $entry;
        }

        echo json_encode($data_array);        
    } 
    
    public function saveNewGroupAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/navigation/';
//        echo getcwd(); die;
        
        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) { 
                $filename = $file->getName();               
                $file->moveTo($baseLocation . $filename);              
            }
        }  
        
        $data_row = new RefSiteGroupTbl();
        $data_row->group_name = $post_data['group_name'];
        $data_row->group_image_url = $filename;
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD GROUP");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'groups', true);  
        }   
        
    }
    
    public function editGroupAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/navigation/';
        $id = $post_data['group_id']; 
        
        $data_row = RefSiteGroupTbl::findFirst("group_id = '$id'");
        $data_row->group_name = $post_data['group_name'];
        if ($this->request->hasFiles(true) == true) { 
            unlink($baseLocation.$data_row->group_image_url);           
            foreach ($this->request->getUploadedFiles() as $file) { 
                $filename = $file->getName();                
                $file->moveTo($baseLocation . $filename);              
            }           
            $data_row->group_image_url = $filename;    
        }            
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "0";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT GROUP");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'groups', true);  
        }   
        
    }   
    
    public function deleteGroupAction(){
        $this->view->disable();
        $post_data = $this->request->getPost();
        $baseLocation = '../../lpm_website/public/img/navigation/';
        $id = $post_data['group_id'];
        
        
        $data_row = RefSiteGroupTbl::findFirst("group_id = '$id'");
            unlink($baseLocation.$data_row->group_image_url);
        $data_row->modified_on = date('Y-m-d H:i:s');
        $data_row->archive_flag = "1";
        
        if ($data_row->save() == false ) {
            foreach ($data_row->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        } else{
            
            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE GROUP");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/              
            
//            echo "success";
            $this->response->redirect(BASE_URI . 'groups', true); 
        }   
        
    }       
    
}
