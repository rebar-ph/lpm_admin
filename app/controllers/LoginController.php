<?php
use MailerController as Mailer;
use Phalcon\Security\Random;

class LoginController extends ControllerBase {

    public function _registerSession($user) {
        $this->session->set(
            'LPMADMINSESSION', array(
            'user_id' => $user->user_id,
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'access_id' => $user->access_id
                )
        );
        $this->session->start();
    }

    public function indexAction() {
        if ($this->session->has('LPMADMINSESSION')) {
            $this->response->redirect(BASE_URI, true);
        } else {

            $this->view->setVar('page_content', 'login/index');
            $this->view->setVar('page_active', 'login');
            $this->view->setVar('page_title', 'Login');

            $this->view->setVar('css', 'login.css');
        }
    }

    public function forgot_passwordAction() {
        $this->view->setVar('page_content', 'login/forgot-password');
        $this->view->setVar('page_active', 'login');
        $this->view->setVar('page_title', 'Login');

        $this->view->setVar('css', 'login.css');
    }

//    public function reset_passwordAction() {
//        $this->view->setVar('page_content', 'login/reset-password');
//        $this->view->setVar('page_active', 'login');
//        $this->view->setVar('page_title', 'Login');
//
//        $this->view->setVar('css', 'login.css');
//    }

    public function create_accountAction() {
        $this->view->setVar('page_content', 'account/create-account');
        $this->view->setVar('page_active', 'home');
        $this->view->setVar('page_title', 'Create Account');

        $this->view->setVar('css', 'account.css');
        $this->view->setVar('js', 'account.js');
    }

    public function company_regAction() {
        $this->view->setVar('page_content', 'account/company-reg');
        $this->view->setVar('page_active', 'home');
        $this->view->setVar('page_title', 'Create Account');

        $this->view->setVar('css', 'account.css');
        $this->view->setVar('js', 'account.js');
    }

    public function personal_regAction() {
        $this->view->setVar('page_content', 'account/personal-reg');
        $this->view->setVar('page_active', 'home');
        $this->view->setVar('page_title', 'Create Account');

        $this->view->setVar('css', 'account.css');
        $this->view->setVar('js', 'account.js');
    }

    public function edit_companyAction() {
        $this->view->setVar('page_content', 'account/edit-company-profile');
        $this->view->setVar('page_active', 'home');
        $this->view->setVar('page_title', 'Edit Profile');

        $this->view->setVar('css', 'account.css');
        $this->view->setVar('js', 'account.js');
    }

    /**
     * {Customer Login Function}
     */
    public function adminLoginAction() {
        $this->view->disable();
        $email = $this->request->getPost("email");
        $password = $this->request->getPost("password");
        $account = AdminUserAccessTbl::findFirst("email = '$email'");
        if ($account) {
            if ($this->security->checkHash($password, $account->password) && $account->access_status =='0' && $account->archive_flag =='0') {
                $this->_registerSession($account);
                //save login time
                $account->last_login = date('Y-m-d H:i:s');
                if ($account->save() == false ) {
                    foreach ($account->getMessages() as $message) {
                        echo($message);
                    }
                }               
                echo json_encode("success");               
            } else {
                $this->security->hash(rand());
                echo json_encode("Invalid Username or Passwordx.");
            }
        } else {
            echo json_encode("Invalid Username or Passwordz.");
        }
    }

    /**
     * {Logout Function}
     */
    public function logoutAction() {
        $this->view->disable();
        $this->session->destroy();
        $this->response->redirect(BASE_URI, true);
    }
    
    
    public function resetPasswordAction()
    {
        $this->view->disable();
        $mailer = new Mailer();
        $random = new Random();
        
        $email = $this->request->getPost("email"); 
//        $dob   = $this->request->getPost("dob");
        
//        $user = SiteUserAccessTbl::findFirst(array(
//            "conditions" => "email = ?1 and birthdate = ?2",
//            "bind" => array(
//                1 => $email,
//                2 => $dob
//            )
//        ));

        $expiration_time = RefSiteSettingsNumTbl::findFirst("setting_name = 'email_password_reset'");
        $hours = $expiration_time->setting_value;

        $user = AdminUserAccessTbl::findFirst("email = '$email' AND archive_flag = '0'");      
        
        if ($user) {            
            $token    = $random->uuid();
            $now      = date("Y-m-d H:i");
            $expiry   = date('Y-m-d H:i', strtotime('+'.$hours.' hour', strtotime($now)));
            $fullname = $user->first_name . " " . $user->last_name;
            
            $resetPw             = new AdminResetpwTbl();
            $resetPw->token      = $token;
            $resetPw->expiration = $expiry;
            $resetPw->user_id   = $user->user_id;
            
            if ($resetPw->save() == false) {
                echo "Something Went Wrong";
            } else {
                $mailer->sendForgotPasswordEmailAction($email, $fullname, $token, $hours);
            }
            
        } else {
            echo "Invalid Email Address.";
        }
    }
    
    public function reset_passwordAction($token)
    {
        //$this->view->disable();
//        $token = $this->dispatcher->getParam("token");
//        $token = $this->request->getParam("token");
//      echo $token; die;
        $check = AdminResetpwTbl::findFirstByToken($token);
        
        if ($check) {

            date_default_timezone_set('Asia/Manila');
            
            if ($check->status == "1") {
                $this->view->setVar('page_content', 'account/reset-password-status');
                $this->view->setVar('page_active', 'home');
                $this->view->setVar('page_title', 'Reset Password');
                $this->view->setVar('css', 'account.css');
                $this->view->setVar('message', 'Token has been already used.');
            } else {

                // $dt   = $check->expiration;
                // $date = date(""$dt);
                // $now  = new \DateTime();
                // $interval = new \DateInterval('PT1H');
                // $periods = new \DatePeriod($date, $interval, $now);
                // $hours = iterator_count($periods);

                $datenow = date("Y-m-d H:i:s");

                $expiration = strtotime($check->expiration);
                $datetime = strtotime($datenow);
                $diff = $expiration - $datetime;
                $hours = $diff / ( 60 * 60 );

                // $expiration_time = RefSiteSettingsNumTbl::findFirst("setting_name = 'email_password_reset'");
                // $expiry_hours = $expiration_time->setting_value;

                if ($hours < 0) {
                    $this->view->setVar('page_content', 'account/reset-password-status');
                    $this->view->setVar('page_active', 'home');
                    $this->view->setVar('page_title', 'Reset Password');
                    $this->view->setVar('css', 'account.css');
                    $this->view->setVar('message', 'Token Expired.');
                } else {
                    $this->view->setVar('page_content', 'login/reset-password');
                    $this->view->setVar('page_active', 'login');
                    $this->view->setVar('page_title', 'Reset Password');
                    $this->view->setVar('css', 'login.css');
                    // $this->view->setVar('js', 'resetpw.js');
                    $this->view->setVar('token', $token);
                }

            }
            // }else{
            //     $this->view->setVar('page_content', 'account/reset-password-status');
            //     $this->view->setVar('page_active', 'home');
            //     $this->view->setVar('page_title', 'Reset Password');
            //     $this->view->setVar('css', 'account.css');
            //     $this->view->setVar('message', 'Reset password link has already expired.');
            // }
            
        } else {
            $this->view->setVar('page_content', 'account/reset-password-status');
            $this->view->setVar('page_active', 'home');
            $this->view->setVar('page_title', 'Reset Password');
            $this->view->setVar('css', 'account.css');
            $this->view->setVar('message', 'Token not found.');
        }
    }    
    
    public function setnewpwAction()
    {
        $this->view->disable();
        $token    = $this->request->getPost("token");
        $password = $this->request->getPost("pass");
        
        $check = AdminResetpwTbl::findFirstBytoken($token);
        if ($check) {
            $user = AdminUserAccessTbl::findFirstByuser_id($check->user_id);
            $user->password = $this->security->hash($password);
            if ($user->update() == false) {
                echo "Something went wrong";
            } else {
                echo $this->request->getPost("pass");
            }
        } else {
            echo "token not found";
        }
    }
    
}
