<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AuditLogger extends ControllerBase {

    public function logAudit($user_id,$ip_address,$action) {
        $auditLogs = new AuditLogs();
        $auditLogs->user_id = $user_id;
        $auditLogs->ip_address = $ip_address;
        $auditLogs->action = $action;

        //$this->db->begin();
        if (!$auditLogs->save()) {
            foreach ($auditLogs->getMessages() as $message) {
                echo $message->getMessage() . "<br/>";
            }
            exit();
        }
    }

}
