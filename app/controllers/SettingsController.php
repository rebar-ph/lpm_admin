<?php

class SettingsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'settings/index');
        $this->view->setVar('page_active', 'settings');
        $this->view->setVar('page_title', 'Settings');

        $this->view->setVar('css', 'users.css');
        // $this->view->setVar('js', 'cls.js');

        $num_settings = RefSiteSettingsNumTbl::find();
        foreach ($num_settings as $row) {
            $this->view->setVar($row->setting_name, $row->setting_value);
        }

        $blocked_keywords = RefSiteBlockedKeywordsTbl::find();
        $this->view->setVar("blocked_keywords", $blocked_keywords);

        $text_settings = RefSiteSettingsTextTbl::find();
        foreach ($text_settings as $row) {
            $this->view->setVar($row->setting_name, $row->setting_value);
        }

        $dates = RefSiteCalendarTbl::find();
        $this->view->setVar("dates", $dates);

        $blocked_users = SiteUserAccessTbl::find("blocked_status = 1");
        $this->view->setVar('blocked_users', $blocked_users);
    }

    public function saveNumSettingsAction() {
        $this->view->disable();
        $post_data = $this->request->getPost();

        $success = 0;
        foreach ($post_data as $key => $value) {
            $setting_name = $key;
            $data_row = RefSiteSettingsNumTbl::findFirst("setting_name = '$setting_name'");

            if ($setting_name == "inquiry_limit") {
                $old_setting_value = $data_row->setting_value;
                if ($value > $old_setting_value) {
                    $added_val = $value - $old_setting_value;
                    $users = SiteUserAccessTbl::find("archive_flag = '0'");
                    foreach ($users as $user) {
                        $user->remaining_inquiries = $user->remaining_inquiries + $added_val;
                        $user->save();
                    }
                }
                if ($value < $old_setting_value) {
                    $deduct_val = $old_setting_value - $value;                   
                    $users = SiteUserAccessTbl::find("archive_flag = '0'");
                    foreach ($users as $user) {
                        $old_remain_inquiries = $user->remaining_inquiries;
                        $new_remain_inquiries = $old_remain_inquiries - $deduct_val;
                        if ($new_remain_inquiries < 0){$new_remain_inquiries = 0;}
                        $user->remaining_inquiries = $new_remain_inquiries;
                        $user->save();
                    }
                }                
                
            }

            $data_row->setting_value = $value;

            if ($data_row->save() == false) {
                foreach ($data_row->getMessages() as $message) {
                    echo $message;
                    echo "\n";
                }
            } else {
                $success = 1;
            }
        }

        if ($success == 1) {
            echo "success";
        }
    }

    public function getBlockedKeywordsAction() {

        $this->view->disable();

        $keywords_arrays = $_POST["keywords_set"];

        foreach ($keywords_arrays as $keyword) {
            echo "keyword:" . $keyword[0];
            $keywords = RefSiteBlockedKeywordsTbl::findFirst("keyword = '$keyword[0]'");

            if (!$keywords) {
                $new_keywords = new RefSiteBlockedKeywordsTbl();
                $new_keywords->keyword = $keyword[0];

                $new_keywords->save();
            } else {
                if ($keyword[1] == "2") {
                    $keywords->delete();
                }
            }
        }

        //echo "Success";   
    }
    
    public function saveBlockedUsersAction() {

        $this->view->disable();

        $user_array = $_POST["keywords_set"];

        foreach ($user_array as $user) {

            if ($user[1] == "1") {
                $user_update = SiteUserAccessTbl::findFirst("user_id = '$user[0]'");
                $user_update->blocked_status = 1;

                $user_update->save();
            } else {
                $user_update = SiteUserAccessTbl::findFirst("user_id = '$user[0]'");
                $user_update->blocked_status = 0;

                $user_update->save();
            }
        }

        //echo "Success";   
    }    

    public function saveTextSettingsAction() {
        $this->view->disable();
        $post_data = $this->request->getPost();

        $success = 0;

        foreach ($post_data as $key => $value) {
            $setting_name = $key;
            $data_row = RefSiteSettingsTextTbl::findFirst("setting_name = '$setting_name'");
            if (!empty($data_row)) {
                $data_row->setting_value = $value;

                if ($data_row->save() == false) {
                    foreach ($data_row->getMessages() as $message) {
                        echo $message;
                        echo "\n";
                    }
                } else {
                    $success = 1;
                }
            }
        }

        if ($success == 1) {
            echo "success";
        }
    }

    public function saveCalendarSettingsAction() {
        $this->view->disable();
        $post_data = $this->request->getPost();
        $success = 0;

        $new_calendar_dates = array();
        foreach ($post_data['dates'] as $key => $value) {
            $date = date('Y-m-d', strtotime($value));
            array_push($new_calendar_dates, $date);
            $data_row = RefSiteCalendarTbl::findFirst("calendar_date = '$date'");

            if (empty($data_row)) {
                $new_row = new RefSiteCalendarTbl();
                $new_row->calendar_date = $date;

                if ($new_row->save() == false) {
                    foreach ($new_row->getMessages() as $message) {
                        echo $message;
                        echo "\n";
                    }
                } else {
                    $success = 1;
                }
            }
        }

        //delete dates not in calendar
        $existing_data = RefSiteCalendarTbl::find();
        foreach ($existing_data as $row) {
            $date_del = $row->calendar_date;
            if (!in_array($date_del, $new_calendar_dates)) {
                $del_row = RefSiteCalendarTbl::findFirst("calendar_date = '$date_del'");
                if ($del_row->delete() == false) {
                    foreach ($del_row->getMessages() as $message) {
                        echo $message;
                        echo "\n";
                    }
                } else {
                    $success = 1;
                }
            }
        }

        if ($success == 1) {
            echo "success";
        }
    }

    public function searchUsersAction() {
        $this->view->disable();
        try {
            $query = $this->request->get("query");
            $names = SiteUserAccessTbl::find(array(
                        "conditions" => "archive_flag = 0 and (company_name like '%$query%' or CONCAT_WS(' ',first_name,last_name) LIKE '%$query%')",
                        "order" => "company_name,last_name,first_name ASC"));

            $suggestions = array();
            foreach ($names as $name) {
                $suggestions[] = array(
                    "value" => $name->company_name,
                    "data" => $name->user_id,
                    "preview" => $name->company_name,
                    "company_name" => $name->company_name,
                    "full_name" => $name->first_name . " " . $name->last_name,
                    "first_name" => $name->first_name
                );
            }

            echo json_encode(array(
                'suggestions' => $suggestions
            ));
        } catch (\Exception $e) {
            echo get_class($e), ": ", $e->getMessage(), "\n";
            echo " File=", $e->getFile(), "\n";
            echo " Line=", $e->getLine(), "\n";
            echo $e->getTraceAsString();
        }
    }

    public function blockUserAction() {
        $this->view->disable();
        $user_id = $_POST['user_id'];


        $user = SiteUserAccessTbl::findFirst("user_id=$user_id");
        $user->blocked_status = 1;
        if ($user->save() == false) {
            foreach ($user->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        }

        echo "Success";
    }

    public function unblockUserAction() {
        $this->view->disable();
        $user_id = $_POST['user_id'];


        $user = SiteUserAccessTbl::findFirst("user_id=$user_id");
        $user->blocked_status = 0;
        if ($user->save() == false) {
            foreach ($user->getMessages() as $message) {
                echo $message;
                echo "\n";
            }
        }

        echo "Success";
    }

}
