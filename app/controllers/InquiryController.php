<?php

class InquiryController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'inquiry/index');
        $this->view->setVar('page_active', 'inquiry');
        $this->view->setVar('page_title', 'Inquiry');

//       	$this->view->setVar('css', 'forms.css');
        $this->session->set('idArr', "");
        
    }

    public function inq_formAction() {
        $reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
        
        $this->view->setVar('page_content', 'inquiry/inq_form');
        $this->view->setVar('page_active', 'inquiry');
        $this->view->setVar('page_title', 'Inquiry');

       	$this->view->setVar('css', 'forms.css');
    }
	
	public function getInquiryListAction() {
		$this->view->disable();
		$inquiries = SiteUserInquiriesTbl::find("status=0 and archive_flag=0 ORDER BY reference_number DESC");
		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
		
	}
	
	public function archiveAction()
	{
		$this->view->disable();
		$inquiry_id = $_POST['inquiry_id'];
		
		$inquiry = SiteUserInquiriesTbl::findFirst("inquiry_id=$inquiry_id");
                $initial_inquiry_status = $inquiry->prev_status;
		$inquiry->status = 10;
                $inquiry->date_archived = date("Y-m-d");
		$inquiry->save();

                if ($initial_inquiry_status == 0){
                    $user_id = $inquiry->user_id;
                    $user = SiteUserAccessTbl::findFirst("user_id=$user_id");
                    $user->remaining_inquiries = $user->remaining_inquiries + 1;
                    $user->save();
                }
		
		echo "Success";
	}

}
