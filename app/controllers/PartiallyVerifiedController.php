<?php

class PartiallyVerifiedController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'partially_verified/index');
        $this->view->setVar('page_active', 'partially_verified');
        $this->view->setVar('page_title', 'Partially Verified');

       	// $this->view->setVar('css', 'partially_verified.css');
    }

    public function formAction() {
		
		$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		$prepared_by = $inquiry->soa_prepared_by;
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$users_info = SiteUserAccessTbl::find();
		$users_info_array = array();
		foreach ($users_info as $user_info)
		{
			$users_info_array[$user_info->user_id] = $user_info->first_name . " " . $user_info->last_name;
		}
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		$cwt_status = RefSiteUserProofDocumentsTbl::findFirst("user_id = $user_id and document_type = 2");
		if($prepared_by){
			$admin = AdminUserAccessTbl::findFirst("user_id = $prepared_by");
		}
		
		$payment = RefSiteInquiryPaymentTbl::findFirstByinquiry_id($inquiry->inquiry_id);
		
		$payment_details = RefSiteInquiryPaymentTxnsTbl::findBypaymain_id($payment->id);
		
		$banks = RefBanks::find();
		$banks_array = array();
		foreach ($banks as $bank)
		{
			$banks_array[$bank->id] = $bank;
		}

		$logged_in_user_id = $this->session->LPMADMINSESSION['user_id'];
		
		$admin_users = AdminUserAccessTbl::find();
		$admin_users_array = array();
		foreach ($admin_users as $admin_user)
		{
			$admin_users_array[$admin_user->user_id] = $admin_user->first_name . " " . $admin_user->last_name;
		}
		
		$credit_memo = CreditMemoTb::findFirst("reference_number='$reference_number' and type=1");
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('payment', $payment);
		$this->view->setVar('payment_details', $payment_details);
		$this->view->setVar('banks_array', $banks_array);
		$this->view->setVar('users_info_array', $users_info_array);
		$this->view->setVar('logged_in_user_id', $logged_in_user_id);
		$this->view->setVar('admin_users_array', $admin_users_array);
		$this->view->setVar('credit_memo', $credit_memo);
		$this->view->setVar('banks', $banks);
		$this->view->setVar('cwt_status', $cwt_status);
		$this->view->setVar('admin', $admin);
		
        $this->view->setVar('page_content', 'partially_verified/form');
        $this->view->setVar('page_active', 'partially_verified');
        $this->view->setVar('page_title', 'Partially Verified');

       	$this->view->setVar('css', 'forms.css');
    }
	
	public function getPartialPaymentListAction() {
	
		$this->view->disable();
		
		$inquiries = SiteUserInquiriesTbl::getPartialVerified();
		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
	
	}
	
	public function addCreditMemoAction() {
		$this->view->disable();
		
		$reference_number = $_POST['reference_number'];
		$credit_amount = $_POST['credit_amount'];
		$reason = $_POST['reason'];
		$user_id = $_POST['user_id'];
		$admin_user_id = $this->session->LPMADMINSESSION['user_id'];
		
		$credit_memo = new CreditMemoTb();
		$credit_memo->user_id = $user_id;
		$credit_memo->admin_user_id = $admin_user_id;
		$credit_memo->reference_number = $reference_number;
		$credit_memo->type = 1;
		$credit_memo->amount = $credit_amount;
		$credit_memo->reason = $reason;
		$credit_memo->save();
                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "SUBMIT CREDIT MEMO");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                  
		echo "Success";
	}

	public function getCreditMemoAction() {
		$this->view->disable();
		$reference_number = $_POST['reference_number'];
		$credit_memo = CreditMemoTb::findFirst("reference_number = '$reference_number' and type = 1");
		if($credit_memo) { echo json_encode($credit_memo->amount); }
		
		// $order = SiteUserInquiriesTbl::findFirstByreference_number($reference_number);
		// $userId = $order->user_id;
		
		// $total_credits = 0;
		// foreach($credit_memo as $credit) {
		// 	if($credit->type == 1) {
		// 		$total_credits += $credit->amount;
		// 	} else {
		// 		$total_credits -= $credit->amount;
		// 	}
		// }
		// if($total_credits > 0) {
		// 	$payment = RefSiteInquiryPaymentTransactionsTbl::findFirst("transaction_no = '$reference_number' and bank_id = 99");
		// 	if($payment) {
		// 		echo "paid";
		// 	} else {
		// 		echo json_encode($total_credits);
		// 	}
			
		// }
	}

}
