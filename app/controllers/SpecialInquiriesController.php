<?php

use MailerController as Mailer;

class SpecialInquiriesController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'special_inquiries/index');
        $this->view->setVar('page_active', 'special_inquiries');
        $this->view->setVar('page_title', 'Special Inquiries');

//       	$this->view->setVar('css', 'forms.css');
    }

    public function si_formAction() {
    	$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserSpecialInquiriesTbl::findFirst("reference_number = '$reference_number'");
		
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id = $user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteSpecialInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserSpecialInquiriesItemsTbl::findBysi_id($inquiry->id);
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);

        $this->view->setVar('page_content', 'special_inquiries/si_form');
        $this->view->setVar('page_active', 'special_inquiries');
        $this->view->setVar('page_title', 'Special Inquiries');

       	$this->view->setVar('css', 'forms.css');
    }

    public function create_quotationAction() {
        $reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserSpecialInquiriesTbl::findFirst("reference_number = '$reference_number'");
		
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		//echo $province->name; die;
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteSpecialInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserSpecialInquiriesItemsTbl::findBysi_id($inquiry->id);
		
		$customerType = $user->customer_type_id;
		if ($customerType == 1) {
            $ctype = "p_";
        } else if ($customerType == 2) {
            $ctype = "c_";
        } else if ($customerType == 3) {
            $ctype = "r_";
        }
		
		if($customerType != 1) {

            $iproof = 0;
            $verified = 0;
            $proofs = RefSiteUserProofDocumentsTbl::findByuser_id($user_id);
            foreach($proofs as $proof) {
                $iproof += 1;
                if($proof->verified_status == 1) {
                    $verified += 1;
                }
            }
			//echo $verified;
            if($verified < 2) {
                 $ctype = "p_";
            }
        }
		//echo $ctype; die;
		
		$inquiry_setting = RefSiteSettingsNumTbl::findFirst("setting_name='inquiry_price'");
		$date_today = date('Y-m-d');
                $setting_val = $inquiry_setting->setting_value;        
        
		$valid_until_date = $this->getValidUntil($setting_val, $date_today);
		
		
		$products_pricing = AdminProductPricingTbl::find();
        $products_pricing_array = array();
        
        foreach ($products_pricing as $pricing) {
            $products_pricing_array[$pricing->kind_id] = $pricing;
        }
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('ctype', $ctype);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('valid_until_date',$valid_until_date);
		$this->view->setVar('products_pricing_array',$products_pricing_array);

        $this->view->setVar('page_content', 'special_inquiries/create_quotation');
        $this->view->setVar('page_active', 'special_inquiries');
        $this->view->setVar('page_title', 'Special Inquiries');

        $this->view->setVar('css', 'forms.css');
    }
	
	public function getSpecialInquiriesListAction(){
		$this->view->disable();
		
		$inquiries = SiteUserSpecialInquiriesTbl::find("archive_flag=0 ORDER BY reference_number DESC");
		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
	}
	
	public function createInquiryAction()
	{
		$this->view->disable();
		
		$special_inquiry_id = $_POST['special_inquiry_id'];
		$valid_until = $_POST['valid_until'];
		$kinds_array = $_POST['kinds_array'];
		$inquiry_for = $_POST['inquiry_for'];
		$sub_total = $_POST['sub_total'];
		
		$special_inquiry = SiteUserSpecialInquiriesTbl::findFirst("id = $special_inquiry_id");
		$user_id = $special_inquiry->user_id;
		
		
		
		$user_access = SiteUserAccessTbl::findFirst("user_id = $user_id");
		if ($user_access->remaining_inquiries != 0)
		{
			$eligible = 1;
			$truck_id = 0;
			if ($inquiry_for == 1)
			{
				$delivery_address = SiteSpecialInquiriesDeliveryAddressTbl::findFirst("inquiry_id=$special_inquiry_id");
				$city_id = $delivery_address->city_id;
				
				$admin_pricings = AdminProductPricingTbl::find();
				$admin_pricings_array = array();
				foreach ($admin_pricings as $admin_pricing)
				{
					$admin_pricings_array[$admin_pricing->kind_id] = $admin_pricing->points;
				}
				
				$total_points = 0;
				foreach ($kinds_array as $kind)
				{
					$total_points += floatval(intval($kind[2]) * floatval($admin_pricings_array[$kind[0]]));
				}
				
				$min_truck = 0;
				$max_truck = 0;
				$trucks = AdminTruckTypesTbl::find("archive_flag=0");
				foreach ($trucks as $truck)
				{
					if ($min_truck == 0) $min_truck = $truck->min_points;
					if ($min_truck > $truck->min_points) $min_truck = $truck->min_points;
					if ($max_truck < $truck->max_points) $max_truck = $truck->max_points;
				}
				
				//$min_truck = AdminTruckTypesTbl::minimum(['column' => 'min_points']);
				//$max_truck = AdminTruckTypesTbl::maximum(['column' => 'max_points']);
				
				
				//$total_points = 0;
				if($total_points < $min_truck) {
					echo "Not Eligible for Delivery";
					return;
				} else if($total_points > $max_truck) {
					$truck = AdminTruckTypesTbl::findFirst("max_points = $max_truck and archive_flag=0");
					if ($truck)
					{
						$truck_id = $truck->id;
					
						$city_charge = AdminCityChargesTbl::findFirst("city_id=$city_id and truck_type_id=$truck_id");
						if ((!$city_charge) || ($city_charge->wheel_restriction == 1))
						{
							echo "Not Eligible for Delivery";
							return;
						}
					}
					else
					{
						echo "Not Eligible for Delivery. No truck found.";
						return;
					}
					
				} else {
					$truck = AdminTruckTypesTbl::findFirst(array(
							"conditions" => "?1 >= min_points and ?1 <= max_points and archive_flag=0",
							"bind" => array(1 => $total_points)                                                     
					));
					
					if ($truck)
					{
						$truck_id = $truck->id;
					
						$city_charge = AdminCityChargesTbl::findFirst("city_id=$city_id and truck_type_id=$truck_id");
						if ((!$city_charge) || ($city_charge->wheel_restriction == 1))
						{
							echo "Not Eligible for Delivery";
							return;
						}
					}
					else
					{
						echo "Not Eligible for Delivery. No truck found.";
						return;
					}
					
				}
			}
			
			$inq = new SiteUserInquiriesTbl();
			
			$now         = date("Y-m-d H:i:s");
			
			$reference_number       = "Q" . substr($special_inquiry->reference_number,2);//$this->generateCode("SiteUserInquiriesTbl", "Q");
			$inq->ud_id             = $this->generateudid();
			$inq->reference_number  = $reference_number;
			$inq->user_id           = $special_inquiry->user_id;
			$inq->for               = $inquiry_for;
			$inq->truckid           = $truck_id;
			$inq->valid_until       = $valid_until;
			$inq->datetime_created  = $now;
			$inq->datetime_modified = $now;
			$inq->remarks			= $special_inquiry->remarks;
			$inq->delivery_address_id = $special_inquiry->delivery_address_id;
			$inq->archive_flag 		= 0;
			
			if ($inq->save() == false) 
			{
				echo "Error";
			}
			else
			{
				if ($inq->for == 1)
				{
					$special_inquiry_address = SiteSpecialInquiriesDeliveryAddressTbl::findFirst("inquiry_id=$special_inquiry_id");
					$inquiry_address = new SiteInquiriesDeliveryAddressTbl();
					$inquiry_address->inquiry_id = $inq->inquiry_id;
					$inquiry_address->region_id = $special_inquiry_address->region_id;
					$inquiry_address->province_id = $special_inquiry_address->province_id;
					$inquiry_address->city_id = $special_inquiry_address->city_id;
					$inquiry_address->address = $special_inquiry_address->address;
					$inquiry_address->address_2 = $special_inquiry_address->address_2;
					$inquiry_address->save();
					
					$inq->delivery_address_id = $inquiry_address->id;
					$inq->save();
				}
				foreach ($kinds_array as $kind)
				{
					$kind[1] = floatval($kind[1]) + ((floatval($kind[4])*floatval($city_charge->wheel_charge))/(floatval($sub_total)*intval($kind[2])));
					$kind[1] = round($kind[1],2);
					$kind[4] = floatval($kind[1]) * intval($kind[2]);
					
					//var_dump($kind);
					$product = new RefSiteUserInquiriesProductsTbl();
					$product->inquiry_id = $inq->inquiry_id;
					$product->product_kind_id = $kind[0];
					$product->quantity = $kind[2];
					$product->unit_price = $kind[1];
					$product->uom = $kind[3];
					$product->total_amount = $kind[4];
					$product->save();
				}
			
				$special_inquiry->archive_flag = 2;
				$special_inquiry->save();


				//$user_id = $special_inquiry->user_id;
				//$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");
				$email = $user_access->email;
				$customer_type_id = $user_access->customer_type_id;

				if($customer_type_id != 1){
					$fullname = $user_access->company_name;
				}else{
					$fullname = $user_access->first_name." ".$user_access->last_name;
				}
				$user_access->remaining_inquiries--;
				$user_access->save();

				$order_no = $special_inquiry->reference_number;
				$ud_id = $special_inquiry->ud_id;
				$soa_date = Date("Y-m-d", $special_inquiry->datetime_created);

				$mailer = new Mailer();
				$mailer->sendSentSpecialInquiryAction($email, $fullname, $reference_number, $inq->ud_id, $inq->valid_until, $customer_type_id);

				echo "Success";
			}
		}
		else
		{
			echo "Not Allowed";
		}
                        /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "SEND QUOTATION");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/	
		
	}
	
	public function searchAction()
    {
        $this->view->disable();
        try {
            $query = $this->request->get("query");
			
			$products = RefSiteProductTbl::find();
			$products_array = array();
			foreach ($products as $product)
			{
				$products_array[$product->product_id] = $product;
			}
			
            
            $s = ProductKindsView::find(array(
                "conditions" => "archive_flag = 0 and (product_name like ?1 or kind_name like ?1)",
                "bind" => array(
                    1 => '%' . $query . '%'
                ),
                "order" => "kind_name ASC"
            ));
            
			$suggestions = array();
            foreach ($s as $x) {
				if($products_array[$x->product_id]->image_link_1 == "") {
                    $photo = 'default.jpg';
                } else {
                    $photo = $products_array[$x->product_id]->image_link_1;
                }
                $suggestions[] = array(
                    "value" => $x->kind_name,
                    "preview" => $x->kind_name,
					"product_name" => $products_array[$x->product_id]->product_name,
                    "data" => $x->product_kind_id,
					"measurement" => $x->measurement,
					"color" => $x->color,
					"uom" => $x->uom,
					"prepaid" => $x->prepaid,
					"lead_time" => $x->lead_time,
                    "image" => $photo,
                    "type" => 1
                );
            }
            
            //     sort($suggestions, SORT_NATURAL | SORT_FLAG_CASE);
            //$suggestions = array_slice($suggestions, 0, 3);
            echo json_encode(array(
                'suggestions' => $suggestions
            ));
            
            $this->view->disable();
        }
        
        catch (\Exception $e) {
            echo get_class($e), ": ", $e->getMessage(), "\n";
            echo " File=", $e->getFile(), "\n";
            echo " Line=", $e->getLine(), "\n";
            echo $e->getTraceAsString();
        }
    }


	public function declineAction()
	{
		$this->view->disable();
		
		$inquiry_id = $_POST['inquiry_id'];
		
		$inquiry = SiteUserSpecialInquiriesTbl::findFirst("id = $inquiry_id");
                $inquiry->date_archived = date("Y-m-d");
		$inquiry->archive_flag = 1;
		$inquiry->save();
		
		$ud_id = $inquiry->ud_id;
		$user_id = $inquiry->user_id;

		$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");
		$customer_type_id = $user_info->customer_type_id;

		if($user_info->customer_type_id != 1){
			$fullname = $user_info->company_name;
		}else{
			$fullname = $user_info->first_name." ".$user_info->last_name;
		}

		$sp_products = RefSiteUserSpecialInquiriesItemsTbl::find("si_id = $inquiry_id");

		foreach ($sp_products as $sp_product){

			$mail_body .= '<table class="email-tbl" style="margin: 10px 0 0 12px;font-size: 14px;color: #4D4D4D;">'
					.'<tr>'
					.'<td style="padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';">Item No.</td>'
					.'<td style="width:30%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';" class="text-center">:</td>'
					.'<td style="width:40%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';font-family: '.'SF-Pro-Text-Bold'.' !important;font-size: 14px !important;" class="content">'.$sp_product->name.'</td>'
					.'</tr>'
					.'<tr>'
					.'<td style="padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';">Unit of Measure</td>'
					.'<td style="width:30%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';" class="text-center">:</td>'
					.'<td style="width:40%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';font-family: '.'SF-Pro-Text-Bold'.' !important;font-size: 14px !important;" class="content">'.$sp_product->uom.'</td>'
					.'</tr>'
					.'<tr>'
					.'<td style="padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';">Quantity</td>'
					.'<td style="width:30%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';" class="text-center">:</td>'
					.'<td style="width:40%;padding: 5px 10px 5px 0;font-family: '.'SF-Pro-Text-Regular'.';font-family: '.'SF-Pro-Text-Bold'.' !important;font-size: 14px !important;" class="content">'.$sp_product->qty.'</td>'
					.'</tr>'
					.'</table>';
		}

		$mailer = new Mailer();
		$mailer->declineSpecialInquiriesAction($user_info->email, $fullname, $mail_body);
        /*****AUDIT LOGS******/
        try {
            $audit_log = new AuditLogger();
            $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DECLINE REQUEST");
        }catch (Exception $e) { }
        /*****AUDIT LOGS******/		
		echo "Success";
	}


	public function toggleForAction()
	{
		$this->view->disable();
		
		$inquiry_for = $_POST['inquiry_for'];
		$inquiry_id = $_POST['inquiry_id'];
		
		$inquiry = SiteUserSpecialInquiriesTbl::findFirst("id = $inquiry_id");
		$inquiry->for = $inquiry_for;
		$inquiry->save();
		
		
		$data_array = array();
		$data_array["message"] = "Success";
		$data_array["inquiry_for"] = $inquiry_for;
		
		echo json_encode($data_array);
		
	}

}
