<?php

class ServedController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'served/index');
        $this->view->setVar('page_active', 'served');
        $this->view->setVar('page_title', 'Served');

//        $this->view->setVar('css', 'served.css');
    }
	
	public function getServedListAction() {
	
		$this->view->disable();
		
		$inquiries = SiteUserInquiriesTbl::find("status=8 and archive_flag=0 ORDER BY reference_number DESC");
		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
	
	}
	
	public function formAction() {
        
		$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		$prepared_by = $inquiry->soa_prepared_by;
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
        $province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
                        $delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		$cwt_status = RefSiteUserProofDocumentsTbl::findFirst("user_id = $user_id and document_type = 2");
		if($prepared_by){
			$admin = AdminUserAccessTbl::findFirst("user_id = $prepared_by");
		}
		
		$banks = RefBanks::find();
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
        $this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
        $this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('banks', $banks);
		$this->view->setVar('cwt_status', $cwt_status);
		$this->view->setVar('admin', $admin);
		
		$this->view->setVar('page_content', 'for_delivery_pickup/form');
        $this->view->setVar('page_active', 'served');
        $this->view->setVar('page_title', 'Served');

       	$this->view->setVar('css', 'forms.css');
    }

}
