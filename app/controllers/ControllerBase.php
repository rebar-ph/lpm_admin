<?php

use Phalcon\Mvc\Controller;
use Phalcon\Security\Random;

class ControllerBase extends Controller
{
    public function initialize() {
  
        if ($this->dispatcher->getControllerName() != "login")
        {
            if (!$this->session->has('LPMADMINSESSION')) {
                $this->response->redirect(BASE_URI."login", true);
            } else {
                $access_id = $this->session->LPMADMINSESSION['access_id']; 
                $user_access = RefAdminAccessTypeTbl::findFirst("access_id = '$access_id'");
                $this->session->set(
                    'LPMADMINACCESS', array(
                    'orders' => $user_access->orders,
                    'soa' => $user_access->soa,
                    'payment' => $user_access->payment,
                    'products' => $user_access->products,
                    'pricing' => $user_access->pricing,
                    'users' => $user_access->users,
                    'settings' => $user_access->settings    
                    )
                );
				
				$orders_array = array("specialinquiries","inquiry","draftorder","posent","soaforapproval","forpayment","verifypayment","partiallyverified","fordeliverypickup","served","expired","archived");
				if (($this->session->LPMADMINACCESS['orders'] == 0) && (in_array($this->router->getControllerName(),$orders_array)))
				{
                                    $this->response->redirect(BASE_URI . 'common/page_eror', true); 
				}
				
				$products_array = array("products","categories","groups","brands");
				if (($this->session->LPMADMINACCESS['products'] == 0) && (in_array($this->router->getControllerName(),$products_array)))
				{
                                    $this->response->redirect(BASE_URI . 'common/page_eror', true); 
				}
				
				$pricing_array = array("pricing","city_charges","trucks");
				if (($this->session->LPMADMINACCESS['pricing'] == 0) && (in_array($this->router->getControllerName(),$pricing_array)))
				{
                                    $this->response->redirect(BASE_URI . 'common/page_eror', true); 
				}
				
				$users_array = array("users","roles");
				if (($this->session->LPMADMINACCESS['users'] == 0) && (in_array($this->router->getControllerName(),$users_array)))
				{
                                    $this->response->redirect(BASE_URI . 'common/page_eror', true); 
				}
				
				$settings_array = array("company","settings","logs");
				if (($this->session->LPMADMINACCESS['settings'] == 0) && (in_array($this->router->getControllerName(),$settings_array)))
				{
                                    $this->response->redirect(BASE_URI . 'common/page_eror', true); 
				}
                
            }

            // terms and conditions data
            $order_tac = RefSiteSettingsTextTbl::find();

            foreach($order_tac as $row){
                $this->view->setVar($row->setting_name, $row->setting_value);
            }

             // lpm info details
            $lpm_infos = RefSiteLpmInfoTbl::find();
            $lpm_info_array = array(); 

            foreach ($lpm_infos as $lpm_info)
            {
                if (!$lpm_info_array[$lpm_info->company_info]) $lpm_info_array[$lpm_info->company_info] = array();
                $lpm_info_array[$lpm_info->company_info][] = $lpm_info->company_data;
            }
            $this->view->setVar("lpm_info_array", $lpm_info_array);
        }
    }
	
	public function generatecode($table, $code)
    {
        $this->view->disable();
            $table = SiteInqCtrTbl::findFirstByid(1);
        $now   = new \DateTime('now');
        $month = $now->format('m');
        $day   = $now->format('d');
        $year  = $now->format('Y');
        $today = Date('Y-m-d');
        $ctr_date = $table->datetime;
        $ctr = $table->ctr;

        if($today == $ctr_date) {
            $ctr += 1;
        } else {
            $ctr = 1;
        }

        $table->datetime = $today;
        $table->ctr = $ctr;
        if($table->update() == false) {
            foreach($table->getMessages() as $msg) {
                echo $msg;
            }
        } else {
          $no = str_pad($ctr, 3, "0", STR_PAD_LEFT);
          $counter = "Q" . "-" . $year . $month . $day . "-" . $no;
          return $counter;
        }
    }
	
	public function generateudid()
    {
        $random = new Random();
        $uuid   = $random->uuid();
        return $uuid;
    }
    
    public function getValidUntil($setting_val, $date_start){
        $date_start_object = date_create($date_start);
        //add days based on settings
        $string_days = $setting_val . " days";
        date_add($date_start_object, date_interval_create_from_date_string($string_days));
        //add days based on calendar settings
        $temp_valid_until = date_format($date_start_object,'Y-m-d');
        $dates = RefSiteCalendarTbl::find("calendar_date >= '$date_start' AND calendar_date <= '$temp_valid_until'");
        $added_days = count($dates);
        date_add($date_start_object, date_interval_create_from_date_string($added_days." days"));
        //check if new intervals fall on a set weekend/holiday
        $date_check = 1;
        $new_start_date = $temp_valid_until;
        $new_valid_until = date_format($date_start_object,'Y-m-d');                
        while ($date_check == 1){
            $dates = RefSiteCalendarTbl::find("calendar_date > '$new_start_date' AND calendar_date <= '$new_valid_until'");
            $added_days = count($dates);
            if ($added_days > 0){
                $new_start_date = $new_valid_until;
                date_add($date_start_object, date_interval_create_from_date_string($added_days." days"));
                $new_valid_until = date_format($date_start_object,'Y-m-d'); 

            } else {
                $date_check = 0;
            } 
        }

        $valid_until = date_format($date_start_object,'Y-m-d');
        return $valid_until;
    }    
    
}