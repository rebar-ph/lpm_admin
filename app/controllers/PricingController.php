<?php
//require APP_PATH . '/app/library/vendor/autoload.php';
require_once 'classes/PHPExcel.php';
//use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\IOFactory;


class PricingController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'pricing/index');
        $this->view->setVar('page_active', 'pricing');
        $this->view->setVar('page_title', 'Pricing');

//        $this->view->setVar('css', 'pricing.css');
		$this->assets->addJs('plugins/upload/upload.js');
    }
	
	public function getPricingListAction()
	{
		$this->view->disable();
		
		$products = RefSiteProductTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_id] = $product->product_name;
		}
		
		$product_kinds = RefSiteProductKindsTbl::find();
		$product_kinds_array = array();
		foreach ($product_kinds as $product_kind)
		{
			$product_kinds_array[$product_kind->product_kind_id] = $product_kind;
		}
		
		$kinds_pricing = AdminProductPricingTbl::find();
		$kinds_pricing_array = array();
		foreach ($kinds_pricing as $kind_pricing)
		{
			$kinds_pricing_array[] = $kind_pricing;
		}
		$this->view->setVar('product_kinds', $product_kinds);
		$this->view->setVar('kinds_pricing', $kinds_pricing);
		
		$data_array = array();
		$data_array['product_kinds'] = $product_kinds_array;
		$data_array['kinds_pricing'] = $kinds_pricing_array;
		$data_array['products'] = $products_array;
		
		echo json_encode($data_array);
	}
	
	public function deletePricingAction()
	{
		$this->view->disable();
		
		$product_kind_id = $_POST['product_kind_id'];
		
		$product_kind = AdminProductPricingTbl::findFirst("kind_id=$product_kind_id");
		$product_kind->delete();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE PRICING");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                
                
		echo "Success";
		
	}
	
	public function savePricingAction()
	{
		$this->view->disable();
		
		$product_kind_id = $_POST['product_kind_id'];
		$textfield_array = $_POST['textfield_array'];
		$mode_checkbox = $_POST['mode_checkbox'];
		
		$kind_pricing = AdminProductPricingTbl::findFirst("kind_id=$product_kind_id");
		$kind_pricing->points = $textfield_array[0];
		$kind_pricing->increment = $textfield_array[1];
		$kind_pricing->min_qty1 = $textfield_array[2];
		$kind_pricing->min_qty2 = $textfield_array[3];
		$kind_pricing->min_qty3 = $textfield_array[4];
		$kind_pricing->base = $textfield_array[5];
		$kind_pricing->mode = $mode_checkbox;
		$kind_pricing->p_price1 = $textfield_array[6];
		$kind_pricing->p_price2 = $textfield_array[7];
		$kind_pricing->p_price3 = $textfield_array[8];
		$kind_pricing->c_price1 = $textfield_array[9];
		$kind_pricing->c_price2 = $textfield_array[10];
		$kind_pricing->c_price3 = $textfield_array[11];
		$kind_pricing->r_price1 = $textfield_array[12];
		$kind_pricing->r_price2 = $textfield_array[13];
		$kind_pricing->r_price3 = $textfield_array[14];
		$kind_pricing->save();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "SAVE PRICING");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                
                
		echo "Success";
	}
	
	public function uploadBatchFileAction()
	{
		$this->view->disable();
        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
                $baseLocation = 'uploads/temp/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                //echo $unique_filename . "." . $ext;
				
				if (in_array($ext,array("xls","xlsx")))
				{
					$inputFileName = $baseLocation . $unique_filename . "." . $ext;
					//$spreadsheet = IOFactory::load($inputFileName);
					//$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
					
					$fileType = PHPExcel_IOFactory::identify($inputFileName);
					//$fileName = 'templates/template2.xlsx';
			
					// Read the file
					$objReader = PHPExcel_IOFactory::createReader($fileType);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = array(1,$objPHPExcel->getActiveSheet()->toArray(null,true,true,true));
					//var_dump($sheetData);
					
					$counter = 1;
					$error_message = "";
					foreach ($sheetData[1] as $sheetRow)
					{
						$error_message_row = "";
						if (($counter > 1) && ($sheetRow["A"]))
						{
							$product_kind_id = $sheetRow["A"];
							$points = $sheetRow["F"];
							$increment = $sheetRow["G"];
							$mql1 = $sheetRow["H"];
							$mql2 = $sheetRow["I"];
							$mql3 = $sheetRow["J"];
							$base_price = $sheetRow["K"];
							$mode = $sheetRow["L"];
							$pl1 = $sheetRow["M"];
							$pl2 = $sheetRow["O"];
							$pl3 = $sheetRow["Q"];
							$cl1 = $sheetRow["S"];
							$cl2 = $sheetRow["U"];
							$cl3 = $sheetRow["W"];
							$wl1 = $sheetRow["Y"];
							$wl2 = $sheetRow["AA"];
							$wl3 = $sheetRow["AC"];
							
							$product_kind_id = intval($product_kind_id);
							$product_pricing = AdminProductPricingTbl::findFirst("kind_id=$product_kind_id");
							if ($product_pricing)
							{
								$product_pricing->points = $points;
								$product_pricing->increment = $increment;
								$product_pricing->min_qty1 = $mql1;
								$product_pricing->min_qty2 = $mql2;
								$product_pricing->min_qty3 = $mql3;
								$product_pricing->base = $base_price;
								if ($mode == "PHP") $product_pricing->mode = 1;
								else $product_pricing->mode = 2;
								$product_pricing->p_price1 = $pl1;
								$product_pricing->p_price2 = $pl2;
								$product_pricing->p_price3 = $pl3;
								$product_pricing->c_price1 = $cl1;
								$product_pricing->c_price2 = $cl2;
								$product_pricing->c_price3 = $cl3;
								$product_pricing->r_price1 = $wl1;
								$product_pricing->r_price2 = $wl2;
								$product_pricing->r_price3 = $wl3;
								//$product_pricing->save();
								if ($product_pricing->save() == false)
								{
									foreach ($kind_row->getMessages() as $msg) $error_message_row .= $msg . "\n";
								}
							}
							else
							{
								$error_message_row .= "Product Kind Not Existing.\n";
							}
						}
						$counter++;
						$error_message .= $error_message_row;
					}
					echo $error_message;
				}
				else
				{
					echo "Wrong File Format";
				}
			}
		}
	}

	public function get_random_filename()
    {
        $date   = Date('Y-m-d H:i:s');
        $length = 20;
        $key    = '';
        $keys   = array_merge(range(0, 9), range('a', 'z'));
        
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key.md5($date);
    }
}
