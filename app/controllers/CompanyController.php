<?php

class CompanyController extends ControllerBase {

    public function indexAction() {

        $lpm_infos = RefSiteLpmInfoTbl::find();
        $lpm_infos_array = array();
        foreach ($lpm_infos as $lpm_info) {
            if (!$lpm_infos_array[$lpm_info->company_info])
                $lpms_infos_array[$lpm_info->company_info] = array();
            $lpm_infos_array[$lpm_info->company_info][] = $lpm_info;
        }

        $ref_banks = RefBanks::find("archive_flag=0");
        $ref_banks_array = array();
        foreach ($ref_banks as $ref_bank) {
            $ref_banks_array [] = $ref_bank;
        }

        $ref_regions = RefRegionsTb::find("country_id=170");
        $ref_regions_array = array();
        foreach ($ref_regions as $ref_region) {
            $ref_regions_array[] = $ref_region;
        }


        $this->view->setVar('lpm_infos_array', $lpm_infos_array);
        $this->view->setVar('ref_banks_array', $ref_banks_array);
        $this->view->setVar('ref_regions_array', $ref_regions_array);

        $this->view->setVar('page_content', 'company/index');
        $this->view->setVar('page_active', 'company');
        $this->view->setVar('page_title', 'Company');

        $this->view->setVar('css', 'users.css');
        $this->view->setVar('js', 'cls.js');
    }

    public function applyCompanyAction() {
        $this->view->disable();
        
        $phone_array = $_POST['phone_array'];
        $mobile_array = $_POST['mobile_array'];
        $fax_array = $_POST['fax_array'];
        $email_array = $_POST['email_array'];
        $address = $_POST['address'];
        $phone_del = $_POST['phone_del'];
        $mobile_del = $_POST['mobile_del'];
        $fax_del = $_POST['fax_del'];
        $email_del = $_POST['email_del'];

        foreach ($phone_array as $key => $value) {
            if ($key !== "undefined"){
                $company_info = RefSiteLpmInfoTbl::findFirst("id=$key");
                $company_info->company_data = $value;
                $company_info->save();
            } else {
                $company_info = new RefSiteLpmInfoTbl();
                $company_info->company_info = "phone_no";
                $company_info->company_data = $value;
                $company_info->save();                
            }
        }
        
        foreach ($phone_del as $val){
                $row = RefSiteLpmInfoTbl::findFirst("id=$val");
                $row->delete();            
        }

        foreach ($mobile_array as $key => $value) {
            if ($key !== "undefined"){
                $company_info = RefSiteLpmInfoTbl::findFirst("id=$key");
                $company_info->company_data = $value;
                $company_info->save();
            } else {
                $company_info = new RefSiteLpmInfoTbl();
                $company_info->company_info = "mobile_no";
                $company_info->company_data = $value;
                $company_info->save();                  
            }
        }
        
        foreach ($mobile_del as $val){
                $row = RefSiteLpmInfoTbl::findFirst("id=$val");
                $row->delete();            
        }        

        foreach ($fax_array as $key => $value) {
            if ($key !== "undefined"){
                $company_info = RefSiteLpmInfoTbl::findFirst("id=$key");
                $company_info->company_data = $value;
                $company_info->save();
            } else {
                $company_info = new RefSiteLpmInfoTbl();
                $company_info->company_info = "fax_no";
                $company_info->company_data = $value;
                $company_info->save();                 
            }
        }

        foreach ($fax_del as $val){
                $row = RefSiteLpmInfoTbl::findFirst("id=$val");
                $row->delete();            
        }        
        
        foreach ($email_array as $key => $value) {
            if ($key !== "undefined"){            
                $company_info = RefSiteLpmInfoTbl::findFirst("id=$key");
                $company_info->company_data = $value;
                $company_info->save();
            } else {
                $company_info = new RefSiteLpmInfoTbl();
                $company_info->company_info = "email_address";
                $company_info->company_data = $value;
                $company_info->save();                 
            }
        }

        foreach ($email_del as $val){
                $row = RefSiteLpmInfoTbl::findFirst("id=$val");
                $row->delete();            
        }        
        
        $company_info = RefSiteLpmInfoTbl::findFirst("company_info='address'");
        $company_info->company_data = $address;
        $company_info->save();

        echo "Success";
    }

    public function applyBanksAction() {
        $this->view->disable();

        $banks_array = $_POST['banks_array'];

        $ref_banks = RefBanks::find();
        foreach ($ref_banks as $ref_bank) {
            if ($ref_bank->id != 99){
                $ref_bank->archive_flag = 1;
                $ref_bank->save();
            }
        }


        foreach ($banks_array as $bank) {
            $bank_id = $bank[0];
            $ref_bank = RefBanks::findFirst("id=$bank_id");
            if (!$ref_bank)
                $ref_bank = new RefBanks();
            $ref_bank->bank_name = $bank[1];
            $ref_bank->bank_branch = $bank[2];
            $ref_bank->account_name = $bank[3];
            $ref_bank->account_no = $bank[4];
            $ref_bank->account_type = $bank[5];
            $ref_bank->archive_flag = 0;
            $ref_bank->save();
        }

        echo "Success";
    }

}
