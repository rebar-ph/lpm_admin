<?php
use MailerController as Mailer;
use AuditLogger as AuditLogger;

class PoSentController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('page_content', 'po_sent/index');
        $this->view->setVar('page_active', 'po_sent');
        $this->view->setVar('page_title', 'PO Sent');

//       	$this->view->setVar('css', 'forms.css');
        $this->session->set('idArr', "");        
    }

    public function po_formAction() {
		
		$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
		$province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
			$delivery_province = RefProvincesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
	

		} else { 
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}


		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		
		$banks = RefBanks::find();
		$banks_array = array();
		foreach ($banks as $bank)
		{
			$banks_array[$bank->id] = $bank;
		}
		
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
		$this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
		$this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('banks_array', $banks_array);
		$this->view->setVar('banks', $banks);
		
		
        $this->view->setVar('page_content', 'po_sent/po_form');
        $this->view->setVar('page_active', 'po_sent');
        $this->view->setVar('page_title', 'PO Sent');

       	$this->view->setVar('css', 'forms.css');
    }

    public function create_soaAction() {
		
		$reference_number = $this->request->getQuery("id");
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number = '$reference_number'");
		
		$user_id = $inquiry->user_id;
		$user = SiteUserAccessTbl::findFirst("user_id=$user_id");
		
		$address = RefUserAddressTbl::findFirstByuser_id($inquiry->user_id);
		$city =  RefCitiesTb::findFirstByid($address->city);
                $province =  RefProvincesTb::findFirstByid($address->province);
		$region = RefRegionsTb::findFirstByid($address->region);
		
		$delivery_address = "";
		if($inquiry->for == "1") 
		{
			$delivery_address = SiteInquiriesDeliveryAddressTbl::findFirstByinquiry_id($inquiry->inquiry_id); 
			$delivery_city = RefCitiesTb::findFirstByid($delivery_address->city_id);
                        $delivery_province = RefCitiesTb::findFirstByid($delivery_address->province_id);
			$delivery_region = RefRegionsTb::findFirstByid($delivery_address->region_id);
			$cityCharge = AdminCityChargesTbl::findFirst(array(
                        "conditions" => "truck_type_id = ?1 and city_id = ?2",
                        "bind" => array(1 => $inquiry->truckid, 2 => $delivery_address->city_id)                                   
                ));
//			$charge =  $cityCharge->wheel_charge;
                        $charge =  0;

		} else { 
                        $charge =  0;
			$delivery_address = 'LPM CONSTRUCTION SUPPLY'."</br>".'1884 Cor., Quirino Ave, Paco, Manila, Metro Manila';
		}
		
		$products = RefSiteProductKindsTbl::find();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[$product->product_kind_id] = $product;
		}
		
		$items = RefSiteUserInquiriesProductsTbl::findByinquiry_id($inquiry->inquiry_id);
		
		$banks = RefBanks::find();
		
		$soa_approval_setting = RefSiteSettingsNumTbl::findFirst("setting_name='soa_approval'");
		$date_today = date('Y-m-d');
                $setting_val = $soa_approval_setting->setting_value;               
                
		$soa_approval_date = $this->getValidUntil($setting_val, $date_today);
                
                $file_2303 = RefSiteUserProofDocumentsTbl::findFirst("user_id = '$user->user_id' AND document_type = '2'");
                $verified_2303 = false;
                if ($file_2303){
                    if ($file_2303->verified_status == '1'){
                        $verified_2303 = true;
                    }
                }
                
        /*****AUDIT LOGS******/
        try {
            $audit_log = new AuditLogger();
            $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "CREATE SOA");
        }catch (Exception $e) { }
        /*****AUDIT LOGS******/
        
		$this->view->setVar('inquiry', $inquiry);
		$this->view->setVar('user', $user);
		$this->view->setVar('address', $address);
		$this->view->setVar('city', $city);
                $this->view->setVar('province', $province);
		$this->view->setVar('region', $region);
		$this->view->setVar('charge', $charge);
		$this->view->setVar('delivery_address', $delivery_address);
		$this->view->setVar('delivery_city', $delivery_city);
                $this->view->setVar('delivery_province', $delivery_province);
		$this->view->setVar('delivery_region', $delivery_region);
		$this->view->setVar('products_array', $products_array);
		$this->view->setVar('items', $items);
		$this->view->setVar('soa_approval_date',$soa_approval_date);
		$this->view->setVar('banks', $banks);
                $this->view->setVar('verified_2303', $verified_2303);
		
		
        $this->view->setVar('page_content', 'po_sent/create_soa');
        $this->view->setVar('page_active', 'po_sent');
        $this->view->setVar('page_title', 'PO Sent');

       	$this->view->setVar('css', 'forms.css');
    }
	
	public function getPOSentListAction() {
		$this->view->disable();
		$inquiries = SiteUserInquiriesTbl::find("status=3 and archive_flag=0 ORDER BY reference_number DESC");
		$inquiries_array = array();
		
		foreach ($inquiries as $inquiry)
		{
			$inquiries_array[] = $inquiry;
		}
		
		$users = SiteUserAccessTbl::find();
		$users_array = array();
		foreach ($users as $user)
		{
			$users_array[$user->user_id] = $user;
		}
		
		$data_array = array();
		$data_array['inquiries'] = $inquiries_array;
		$data_array['users'] = $users_array;
		
		echo json_encode($data_array);
	}
	
	public function sendSOAAction() {
		$this->view->disable();
	
		$delivery_charge = 0;
		$discount = 0;
		$cwt = 0;
		
		$reference_number = $_POST['reference_number'];
		if ($_POST['delivery_charge']) $delivery_charge = $_POST['delivery_charge'];
		if ($_POST['discount']) $discount = $_POST['discount'];
		if ($_POST['cwt']) $cwt = $_POST['cwt'];
		$valid_until = $_POST['valid_until'];
		
		$inquiry = SiteUserInquiriesTbl::findFirst("reference_number='$reference_number'");

		if ($inquiry)
		{

			$ud_id = $inquiry->ud_id;


			if ($inquiry->recompute_status == 0)
			{
				//send SOA to customer
				$inquiry->reference_number = "S" . substr($inquiry->reference_number,1);
				$inquiry->delivery_charge = $delivery_charge;
				$inquiry->discount = $discount;
				$inquiry->cwt = $cwt;
				$inquiry->valid_until = $valid_until;
				$inquiry->status = 4;
                                $inquiry->datetime_created = date('Y-m-d H:i:s');
                                $inquiry->soa_date = date('Y-m-d');
                                $inquiry->soa_prepared_by = $this->session->LPMADMINSESSION['user_id'];
				if ($inquiry->save() == false)
				{
					foreach ($inquiry->getMessages() as $msg) echo $msg;
				}
				$inquiry->save();
				
				$user_id = $inquiry->user_id;
				$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");

				if($user_info->customer_type_id != 1){
					$fullname = $user_info->company_name;
				}else{
					$fullname = $user_info->first_name." ".$user_info->last_name;
				}
				
				$mailer = new Mailer();
				$mailer->sendSOAAction($user_info->email, $fullname, $inquiry->reference_number, $inquiry->valid_until, $ud_id, $user_info->customer_type_id);
			}
			else
			{
				//resend SOA as revision
				$inquiry_id = $inquiry->inquiry_id;
				$inquiry_versions = InquiryVersionsTbl::find("inquiry_id = $inquiry_id");
				$new_reference_number = "";
				if (count($inquiry_versions) == 0)
				{
					$inquiry_version = new InquiryVersionsTbl();
					$inquiry_version->admin_id = $this->session->LPMADMINSESSION['user_id'];
					$inquiry_version->inquiry_id = $inquiry_id;
					$inquiry_version->reference_number = $inquiry->reference_number."-A";
					$inquiry_version->delivery_charge = $inquiry->delivery_charge;
					$inquiry_version->discount = $inquiry->discount;
					$inquiry_version->cwt = $inquiry->cwt;
					$inquiry_version->valid_until = $inquiry->valid_until;
					$inquiry_version->datetime_created = $datetime_created;
					$inquiry_version->save();
					
					$new_reference_number = $inquiry->reference_number."-B";
				}
				else
				{
					$last_character = substr($inquiry->reference_number,-1);
					$last_character++;
					$new_reference_number = substr($inquiry->reference_number,0,-1).$last_character;
				}
				
				$inquiry_version = new InquiryVersionsTbl();
				$inquiry_version->admin_id = $this->session->LPMADMINSESSION['user_id'];
				$inquiry_version->inquiry_id = $inquiry_id;
				$inquiry_version->reference_number = $new_reference_number;
				$inquiry_version->delivery_charge = $delivery_charge;
				$inquiry_version->discount = $discount;
				$inquiry_version->cwt = $cwt;
				$inquiry_version->valid_until = $valid_until;
				$inquiry_version->datetime_created = date('Y-m-d H:i:s');
				$inquiry_version->save();
				
				$inquiry->reference_number = $new_reference_number;
				$inquiry->delivery_charge = $delivery_charge;
				$inquiry->discount = $discount;
				$inquiry->cwt = $cwt;
				$inquiry->valid_until = $valid_until;
				$inquiry->status = 4;
				$inquiry->recompute_status = 0;
				if ($inquiry->save() == false)
				{
					foreach ($inquiry->getMessages() as $msg) echo $msg;
				}
				$inquiry->save();

				$user_id = $inquiry->user_id;
				$user_info = SiteUserAccessTbl::findFirst("user_id = $user_id");

				if($user_info->customer_type_id != 1){
					$fullname = $user_info->company_name;
				}else{
					$fullname = $user_info->first_name." ".$user_info->last_name;
				}

				$mailer = new Mailer();
				$mailer->sendRevisedSOAAction($user_info->email, $fullname, $new_reference_number, $valid_until, $ud_id, $user_info->customer_type_id);
			}
		}
                
                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "SEND SOA");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                
                
		echo "Success";
	}

}
