<?php

class CommonController extends ControllerBase {

    public function indexAction() {

    }

    public function saveSessionVarAction() {
        $this->view->disable();        
        $idArr = $_POST['idArr'];        
        $this->session->set('idArr', $idArr);
        
    }
    
    public function addDaysCalendarBasedAction($date_start, $date_end){
        $this->view->disable();
        
        $dates = RefSiteCalendarTbl::find("calendar_date >= '$date_start' AND calendar_date <= '$date_end'");
        $added_days = count($dates);
        $new_date_end = date('Y-m-d', strtotime($date_end. ' + '.$added_days.' days'));
        echo $new_date_end;
    }  
    
    public function page_errorAction() {      
        //html header
        $this->view->setVar('page_title', 'Page Error'); //required for Header Title
//        $this->view->setVar('css', 'home.css');

        //required fields
        $this->view->setMainView('noaccess');
        $this->view->setVar('page_name', 'Page Error');
        $this->view->setVar('page_active', 'page_error');
        $this->view->setVar('page_content', 'common/page_error'); //required for body content
        
        $lpm_infos = RefSiteLpmInfoTbl::find();
        $lpm_info_array = array(); 

        foreach ($lpm_infos as $lpm_info)
        {
            if (!$lpm_info_array[$lpm_info->company_info]) $lpm_info_array[$lpm_info->company_info] = array();
            $lpm_info_array[$lpm_info->company_info][] = $lpm_info->company_data;
        }

        $this->view->setVar("lpm_info_array", $lpm_info_array);        
        
    }    
    
}
