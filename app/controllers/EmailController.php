<?php

class EmailController extends ControllerBase
{
    // temporary controller for email mockups
    public function indexAction()
    {
    	$this->view->setVar('page_content','email/index');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function email_fpAction()
    {
    	$this->view->setVar('page_content','email/email-fp');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function email_inqAction()
    {
    	$this->view->setVar('page_content','email/email-inquiry');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function email_poAction()
    {
    	$this->view->setVar('page_content','email/email-po-sent');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function email_soaAction()
    {
    	$this->view->setVar('page_content','email/email-soa');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function revised_soaAction()
    {
    	$this->view->setVar('page_content','email/email-soa-revised');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function verified_paymentAction()
    {
    	$this->view->setVar('page_content','email/email-verified-p');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function declined_paymentAction()
    {
    	$this->view->setVar('page_content','email/email-declined-p');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function expired_inqAction()
    {
    	$this->view->setVar('page_content','email/email-expired-inq');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function expired_soaAction()
    {
    	$this->view->setVar('page_content','email/email-expired-soa');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function ready_pickupAction()
    {
    	$this->view->setVar('page_content','email/email-ready-pickup');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function ready_deliveryAction()
    {
    	$this->view->setVar('page_content','email/email-ready-delivery');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function declined_inqAction()
    {
    	$this->view->setVar('page_content','email/email-declined-inq');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }

    public function quotientAction()
    {
    	$this->view->setVar('page_content','email/email-quotient');
        $this->view->setVar('page_active','home');
        $this->view->setVar('page_title','Email');

		$this->view->setVar('css','email.css');        
    }
}