<?php
require APP_PATH . '/app/library/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailerController extends \Phalcon\Mvc\Controller
{
    
   /**
    
    * Sends a forgot password email action.
    *
    * @param      <type>  $email     The email of the customer
    * @param      <type>  $fullname  The fullname of the customer

    */
   
    public function sendForgotPasswordEmailAction($email,$fullname,$token,$hours)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            
            $message         = file_get_contents(APP_PATH . '/app/views/email/email-fp.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%username%', $fullname, $message);
            $message         = str_replace('%link%', BASE_URI."login/reset_password/".$token, $message);
            $message         = str_replace('%hours%', $hours, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'Reset your LPM Account Password';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendVerificationEmailAction($email,$fullname,$token)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            
            $message         = file_get_contents(APP_PATH . '/app/views/email/index.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%username%', $fullname, $message);
            $message         = str_replace('%link%', STOREFRONT_URI."account/verify/".$token, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'Activate your LPM Account';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
	
	public function sendInquirySubmittedAction($email, $fullname, $order_no, $valid_until)
	{
		$this->view->disable();
		$mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            
            $message         = file_get_contents(APP_PATH . '/app/views/email/email-inquiry.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
			$message         = str_replace('%valid_until%', $valid_until, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Inquiry Submitted';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
	}
	
	public function sendPOSubmittedAction($email, $fullname, $order_no, $order_date)
	{
		$this->view->disable();
		$mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            
            $message         = file_get_contents(APP_PATH . '/app/views/email/email-po-sent.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
			$message         = str_replace('%order_date%', $order_date, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Purchase Order Submitted';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
	}
	
	public function sendSOAAction($email, $fullname, $order_no, $order_date, $ud_id, $customer_type)
	{
		$this->view->disable();
		$mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/soa/for_approval/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/soa/for_approval/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-soa.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
			$message         = str_replace('%order_date%', $order_date, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM SOA for Approval';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
	}

    public function sendRevisedSOAAction($email, $fullname, $order_no, $order_date, $ud_id, $customer_type)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/soa/for_approval/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/soa/for_approval/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-soa-revised.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%order_no_link%', $link_url, $message);
            $message         = str_replace('%order_date%', $order_date, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Revised SOA for Approval';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendVerifyPaymentAction($email, $fullname, $bank_name, $date_paid, $trans_no, $amount, $order_no, $ud_id, $soa_date, $customer_type)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/payment_verify/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/payment_verify/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-verified-p.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%bank_name%', $bank_name, $message);
            $message         = str_replace('%date_paid%', $date_paid, $message);
            $message         = str_replace('%trans_no%', $trans_no, $message);
            $message         = str_replace('%amount%', $amount, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
            $message         = str_replace('%soa_date%', $soa_date, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Verified Payment';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendDeclinedPaymentAction($email, $fullname, $bank_name, $date_paid, $trans_no, $amount, $order_no, $ud_id, $soa_date, $customer_type, $decline_reason)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/payment_verify/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/payment_verify/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-declined-p.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%bank_name%', $bank_name, $message);
            $message         = str_replace('%date_paid%', $date_paid, $message);
            $message         = str_replace('%trans_no%', $trans_no, $message);
            $message         = str_replace('%amount%', $amount, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
            $message         = str_replace('%soa_date%', $soa_date, $message);
            $message         = str_replace('%decline_reason%', $decline_reason, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Declined Payment';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendForPickupAction($email, $fullname, $order_no, $ud_id, $soa_date, $customer_type)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/soa/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/soa/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-ready-pickup.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
            $message         = str_replace('%soa_date%', $soa_date, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Ready for Pick Up';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendForDeliveryAction($email, $fullname, $order_no, $ud_id, $soa_date, $customer_type)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."orders/soa/company_name/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."orders/soa/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-ready-delivery.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
            $message         = str_replace('%soa_date%', $soa_date, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Ready for Delivery';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendSentSpecialInquiryAction($email, $fullname, $order_no, $ud_id, $soa_date, $customer_type)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {

            if($customer_type != 1){
                $link_url = STOREFRONT_URI."inquiry/company/".$ud_id;

            }else{
                $link_url = STOREFRONT_URI."inquiry/personal/".$ud_id;
            }

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-quotient.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%order_no%', $order_no, $message);
            $message         = str_replace('%link_url%', $link_url, $message);
            $message         = str_replace('%soa_date%', $soa_date, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Sent Special Inquiry';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
    
    public function declineSpecialInquiriesAction($email, $fullname, $mail_body)
    {
        $this->view->disable();
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            
            $link_url = STOREFRONT_URI."product_catalog/combined/all/";

            $message         = file_get_contents(APP_PATH . '/app/views/email/email-declined-inq.phtml');
            $message         = str_replace('%base_url%', STOREFRONT_URI, $message);
            $message         = str_replace('%about_link%', STOREFRONT_URI."about_us", $message);
            $message         = str_replace('%term_link%', STOREFRONT_URI."terms_and_conditions", $message);
            $message         = str_replace('%contact_link%', STOREFRONT_URI."contact_us", $message);

            $message         = str_replace('%fullname%', $fullname, $message);
            $message         = str_replace('%mail_body%', $mail_body, $message);
            $message         = str_replace('%link_url%', $link_url, $message);

            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host       = 'secure.emailsrvr.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true; // Enable SMTP authentication
            $mail->Username   = 'contact@lpmconstruction.com.ph'; // SMTP username
            $mail->Password   = 'p@55w0rd'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom('contact@lpmconstruction.com.ph', 'LPM');
            $mail->addAddress($email,$fullname);
            
    
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'LPM Declined Special Inquiry';
            $mail->Body    = $message;
            // $mail->AltBody = strip_tags($message);
            
            $mail->send();
            //echo 'Message has been sent';
        }
        catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }



    
	
	




    
}