<?php
require_once 'classes/PHPExcel.php';

class CityChargesController extends ControllerBase {

    public function indexAction() {
		
		$truck_types = AdminTruckTypesTbl::find("archive_flag=0");
		
		
		$this->view->setVar('truck_types', $truck_types);
		
        $this->view->setVar('page_content', 'city_charges/index');
        $this->view->setVar('page_active', 'city_charges');
        $this->view->setVar('page_title', 'City Charges');

//        $this->view->setVar('css', 'city_charges.css');
		$this->assets->addJs('plugins/upload/upload.js');
    }
	
	public function getCityChargesListAction()
	{
		$this->view->disable();
		
		$ref_regions = RefRegionsTb::find();
		$ref_regions_array = array();
		foreach ($ref_regions as $ref_region)
		{
			$ref_regions_array[$ref_region->id] = $ref_region->name;
		}
		
		$ref_provinces = RefProvincesTb::find();
		$ref_provinces_array = array();
		$region_provinces_array = array();
		foreach ($ref_provinces as $ref_province)
		{
			$ref_provinces_array[$ref_province->id] = $ref_province->name;
			$region_provinces_array[$ref_province->id] = $ref_province->region_id;
		}
		
		$ref_cities = RefCitiesTb::find("province_id=47");
		$ref_cities_array = array();
		$ref_city_ids_array = array();
		foreach ($ref_cities as $ref_city)
		{
			$ref_cities_array[$ref_city->id] = $ref_city->name;
			$ref_city_ids_array[] = $ref_city;
		}
		
		$ref_cities = RefCitiesTb::find("province_id != 47");
		//$ref_cities_array = array();
		foreach ($ref_cities as $ref_city)
		{
			$ref_cities_array[$ref_city->id] = $ref_city->name;
			$ref_city_ids_array[] = $ref_city;
		}
		
		$truck_types = AdminTruckTypesTbl::find("archive_flag=0");
		$truck_types_array = array();
		foreach ($truck_types as $truck_type)
		{
			$truck_types_array[$truck_type->id] = $truck_type;
		}
		
		$city_charges = AdminCityChargesTbl::find();
		$city_charges_array = array();
		foreach ($city_charges as $city_charge)
		{
			if ($truck_types_array[$city_charge->truck_type_id])
			{
				if (!$city_charges_array[$city_charge->city_id]) $city_charges_array[$city_charge->city_id] = array();
				$city_charges_array[$city_charge->city_id][$city_charge->truck_type_id] = $city_charge;
			}
		}
		
		$data_array = array();
		$data_array['regions'] = $ref_regions_array;
		$data_array['provinces'] = $ref_provinces_array;
		$data_array['cities'] = $ref_cities_array;
		$data_array['city_ids'] = $ref_city_ids_array;
		$data_array['city_charges'] = $city_charges_array;
		$data_array['region_provinces'] = $region_provinces_array;
		$data_array['truck_types'] = $truck_types_array;
		
		echo json_encode($data_array);
	}
	
	public function deleteCityChargeAction()
	{
		$this->view->disable();
		
		$city_charge_id = $_POST['city_charge_id'];
		
		$city_charge = AdminCityChargesTbl::findFirst("city_charge_id=$city_charge_id");
		$city_charge->delete();

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE CITY CHARGE");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                
                
		echo "Success";
		
	}
	
	public function saveCityChargeAction()
	{
		$this->view->disable();
		
		$city_charge_id_array = $_POST['city_charge_id_array'];
		$textfield_array = $_POST['textfield_array'];
		$truck_types_array = $_POST['truck_types_array'];
		$restriction_array = $_POST['restriction_array'];
		
		$region_id = $_POST['region_id'];
		$province_id = $_POST['province_id'];
		$city_id = $_POST['city_id'];
		
		foreach ($city_charge_id_array as $key => $city_charge_id)
		{
			$truck_type_id = $truck_types_array[$key];
			$city_charge = AdminCityChargesTbl::findFirst("city_id=$city_id and truck_type_id=$truck_type_id");
			if (!$city_charge)
			{
				$city_charge = new AdminCityChargesTbl();
				$city_charge->region_id = $region_id;
				$city_charge->province_id = $province_id;
				$city_charge->city_id = $city_id;
			}
			$city_charge->truck_type_id = $truck_types_array[$key];
			$city_charge->wheel_charge = $textfield_array[$key];
			$city_charge->wheel_restriction = $restriction_array[$key];
			$city_charge->save();
		}

            /*****AUDIT LOGS******/
            try {
                $audit_log = new AuditLogger();
                $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "SAVE CITY CHARGE");
            }catch (Exception $e) { }
            /*****AUDIT LOGS******/                 
                
		echo "Success";
	}
	
	public function uploadBatchFileAction()
	{
		$this->view->disable();
		
		if ($this->request->hasFiles() == true)
		{
			foreach ($this->request->getUploadedFiles() as $file)
			{
				$unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
                $baseLocation = 'uploads/temp/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                //echo $unique_filename . "." . $ext;
				//echo $ext;
				
				$truck_types = AdminTruckTypesTbl::find("archive_flag=0");
				$truck_types_array = array();
				foreach ($truck_types as $truck_type)
				{
					$truck_types_array[$truck_type->truck_type] = $truck_type->id;
				}
				
				$ref_cities = RefCitiesTb::find();
				$ref_cities_array = array();
				foreach ($ref_cities as $ref_city)
				{
					$ref_cities_array[$ref_city->id] = $ref_city;
				}
				
				$ref_provinces = RefProvincesTb::find();
				$ref_provinces_array = array();
				foreach ($ref_provinces as $ref_province)
				{
					$ref_provinces_array[$ref_province->id] = $ref_province;
				}
				/*
				$ref_regions = RefRegionsTb::find();
				$ref_regions_array = array();
				foreach ($ref_regions as $ref_region)
				{
					$ref_regions_array[$ref_region->id] = $ref_region;
				}*/
				
				
				if (in_array($ext,array("xls","xlsx")))
				{
					//echo "Ariel"; die;
					$inputFileName = $baseLocation . $unique_filename . "." . $ext;
					//$spreadsheet = IOFactory::load($inputFileName);
					//$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
					
					//echo "Ariel"; die;
					$fileType = PHPExcel_IOFactory::identify($inputFileName);
					//$fileName = 'templates/template2.xlsx';
					//var_dump($fileType);
			
					
					// Read the file
					$objReader = PHPExcel_IOFactory::createReader($fileType);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = array(1,$objPHPExcel->getActiveSheet()->toArray(null,true,true,true));
					//var_dump($sheetData); die;
					
					
					$counter = 1;
					$error_message = "";
					$truck_type_index_array = array();
					//echo count($sheetData[1]);
					foreach ($sheetData[1] as $sheetRow)
					{
						if (($counter > 1) && ($sheetRow["A"]))
						{
							
							$city_id = $sheetRow["D"];
							//echo $city_id."::";
							$province_id = $ref_cities_array[$city_id]->province_id;
							$region_id = $ref_provinces_array[$province_id]->region_id;
							$index = "E";
							while ($sheetRow[$index])
							{
								//echo $sheetRow[$index];
								$truck_type_id = $truck_type_index_array[$index];
								
								
								
								$city_charge = AdminCityChargesTbl::findFirst("city_id=$city_id and truck_type_id=$truck_type_id");
								if (!$city_charge)
								{
									$city_charge = new AdminCityChargesTbl();
									$city_charge->city_id = $city_id;
									$city_charge->province_id = $province_id;
									$city_charge->region_id = $region_id;
									$city_charge->truck_type_id = $truck_type_id;
								}
								
								if ($sheetRow[$index] == "RESTRICTED")
								{
									$city_charge->wheel_charge = 0.00;
									$city_charge->wheel_restriction = 1;
								}
								else if (is_numeric($sheetRow[$index]))
								{
									$city_charge->wheel_charge = $sheetRow[$index];
									$city_charge->wheel_restriction = 0;
								}
								else
								{
									$city_charge->wheel_charge = $sheetRow[$index];
									$city_charge->wheel_restriction = 0;
								}
								$city_charge->save();
								
								
								$index++;
							}
						}
						else if ($counter == 1)
						{
							$index = "E";
							while ($sheetRow[$index])
							{
								//echo "[".$sheetRow[$index]."]";
								//echo $truck_types_array[$sheetRow[$index]];
								if (!$truck_types_array[$sheetRow[$index]])
								{
									echo "Invalid Truck Type Found: ".$sheetRow[$index]."\n";
									return;
								}
								else
								{
									$truck_type_index_array[$index] = $truck_types_array[$sheetRow[$index]];
								}
								$index++;
							}
						}
						$counter++;
					}
				}
				else
				{
					echo "Wrong File Format";
				}
			}
		}
	}
	
	public function get_random_filename()
    {
        $date   = Date('Y-m-d H:i:s');
        $length = 20;
        $key    = '';
        $keys   = array_merge(range(0, 9), range('a', 'z'));
        
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key.md5($date);
    }
	
	

}
