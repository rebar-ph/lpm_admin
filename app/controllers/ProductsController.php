<?php
//require APP_PATH . '/app/library/vendor/autoload.php';
require_once 'classes/PHPExcel.php';
//use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\IOFactory;

class ProductsController extends ControllerBase {

    public function indexAction() {
        
		$this->view->setVar('page_content', 'products/index');
        $this->view->setVar('page_active', 'products');
        $this->view->setVar('page_title', 'Products');

       	$this->view->setVar('css', 'products.css');
       	$this->view->setVar('js', 'products.js');
		
		$this->assets->addJs('plugins/upload/upload.js');
    }

    public function add_newAction() {
        
		$brands = RefSiteBrandTbl::find("archive_flag = '0' ORDER BY brand_name ASC");
		
		$categories = RefSiteCategoryTbl::find("archive_flag = '0' ORDER BY category_name ASC");
		
		$groups = RefSiteGroupTbl::find("archive_flag = '0' ORDER BY group_name ASC");
		
		
		$this->view->setVar('brands', $brands);
		$this->view->setVar('categories', $categories);
		$this->view->setVar('groups', $groups);
		
		$this->view->setVar('page_content', 'products/add_products');
        $this->view->setVar('page_active', 'products');
        $this->view->setVar('page_title', 'Products');

       	$this->view->setVar('css', 'products.css');
       	$this->view->setVar('js', 'products.js');
    }

    public function editAction($product_id=0) {
        
		$brands = RefSiteBrandTbl::find("archive_flag = '0' ORDER BY brand_name ASC");
		
		$categories = RefSiteCategoryTbl::find("archive_flag = '0' ORDER BY category_name ASC");
		
		$groups = RefSiteGroupTbl::find("archive_flag = '0' ORDER BY group_name ASC");
		
		$product = RefSiteProductTbl::findFirst("product_id=$product_id");
		
		$product_kinds = RefSiteProductKindsTbl::find("product_id=$product_id and archive_flag = 0");
		
		$product_groups = RefSiteProductGroupTbl::find("product_id=$product_id");
		$product_group_ids = array();
		foreach ($product_groups as $product_group)
		{
			//echo $product_group->group_id;
			$product_group_ids[] = $product_group->group_id;
		}
		//var_dump($product_group_ids); die;
		
		$this->view->setVar('brands', $brands);
		$this->view->setVar('categories', $categories);
		$this->view->setVar('groups', $groups);
		$this->view->setVar('product', $product);
		$this->view->setVar('product_kinds', $product_kinds);
		$this->view->setVar('product_group_ids', $product_group_ids);
		
		$this->view->setVar('page_content', 'products/edit-product');
        $this->view->setVar('page_active', 'products');
        $this->view->setVar('page_title', 'Products');

        $this->view->setVar('css', 'products.css');
        $this->view->setVar('js', 'products.js');
    }
	
	public function getProductsListAction() {
		$this->view->disable();
		
		$products = RefSiteProductTbl::getProductsList();
		$products_array = array();
		foreach ($products as $product)
		{
			$products_array[] = $product;
		}
		
		$product_groups = RefSiteProductGroupTbl::find();
		$product_groups_array = array();
		foreach ($product_groups as $product_group)
		{
			if (!$product_groups_array[$product_group->product_id]) $product_groups_array[$product_group->group_id] = array();
			$product_groups_array[$product_group->product_id][] = $product_group->group_id;
		}
		
		$groups = RefSiteGroupTbl::find();
		$groups_array = array();
		foreach ($groups as $group)
		{
			$groups_array[$group->group_id] = $group->group_name;
		}
		
		$categories = RefSiteCategoryTbl::find();
		$categories_array = array();
		foreach ($categories as $category)
		{
			$categories_array[$category->category_id] = $category->category_name;
		}
		
		$brands = RefSiteBrandTbl::find();
		$brands_array = array();
		foreach ($brands as $brand)
		{
			$brands_array[$brand->brand_id] = $brand->brand_name;
		}
		
		$data_array = array();
		$data_array['products'] = $products_array;
		$data_array['product_groups'] = $product_groups_array;
		$data_array['groups'] = $groups_array;
		$data_array['categories'] = $categories_array;
		$data_array['brands'] = $brands_array;
		
		echo json_encode($data_array);
	}
	
	public function addProductAction()
	{
		$this->view->disable();
		$brand_id = $_POST['brand_id'];
		$category_id = $_POST['category_id'];
		$product_name = $_POST['product_name'];
		$product_description = $_POST['product_description'];
		$groups_array = $_POST['groups_array'];
		$state_id = $_POST['state_id'];
		$kinds_array = $_POST['kinds_array'];
		$measurements_array = $_POST['measurements_array'];
		$colors_array = $_POST['colors_array'];
		$types_array = $_POST['types_array'];
		$displays_array = $_POST['displays_array'];
		$uoms_array = $_POST['uoms_array'];
		$lead_times_array = $_POST['lead_times_array'];
		
		$product = new RefSiteProductTbl();
		$product->product_name = $product_name;
		$product->description = $product_description;
		$product->category_id = $category_id;
		$product->datetime_created = date('Y-m-d H:i:s');
		$product->state = $state_id;
		$product->brand_id = $brand_id;
		$product->modified_on = $product->datetime_created;
		$product->archive_flag = 0;
		$product->display_status = 0;
		        
        //Save Images
        $baseLocation = '../../lpm_website/public/product_images/';
        
        if(!empty($_POST['image_link_1'])){
            if ($_POST['image_link_1'] !== "DEL"){
                $data = $_POST['image_link_1'];
                $filename = md5($product_name.mt_rand());
                $ext = ".jpg";        

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents($baseLocation.$filename.$ext, $data);
                $product->image_link_1 = $filename.$ext;
            } else {$product->image_link_1 = NULL;}
        }
        if(!empty($_POST['image_link_2'])){
            if ($_POST['image_link_2'] !== "DEL"){
                $data = $_POST['image_link_2'];
                $filename = md5($product_name.mt_rand());
                $ext = ".jpg";                    

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents($baseLocation.$filename.$ext, $data);
                $product->image_link_2 = $filename.$ext;
            } else {$product->image_link_2 = NULL;}
        }
        if(!empty($_POST['image_link_3'])){
            if ($_POST['image_link_3'] !== "DEL"){
                $data = $_POST['image_link_3'];
                $filename = md5($product_name.mt_rand());
                $ext = ".jpg";                    

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents($baseLocation.$filename.$ext, $data);
                $product->image_link_3 = $filename.$ext;
            } else {$product->image_link_3 = NULL;}
        }
        if(!empty($_POST['image_link_4'])){
            if ($_POST['image_link_4'] !== "DEL"){
                $data = $_POST['image_link_4'];
                $filename = md5($product_name.mt_rand());
                $ext = ".jpg";                    

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents($baseLocation.$filename.$ext, $data);
                $product->image_link_4 = $filename.$ext;
            } else {$product->image_link_4 = NULL;}
        }
        if(!empty($_POST['image_link_5'])){
            if ($_POST['image_link_5'] !== "DEL"){
                $data = $_POST['image_link_5'];
                $filename = md5($product_name.mt_rand());
                $ext = ".jpg";                    

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents($baseLocation.$filename.$ext, $data);
                $product->image_link_5 = $filename.$ext;
            } else {$product->image_link_5 = NULL;}
        } 
        //END Save Images
                
		if ($product->save() == false)
		{
			foreach ($product->getMessages() as $msg) echo $msg;
		}
		
		foreach ($groups_array as $group)
		{       
			if (!empty($group)){
				$product_group = new RefSiteProductGroupTbl();
				$product_group->product_id = $product->product_id;
				$product_group->group_id = $group;
				$product_group->save();
			}
		}
		
		foreach ($kinds_array as $key=>$kind)
		{
			$kind_row = new RefSiteProductKindsTbl();
			$kind_row->product_id = $product->product_id;
			$kind_row->kind_name = $kind;
			$kind_row->measurement = $measurements_array[$key];
			$kind_row->color = $colors_array[$key];
			$kind_row->uom = $uoms_array[$key];
			$kind_row->date_created = $product->datetime_created;
			$kind_row->modified_on = $product->datetime_created;
			$kind_row->prepaid = $types_array[$key];
			$kind_row->display_status = $displays_array[$key];
			$kind_row->lead_time = $lead_times_array[$key];
			$kind_row->archive_flag = 0;
			if ($kind_row->save() == false)
			{
				foreach ($kind_row->getMessages() as $msg) echo $msg;
			} else {
			
                $pricing_row = new AdminProductPricingTbl();
                $pricing_row->kind_id = $kind_row->product_kind_id;
                $pricing_row->product_id = $product->product_id;
                $pricing_row->points = 0;
                $pricing_row->min_qty1 = 0;
                $pricing_row->min_qty2 = 0;
                $pricing_row->min_qty3 = 0;
                $pricing_row->p_price1 = 0;
                $pricing_row->p_price2 = 0;
                $pricing_row->p_price3 = 0;
                $pricing_row->c_price1 = 0;
                $pricing_row->c_price2 = 0;
                $pricing_row->c_price3 = 0;
                $pricing_row->r_price1 = 0;
                $pricing_row->r_price2 = 0;
                $pricing_row->r_price3 = 0;
                $pricing_row->mode = 1;
                $pricing_row->base = 0;
                $pricing_row->increment = 1;
                $pricing_row->save();
            }    
		} 
                
                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "ADD PRODUCT");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                 
		
		echo "Success";
	}
	
	public function updateProductAction()
	{
		$this->view->disable();
		$product_id = $_POST['product_id'];
		$brand_id = $_POST['brand_id'];
		$category_id = $_POST['category_id'];
		$product_name = $_POST['product_name'];
		$product_description = $_POST['product_description'];
		$groups_array = $_POST['groups_array'];
		$state_id = $_POST['state_id'];
		$kind_ids_array = $_POST['kind_ids_array'];
		$kinds_array = $_POST['kinds_array'];
		$measurements_array = $_POST['measurements_array'];
		$colors_array = $_POST['colors_array'];
		$types_array = $_POST['types_array'];
		$displays_array = $_POST['displays_array'];
		$uoms_array = $_POST['uoms_array'];
		$lead_times_array = $_POST['lead_times_array'];
		
		$product = RefSiteProductTbl::findFirst("product_id = $product_id");
		$product->product_name = $product_name;
		$product->description = $product_description;
		$product->category_id = $category_id;
		//$product->datetime_created = date('Y-m-d H:i:s');
		$product->state = $state_id;
		$product->brand_id = $brand_id;
		$product->modified_on = date('Y-m-d H:i:s');
		$product->archive_flag = 0;
                
                //Save Images
                $baseLocation = '../../lpm_website/public/product_images/';
                
                if(!empty($_POST['image_link_1'])){
                        $filename = md5($product_name.mt_rand());
                        $ext = ".jpg";                     
                    if ($_POST['image_link_1'] !== "DEL"){
                        $data = $_POST['image_link_1'];       

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents($baseLocation.$filename.$ext, $data);
                        $product->image_link_1 = $filename.$ext;
                    } else {unlink($baseLocation.$product->image_link_1); $product->image_link_1 = NULL;}
                }
                if(!empty($_POST['image_link_2'])){
                        $filename = md5($product_name.mt_rand());
                        $ext = ".jpg";                     
                    if ($_POST['image_link_2'] !== "DEL"){
                        $data = $_POST['image_link_2'];                   

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents($baseLocation.$filename.$ext, $data);
                        $product->image_link_2 = $filename.$ext;
                    } else {unlink($baseLocation.$product->image_link_2); $product->image_link_2 = NULL;}
                }
                if(!empty($_POST['image_link_3'])){
                        $filename = md5($product_name.mt_rand());
                        $ext = ".jpg";                     
                    if ($_POST['image_link_3'] !== "DEL"){
                        $data = $_POST['image_link_3'];                  

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents($baseLocation.$filename.$ext, $data);
                        $product->image_link_3 = $filename.$ext;
                    } else {unlink($baseLocation.$product->image_link_3); $product->image_link_3 = NULL;}
                }
                if(!empty($_POST['image_link_4'])){
                        $filename = md5($product_name.mt_rand());
                        $ext = ".jpg";                     
                    if ($_POST['image_link_4'] !== "DEL"){
                        $data = $_POST['image_link_4'];                 

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents($baseLocation.$filename.$ext, $data);
                        $product->image_link_4 = $filename.$ext;
                    } else {unlink($baseLocation.$product->image_link_4); $product->image_link_4 = NULL;}
                }
                if(!empty($_POST['image_link_5'])){
                        $filename = md5($product_name.mt_rand());
                        $ext = ".jpg";                     
                    if ($_POST['image_link_5'] !== "DEL"){
                        $data = $_POST['image_link_5'];                  

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents($baseLocation.$filename.$ext, $data);
                        $product->image_link_5 = $filename.$ext;
                    } else {unlink($baseLocation.$product->image_link_5); $product->image_link_5 = NULL;}
                } 
                //END Save Images                
                
		if ($product->save() == false)
		{
			foreach ($product->getMessages() as $msg) echo $msg;
		}
		
		
		$product_groups = RefSiteProductGroupTbl::find("product_id=$product_id");
		$product_groups->delete();
		
		foreach ($groups_array as $group)
		{
            if (!empty($group)){
			$product_group = new RefSiteProductGroupTbl();
			$product_group->product_id = $product_id;
			$product_group->group_id = $group;
			$product_group->save();
            }    
		}
		
		$product_kinds = RefSiteProductKindsTbl::find("product_id=$product_id");
		foreach ($product_kinds as $product_kind)
		{
			$product_kind->archive_flag = 1;
			$product_kind->save();
		}
		
		$has_published_status = 0;
		foreach ($kinds_array as $key=>$kind)
		{
			$new_kind = 0;
			$product_kind_id = $kind_ids_array[$key];
			if ($product_kind_id != 0) $kind_row = RefSiteProductKindsTbl::findFirst("product_kind_id=$product_kind_id");
			else 
			{
				$kind_row = new RefSiteProductKindsTbl();
				$kind_row->date_created = $product->modified_on;
				
				$new_kind = 1;
			}
			$kind_row->product_id = $product->product_id;
			$kind_row->kind_name = $kind;
			$kind_row->measurement = $measurements_array[$key];
			$kind_row->color = $colors_array[$key];
			$kind_row->uom = $uoms_array[$key];
			$kind_row->modified_on = $product->modified_on;
			$kind_row->prepaid = $types_array[$key];
			$kind_row->display_status = $displays_array[$key];
			if ($kind_row->display_status == 0) $kind_row->published_status = 0;
			if ($kind_row->published_status == 1) $has_published_status += 1;
			$kind_row->lead_time = $lead_times_array[$key];
			$kind_row->archive_flag = 0;
			if ($kind_row->save() == false)
			{
				foreach ($kind_row->getMessages() as $msg) echo $msg;
			}
			else
			{
				if ($new_kind == 1)
				{
					$pricing_row = new AdminProductPricingTbl();
					$pricing_row->kind_id = $kind_row->product_kind_id;
					$pricing_row->product_id = $product->product_id;
					$pricing_row->points = 0;
					$pricing_row->min_qty1 = 0;
					$pricing_row->min_qty2 = 0;
					$pricing_row->min_qty3 = 0;
					$pricing_row->p_price1 = 0;
					$pricing_row->p_price2 = 0;
					$pricing_row->p_price3 = 0;
					$pricing_row->c_price1 = 0;
					$pricing_row->c_price2 = 0;
					$pricing_row->c_price3 = 0;
					$pricing_row->r_price1 = 0;
					$pricing_row->r_price2 = 0;
					$pricing_row->r_price3 = 0;
					$pricing_row->mode = 1;
					$pricing_row->base = 0;
					$pricing_row->increment = 1;
					$pricing_row->save();
				}
			}
			
			
		}
		if ($has_published_status == 0) {
				$product->display_status = 0;
		}  else {
				$product->display_status = 1;
		}
		$product->save();

                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "EDIT PRODUCT");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                 
                
		echo "Success";
	}
	
	public function deleteProductAction()
	{
		$this->view->disable();
		
		$product_id = 0;
		if ($_POST['product_id']) $product_id = $_POST['product_id'];
		//echo $product_id;
		$product = RefSiteProductTbl::findFirst("product_id=$product_id");
		$product->archive_flag = 1;
		if ($product->save() == false)
		{
			foreach ($product->getMessages() as $message) echo $message;
		}
		
		$product_kinds = RefSiteProductKindsTbl::find("product_id=$product_id");
		foreach ($product_kinds as $product_kind)
		{
			$product_kind->archive_flag = 1;
			$product_kind->save();
		}
		
		$product_pricing = AdminProductPricingTbl::find("product_id=$product_id");
		$product_pricing->delete();
		
		//echo $product_id;
                
                /*****AUDIT LOGS******/
                try {
                    $audit_log = new AuditLogger();
                    $audit_log->logAudit($this->session->LPMADMINSESSION['user_id'], "IP ADDRESS", "DELETE PRODUCT");
                }catch (Exception $e) { }
                /*****AUDIT LOGS******/                 
                
		echo "Success";
	}
	
	public function toggleDisplayStatusAction()
	{
		$this->view->disable();
		
		$product_kind_id = 0;
		if ($_POST['product_kind_id']) $product_kind_id = $_POST['product_kind_id'];
		
		if ($product_kind_id != "ALL")
		{
			$product_kind = RefSiteProductKindsTbl::findFirst("product_kind_id=$product_kind_id");
			if ($product_kind->published_status == 1) $product_kind->published_status = 0;
			else $product_kind->published_status = 1;
			$product_kind->save();
			
			$product_id = $product_kind->product_id;
			
			$has_published_status = 0;
//			$product_kinds = RefSiteProductKindsTbl::find("product_id=$product_id");
                        $product_kinds = RefSiteProductKindsTbl::find("product_id=$product_id AND archive_flag = '0'");
			foreach ($product_kinds as $product_kind)
			{
				if ($product_kind->published_status == 1) $has_published_status += 1;
			}
			
			if ($has_published_status == 0)
			{
				$product = RefSiteProductTbl::findFirst("product_id=$product_id");
				$product->display_status = 0;
				$product->save();
			}
			else
			{
				$product = RefSiteProductTbl::findFirst("product_id=$product_id");
				$product->display_status = 1;
				$product->save();
			}
		}
		else
		{
			$product_kinds = RefSiteProductKindsTbl::publishAll();
		}
		
		echo "Success";
		
	}
	
	public function uploadBatchFileAction()
	{
		$this->view->disable();
        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $unique_filename = $this->get_random_filename();
                $ext             = $file->getExtension();
                $baseLocation = 'uploads/temp/';
                $file->moveTo($baseLocation . $unique_filename . "." . $ext);
                //echo $unique_filename . "." . $ext;
				
				if (in_array($ext,array("xls","xlsx")))
				{
					$inputFileName = $baseLocation . $unique_filename . "." . $ext;
					//$spreadsheet = IOFactory::load($inputFileName);
					//$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
					
					$fileType = PHPExcel_IOFactory::identify($inputFileName);
					//$fileName = 'templates/template2.xlsx';
			
					// Read the file
					$objReader = PHPExcel_IOFactory::createReader($fileType);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = array(1,$objPHPExcel->getActiveSheet()->toArray(null,true,true,true));
					//var_dump($sheetData); die;
					
					$ref_categories = RefSiteCategoryTbl::find();
					$ref_categories_array = array();
					foreach ($ref_categories as $ref_category)
					{
						$ref_categories_array[$ref_category->category_name] = $ref_category->category_id;
					}
					
					$ref_brands = RefSiteBrandTbl::find();
					$ref_brands_array = array();
					foreach ($ref_brands as $ref_brand)
					{
						$ref_brands_array[$ref_brand->brand_name] = $ref_brand->brand_id;
					}
					
					$ref_groups = RefSiteGroupTbl::find();
					$ref_groups_array = array();
					foreach ($ref_groups as $ref_group)
					{
						$ref_groups_array[$ref_group->group_name] = $ref_group->group_id;
					}
					
					$counter = 1;
					$error_message = "";
					foreach ($sheetData[1] as $sheetRow)
					{
						//var_dump($sheetRow);
						$error_message_row = "";
						if (($counter > 1) && ($sheetRow["A"]))
						{
							$category_name = $sheetRow["A"];
							$brand_name = $sheetRow["B"];
							$product_name = $sheetRow["C"];
							$kind_name = $sheetRow["D"];
							$measurement = $sheetRow["E"];
							$color = $sheetRow["F"];
							$groups = $sheetRow["G"];
							$tag = $sheetRow["H"];
							$type = $sheetRow["I"];
							$display = $sheetRow["J"];
							$uom = $sheetRow["K"];
							$lead_time = $sheetRow["L"];
							
							
							//echo "Category " . $category_name;
							if ($ref_categories_array[$category_name]) $category_id = $ref_categories_array[$category_name];
							else $error_message_row .= "No Category Found for Row ".$counter."\n";
							
							if ($ref_brands_array[$brand_name]) $brand_id = $ref_brands_array[$brand_name];
							else $error_message_row .= "No Brand Found for Row ".$counter."\n";
							
							if ($error_message_row == "")
							{
								$state_id = 0;
								switch($tag)
								{
									case "New Arrival": $state_id = 1;
										break;
									case "Best Seller": $state_id = 2;
										break;
								}
								
								$product = RefSiteProductTbl::findFirst("product_name = '$product_name'");
								if (!$product)
								{
									$product = new RefSiteProductTbl();
									$product->datetime_created = date('Y-m-d H:i:s');
									$product->product_name = $product_name;
									$product->description = "";
									$product->category_id = $category_id;
									
									$product->state = $state_id;
									$product->brand_id = $brand_id;
									$product->modified_on = $product->datetime_created;
									$product->archive_flag = 0;
									$product->display_status = 0;
									if ($product->save() == false)
									{
										foreach ($product->getMessages() as $msg) $error_message_row .= $msg . "\n";
									}
								}
								
								$groups_array = array_map('trim', explode(",",$groups));
								$product_id = $product->product_id;
								
								
								foreach ($groups_array as $group)
								{       
									if (!empty($group)){
										
										$group_id = $ref_groups_array[$group];
										if ($group_id)
										{
											$product_group = RefSiteProductGroupTbl::findFirst("product_id=$product_id and group_id=$group_id");
											if (!$product_group)
											{
												$product_group = new RefSiteProductGroupTbl();
												$product_group->product_id = $product_id;
												$product_group->group_id = $group_id;
												$product_group->save();
											}
										}
										else
										{
											$error_message_row .= "No Group Found for Row ".$counter."\n";
										}
									}
								}
								
								$prepaid = 0;
								if (strtoupper($type) != "REGULAR")
								{
									$prepaid = 1;
								}
								
								$display_status = 1;
								if (strtoupper($display) != "YES")
								{
									$display_status = 0;
								}
								
								$kind_row = new RefSiteProductKindsTbl();
								$kind_row->product_id = $product_id;
								$kind_row->kind_name = $kind_name;
								$kind_row->measurement = $measurement;
								$kind_row->color = $color;
								$kind_row->uom = $uom;
								$kind_row->date_created = date('Y-m-d H:i:s');
								$kind_row->modified_on = date('Y-m-d H:i:s');
								$kind_row->prepaid = $prepaid;
								$kind_row->display_status = $display_status;
								$kind_row->lead_time = $lead_time;
								$kind_row->archive_flag = 0;
								if ($kind_row->save() == false)
								{
									foreach ($kind_row->getMessages() as $msg) $error_message_row .= $msg . "\n";
								} else {
									$pricing_row = new AdminProductPricingTbl();
									$pricing_row->kind_id = $kind_row->product_kind_id;
									$pricing_row->product_id = $product->product_id;
									$pricing_row->points = 0;
									$pricing_row->min_qty1 = 0;
									$pricing_row->min_qty2 = 0;
									$pricing_row->min_qty3 = 0;
									$pricing_row->p_price1 = 0;
									$pricing_row->p_price2 = 0;
									$pricing_row->p_price3 = 0;
									$pricing_row->c_price1 = 0;
									$pricing_row->c_price2 = 0;
									$pricing_row->c_price3 = 0;
									$pricing_row->r_price1 = 0;
									$pricing_row->r_price2 = 0;
									$pricing_row->r_price3 = 0;
									$pricing_row->mode = 1;
									$pricing_row->base = 0;
									$pricing_row->increment = 1;
									$pricing_row->save();
								}    
			
							}
							else
							{
								$error_message .= $error_message_row . "Row " . $counter . " with Product and Kind Name " . $product_name . " " . $kind_name . " not inserted.\n";
							}
							
						}
						$error_message .= $error_message_row;
						$counter++;
					}
					echo $error_message;
					//return $unique_filename . "." . $ext;
				}
				else
				{
					echo "Wrong File Format";
				}
            }
			
        }
		
		
	}
	
	public function get_random_filename()
    {
        $date   = Date('Y-m-d H:i:s');
        $length = 20;
        $key    = '';
        $keys   = array_merge(range(0, 9), range('a', 'z'));
        
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key.md5($date);
    }

}
